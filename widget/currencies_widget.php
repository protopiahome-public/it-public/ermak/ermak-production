<?php 
	
	
	class SMCP_Currencies_Widget extends WP_Widget
	{
		
		/**
		 * Constructor
		 *
		 * @return void
		 * @since 0.5.0
		 */
		function SMCP_Currencies_Widget()
		{
			parent::__construct( false, __('Ermak Currencies Widget', 'smco'), array('description' => __("All currency Types of Metagame", 'smc'), 'classname' => 'smcp_currencies') );
		}
		
		/**
		 * Display widget.
		 *
		 * @param array $args
		 * @param array $instance
		 * @return void
		 * @since 0.5.0
		 */
		function widget( $args, $instance ) 
		{
			global $SMP_Currency_Type;
			extract( $args );
			$instance['title'] ? NULL : $instance['title'] = '';
			$title = apply_filters('widget_title', $instance['title']);
			$output = $before_widget."\n";
			if($title)
				$output .= $before_title.$title.$after_title;
			$currs		= SMP_Currency_Type::get_all();
			$output		.= "<table style='width:100%;'>
			<thread>
				<th>". __("Currency Type","smp"). "</th>
				<th>". __("Rate for 1 UE", "smp"). "</th>
			</thread>";
			$i=0;
			foreach($currs as $curr_id)
			{
				$curr	= SMP_Currency_Type::get_instance($curr_id);
				$class	= $i%2 == 0 ? "ob" : "ab";
				$output	.= "<tr class='$class'><td>" . $curr->body->post_title. "</td><td>" . $curr->get_rate() . "</td></tr>";
				$i++;
			}
			$output	.= "</table>";
			$output .= $after_widget."\n";
			echo $output;
		}
		
		/**
		 * Configuration form.
		 *
		 * @param array $instance
		 * @return void
		 * @since 0.5.0
		 */
		function form( $instance ) 
		{
			$defaults = array(
				'title' 			=> __("Currencies", "smp"),		
			);
			$instance = wp_parse_args( (array) $instance, $defaults );
			?>				
				<div class="alx-options-posts">
					<p>
						<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>:</label>
						<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance["title"]); ?>" />
					</p>		 
				</div>
			<?php
		}
		
		/**
		 * Display widget.
		 *
		 * @param array $instance
		 * @return array
		 * @since 1.5.6
		 */
		function update( $new_instance, $old_instance )
		{
			$instance = $old_instance;
			$instance['title'] 			= strip_tags($new_instance['title']);		
			return $instance;
		}
	}
	
/*  Register widget
/* ------------------------------------ */
	function smcp_currencies_register_widget() { 
		register_widget( 'SMCP_Currencies_Widget' );
	}
	add_action( 'widgets_init', 'smcp_currencies_register_widget' );
	

