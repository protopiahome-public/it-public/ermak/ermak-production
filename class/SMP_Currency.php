<?php
	class SMP_Currency
	{
		public $options;
		function __construct()
		{
		
			$this->options									= get_option(SMP);
			
			add_action('admin_menu',										array($this, 'admin_page_handler'), 2);
			//if(!$this->options['finance_enabled'])							return;
			/*	*/
			add_action('init', 												array(SMP_Currency_Type, 'add_SMP_Currency_Type'),12 );
			add_action('admin_menu',										array(SMP_Currency_Type, 'my_extra_fields_ca_type'));
			add_action('save_post_smp_currency_type',						array(SMP_Currency_Type, 'true_save_box_data_ca_type'));
			add_filter('manage_edit-smp_currency_type_columns',		 		array(SMP_Currency_Type, 'add_views_column_type'), 4);
			add_filter('manage_edit-smp_currency_type_sortable_columns', 	array(SMP_Currency_Type, 'add_views_sortable_column_type'));
			add_filter('manage_smp_currency_type_posts_custom_column', 		array(SMP_Currency_Type, 'fill_views_column_type'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
			add_action( SMC_LOCATION_NAME.'_add_form_fields', 				array(SMP_Currency_Type, 'add_location_type_to_location'), 9, 2 );
			add_action( SMC_LOCATION_NAME.'_edit_form_fields', 				array(SMP_Currency_Type, 'edit_location_type_to_location'), 9, 2 );
			//add_filter("manage_edit-location_columns", 					array(SMP_Currency_Type, 'location_columns'));
			//add_filter("manage_location_custom_column", 					array(SMP_Currency_Type, 'manage_location_columns'), 10, 3);			
			add_filter("smc_location_tab_over_params",						array(SMP_Currency_Type, 'smc_location_tab_over_params'), 9, 3);
			
			//Factory
			add_action('smp_admin_factory_box_func',						array(SMP_Currency_Type, 	'my_extra_fields_factory'));
			add_action('save_post_factory',									array(SMP_Currency_Type, 	'true_save_box_data_factory'));
			add_filter('manage_edit-factory_columns',		 				array(SMP_Currency_Type, 	'add_views_column_factory'), 4);
			add_filter('manage_edit-factory_sortable_columns', 				array(SMP_Currency_Type,	'add_views_sortable_column_factory'));
			add_filter('manage_factory_posts_custom_column', 				array(SMP_Currency_Type,	'fill_views_column_factory'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php				
			add_filter('smp_factory_add_batch', 							array($this,	'smp_factory_add_batch'), 5, 3); 			
			
			
			
			add_action('smp_admin_factory_box_func',						array($this, 	'my_extra_fields_factory'), 9);
			add_action('save_post_factory',									array($this, 	'true_save_box_data_factory'), 9);
			add_filter('manage_edit-factory_columns',		 				array($this, 	'add_views_column_factory'), 9, 4);
			add_filter('manage_edit-factory_sortable_columns', 				array($this,	'add_views_sortable_column_factory'), 9);
			add_filter('manage_factory_posts_custom_column', 				array($this,	'fill_views_column_factory'), 9, 2); // wp-admin/includes/class-wp-posts-list-table.php				
			
			//Goods Batch
			add_filter('smp_batch_extra_fields',						array(SMP_Currency_Type, 	'my_extra_fields_batch'),11, 2);
			add_action('save_post_goods_batch',							array(SMP_Currency_Type, 	'true_save_box_data_batch'));
			add_filter('manage_edit-goods_batch_columns',		 		array(SMP_Currency_Type, 	'add_views_column_batch'), 5);
			add_filter('manage_edit-goods_batch_sortable_columns', 		array(SMP_Currency_Type,	'add_views_sortable_column_batch'));
			add_filter('manage_goods_batch_posts_custom_column', 		array(SMP_Currency_Type,	'fill_views_column_batch'), 6, 2); // wp-admin/includes/class-wp-posts-list-table.php
			add_filter('smp_butch_slide_0', 								array(SMP_Currency_Type,	'smp_butch_slide_5'), 1, 2); // wp-admin/includes/class-wp-posts-list-table.php
			add_filter("smp_batch_card_price",								array($this, "smp_batch_card_price"),11,2);
			
			
			add_action('init', 												array(SMP_Account, 'add_SMC_Currency'),12 );
			add_action('admin_menu',										array(SMP_Account, 'my_extra_fields_ca'));
			add_action("save_post_smp_currency_account",					array(SMP_Account, 'true_save_box_data'));
			add_filter('manage_edit-smp_currency_account_columns',		 	array(SMP_Account, 'add_views_column'), 4);
			add_filter('manage_edit-smp_currency_account_sortable_columns', array(SMP_Account, 'add_views_sortable_column'));
			add_filter('manage_smp_currency_account_posts_custom_column', 	array(SMP_Account, 'fill_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
			add_filter('pre_get_posts',										array(SMP_Account,	'add_column_views_request'));
			add_action('create_'.SMC_LOCATION_NAME, 						array(SMP_Account, 'save_taxonomy_custom_meta'), 10);
			add_action("delete_".SMC_LOCATION_NAME,							array(SMP_Account, 'delete_location_handler'));
		
			add_action( 'admin_notices',					array(SMP_Currency_Type, 'after_install_sticker') , 43);
			
			//	Metagame menu in frontend		
			add_action( 'wp_before_admin_bar_render',		array($this, 'my_admin_bar_render' ), 12);			
			add_action('smc_myajax_submit', 				array($this, 'smc_myajax_submit'));
			add_filter("smp_my_tools",						array($this, 'smp_my_tools'));
			
			//
			add_filter("smp_factory_get_need",				array($this, "smp_factory_get_need"), 11, 2);
			add_filter("smp_factory_rize_need",				array($this, "smp_factory_rize_need"), 11, 2);
			add_filter("smp_factory_price_production",		array($this, "smp_factory_price_production"), 11, 3);
			add_filter("smp_factory_form_info",				array($this, "smp_factory_form_info"), 11, 2);
			
		}		
		
		function my_admin_bar_render() 
		{
			if(!current_user_can('administrator'))	return;
			global $wp_admin_bar;
			$wp_admin_bar->add_menu( array(
				'parent' => 'metagame', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'finance_panel', // ID ссылки
				'title' => __('Ermak. Finance', "smp"), //заголовок ссылки
				'href' => "/wp-admin/admin.php?page=Metagame_Finance_page" //имя файла	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'finance_panel', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'SMP_Currency_Type_panel', // ID ссылки
				'title' => __('all Currency Types', 'smp'), //заголовок ссылки
				'href' => "/wp-admin/edit.php?post_type=".SMP_CURRENCY_TYPE //имя файла	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'finance_panel', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'smc_currency_accaunt_panel', // ID ссылки
				'title' => __('all Currency Accounts', 'smp'), //заголовок ссылки
				'href' => "/wp-admin/edit.php?post_type=".SMP_CURRENCY_ACCOUNT //имя файла	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'finance_panel', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'smp_invoice_panel', // ID ссылки
				'title' => __('all Invoices', 'smp'), //заголовок ссылки
				'href' => "/wp-admin/edit.php?post_type=smp_invoice" //имя файла	
			));
		}
		function admin_page_handler()
		{
			add_menu_page( 
						__('Ermak. Finance', "smp"), // title
						__('Ermak. Finance', "smp"), // name in menu
						'manage_options', // capabilities
						'Metagame_Finance_page', // slug
						array($this, 'menu_Finance_page'), // options function name
						'none', //'dashicons-portfolio',//SMP_URLPATH .'img/icon.png', // icon url
						'20.521'
						);
		}
		function menu_Finance_page()
		{ 
			if($_POST['all_submit']) 
			{
				$this->options['finance_enabled']		= $_POST['finance_enabled']=="on" ? 1 : 0;				
				update_option(SMP, $this->options);
				echo $this->options['finance_enabled'];
			}
			if($_POST['set_settings']) 
			{
				$this->options['default_currency_type_id']		= $_POST['default_currency_type_id'];				
				$this->options['currency_transactions']			= $_POST['currency_transactions']=="on";				
				update_option(SMP, $this->options);
			}
			if($_POST['update_all_locations'])
			{
				$cts		= SMP_Currency_Type::get_all_ids();
				$args		= array(
										 'number' 		=> 0
										,'offset' 		=> 0
										,"hide_empty"	=> false
										,'fields'		=> 'ids'
									);
				$locs		= get_terms(SMC_LOCATION_NAME, $args);
				foreach($locs as $loc)
				{					
					foreach($cts as $ct_id)
					{					
						$args	= array(
											'numberposts' 	=> -1,
											'offset' 		=> 0, 
											'post_type' 	=> SMP_CURRENCY_ACCOUNT,
											'post_status' 	=> 'publish', 
											'fields'		=> 'ids', 
											'meta_query' 	=> array(
																		'relation'		=> 'AND',
																		array(	
																				'key' 	=> 'owner_id', 
																				'value' => $loc
																			  ),
																		array(	
																				'key' 	=> 'ctype_id', 
																				'value' => $ct_id
																			  )
																	)
										  );
						$exist	= get_posts($args);	
						if(count($exist)==0)
							SMP_Account::save_taxonomy_custom_meta($loc, $ct_id);
					}
				}
			}
			?>
				<h2><?php _e("Metagame Finance", "smp")?></h2>
				<form name="settings"  method="post"  enctype="multipart/form-data" id="settings">
						
					<div class="h2">
						<div class="h4">
							<board_title><?php _e("Settings"); ?></board_title>
							<input name='finance_enabled' id='finance_enabled' type="checkbox" class="css-checkbox" <?php checked(1, $this->options['finance_enabled']);?> /> 
							<label for="finance_enabled" class="css-label"><?php _e("Enabled Finance", "smp"); ?></label>
							<p>
								<input type="submit" name="all_submit" value="<?php _e("Edit");?>"/>
							</p>
							<hr/>
							<!--p>
								<board_title><?php _e("Set default Currency type", "smp");?></board_title>								
								<?php  echo $this->wp_droplist_currency_type(array("name"=>"default_currency_type_id", "id"=>"default_currency_type_id", "class"=>"h2", 'selected'=> $this->options['default_currency_type_id']));?>
							</p-->
							<p>
								<board_title><?php _e("Arm Functions", "smp");?></board_title>	
								<input name='currency_transactions' id='currency_transactions' type="checkbox" class="css-checkbox" <?php checked(1, $this->options['currency_transactions']);?> /> 
								<label for="currency_transactions" class="css-label"><?php _e("Enabled Currency operation center", "smp"); ?></label>
							</p>
							<p>
								<board_title><?php _e("", "smp"); ?></board_title>
							</p>
							<p>
								<input type="submit" name="set_settings" class="button-primary" value="<?php _e("Edit"); ?>"/>
							</p>
						</div>
						<div class="h4">
							<board_title><?php _e("Actions"); ?></board_title>
							<input type="submit" name="update_all_locations" class="button-primary" value="<?php _e("Add lost accounts for all orphan Locations", "smp");?>"/>
						</div>
					</div>
				</form>
			<?php
		}
		
		
		//========================
		//
		//
		//
		//========================
		
		
		static function install()
		{
			init_textdomain_smp();			
			// create bank page
			$my_bank = array(
								  'post_title'   		=> __("Bank", "smp"),
								  'post_type' 			=> 'page',
								  'post_content' 		=> "[smp_bank]"  ,
								  'post_status'  		=> 'publish',
								  'comment_status'		=> 'closed',
								);

			return wp_insert_post( $my_bank ); 
		}
		
		static function is_owner($user_id, $account_id)
		{
			$owner_id			= get_post_meta($account_id, "owner_id", true);
			return Factories::is_user_owner($owner_id, $user_id);
		}
		
		static function transfer_from($account_id, $summ, $payee_id='unknown', $reason='output')
		{
			if($summ<0)			return false;
			$ac					= SMP_Account::get_instance($account_id);
			if(!$ac->is_enabled())		return false;
			$count				= $ac->get_meta("count");
			if($summ>$count)	$summ =$count;
			$ost				= $count - $summ;
			$ac->update_meta("count", $ost);
			//
			$ctype_id			= $ac->get_meta("ctype_id");
			$ct					= SMP_Currency_Type::get_instance($ctype_id);
			if($ct->is_enabled())
			{
				$ct->update_turnover($summ);
			}
			else
			{
				self::update_unknown_turnover($summ);
			}
			//
			global   $Direct_Message_Menager;
			if(isset($Direct_Message_Menager))
			{							
				$owner_id				= $ac->get_meta("owner_id"); 
				$owner					= SMC_Location::get_instance($owner_id);				
				$you_transfer_text		= array(
													"post_title"	=> sprintf(__("Your currency transfer report to %s about %d rubles", "smp"), $ac->get("post_title"), $summ), 
													"post_content"	=> sprintf(__("You really transfered a money.<BR><span class='smp-report0'>From currency Account:</span> <span class='smp-report1'>#%d</span><BR><span class='smp-report0'>To currency Account:</span> <span class='smp-report1'>#%d<BR></span><span class='smp-report0'>Summae:</span> <span class='smp-report1'>%d rubles</span><BR><span class='smp-report0'>Date:</span> <span class='smp-report1'>%s</span>", "smp"), $account_id, $payee_id, $summ, get_the_time('j M, Y G:i:s', time()))
												);
				$Direct_Message_Menager->send_to_all_location_owners($owner_id, $you_transfer_text, 4);
			}	
			SMP_Currency::set_report($account_id, -$summ, $reason, $payee_id);	
			return $summ;
		}
		
		static function transfer_to($account_id, $summ, $payer_id="unknown", $reason='input')
		{
			if($summ<0)			return false;
			$ac					= SMP_Account::get_instance($account_id);
			if(!$ac->is_enabled())		return false;
			$count				= $ac->get_meta("count");
			$ost				= $count + $summ;
			$ac->update_meta("count", $ost); 
			//
			$ctype_id			= $ac->get_meta("ctype_id");
			$ct					= SMP_Currency_Type::get_instance($ctype_id);
			if($ct->is_enabled())
			{
				$ct->update_turnover($summ);
			}
			else
			{
				self::update_unknown_turnover($summ);
			}
			//
			global   $Direct_Message_Menager;
			if(isset($Direct_Message_Menager))
			{							
				$owner_id				= $ac->get_meta("owner_id"); 
				$owner					= SMC_Location::get_instance($owner_id);				
				$you_transfer_text		= array(
													"post_title"	=> sprintf(__("Obtained the remittance to %s about %d rubles. Report.", "smp"), $ac->body->post_title, $summ), 
													"post_content"	=> sprintf(__("Credited to your balance: <BR><span class='smp-report0'>From currency Account:</span> <span class='smp-report1'>#%s</span><BR><span class='smp-report0'>To currency Account:</span> <span class='smp-report1'>#%s<BR></span><span class='smp-report0'>Summae:</span> <span class='smp-report1'>%d rubles</span><BR><span class='smp-report0'>Date:</span> <span class='smp-report1'>%s</span>", "smp"), $payer_id, $account_id, $summ, get_the_time('j M, Y G:i:s', time()))
												);
				$Direct_Message_Menager->send_to_all_location_owners($owner_id, $you_transfer_text, 4);
			}
			self::set_report($account_id, $summ, $reason, $payer_id);
			
			return $ost;
		}
		
		static function get_report($account_id)
		{
			$reports					= get_post_meta($account_id, "report", true);
			if(!is_array($reports))		$reports	= array();
			return $reports;
		}
		static function set_report($account_id, $summae, $reason, $partner_account_id)
		{
			$reports					= self::get_report($account_id);
			$reports[]					= array(
													"summae"				=> $summae,
													"reason"				=> $reason,
													"partner_account_id"	=> $partner_account_id,
													"balance"				=> get_post_meta($account_id, "count", true),
													"time"					=> time()
												);
			update_post_meta($account_id, "report", $reports);
		}
		
		static function get_all_user_accounts($user_id=-1)
		{
			global $Soling_Metagame_Constructor;
			if ( !is_user_logged_in() )	return array();
			if($user_id == -1)	$user_id = get_current_user_id();
			$my_locs			= $Soling_Metagame_Constructor->all_user_locations($user_id);
			if(count($my_locs)==0)		return array();
			$meta_query			= array('relation'			=> 'OR');
			
			$arg				= array(
											
											'numberposts'	=> -1,
											'offset'    	=> 0,
											'post_type' 	=> SMP_CURRENCY_ACCOUNT,
											'post_status' 	=> 'publish',
											'meta_query'	=> array(
																		array(
																				'key'		=> 'owner_id',
																				'value'		=> $my_locs,
																				'operator'	=> 'OR'
																			 )
																	),
										);
			return get_posts($arg);
			//echo "<BR>------------<BR>";
			//var_dump($accounts);
		}
		function wp_droplist_currency_type($params=null)
		{
			$args		= array(
									"numberposts"		=> 0,
									"offset"			=> 0,
									'orderby'  			=> 'title',
									'order'     		=> 'ASC',
									'post_type' 		=> SMP_CURRENCY_TYPE,
									'post_status' 		=> 'publish',									
								);
			$hubs		= get_posts($args);
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";
			$DD			= array();
			foreach($hubs as $hub)
			{
				$DD[]	= $hub->ID . "+" . get_post_meta($hub->ID, "rate", true);
				$html	.= "<option value='".$hub->ID."' ".selected($hub->ID, $params['selected'], false).">".$hub->post_title."</option>";
			}
			$html		.= "</select><div ct_data rates='-1+0,". implode(",", $DD)."'></div>";
			return $html;
		}
		public function get_locations_accounts_id($loc_id, $currency_type_id=-1)
		{
			$meta_query		= array(
										'relation'			=> 'AND',
										array(
												'key'		=> 'owner_id',
												'value'		=> $loc_id,
												'compare'	=> '='
											  ),
										array( 'relation'	=> 'OR',
															array(
																	'key'		=> 'is_lock',
																	'value'		=> 0,
																	'compare'	=> '='
																  ),
															array(
																	'key'		=> 'is_lock',
																	'compare'	=> 'NOT EXISTS'
																  )
											  )
									 );
			if($currency_type_id != -1)
			{
				$meta_query[]	= array(	
											'key'			=> "ctype_id",
											'value'			=> $currency_type_id,
											'compare'		=> '='
										);
			}
			$args		= array(
									"numberposts"		=> -1,
									"offset"			=> 0,
									'orderby'  			=> 'title',
									'order'     		=> 'ASC',
									'post_type' 		=> SMP_CURRENCY_ACCOUNT,
									'post_status' 		=> 'publish',	
									'fields' 			=> 'ids',	
									'meta_query'		=> $meta_query,
								);
			//insertLog("get_locations_accounts_id", $args);
			$accounts	= get_posts($args);
			//insertLog("get_locations_accounts_id", $accounts);
			return $accounts;
		}
		
		function wp_droplist_accounts($params=null)
		{
			global $Soling_Metagame_Constructor;
			$args		= array(
									"numberposts"		=> -1,
									"offset"			=> 0,
									'orderby'  			=> 'title',
									'order'     		=> 'ASC',
									'post_type' 		=> SMP_CURRENCY_ACCOUNT,
									'post_status' 		=> 'publish',									
								);
			if(isset($params['user_id']))
			{
				$ow_locations_ids				= $Soling_Metagame_Constructor->all_user_locations($params['user_id']);
				if(count($ow_locations_ids)==0 || !is_user_logged_in())	return __("You have nothing!", "smp");
				$meta_query						= array();
				$meta_query['relation']			= 'OR';
				foreach($ow_locations_ids as $idd)
				{
					$arr						= array();
					$arr['value']				= $idd;
					$arr['key']					= 'owner_id';
					$arr['compare']				= '='; 
					array_push($meta_query, $arr);
				}
				$args['meta_query']				= $meta_query;
			}
			$hubs		= get_posts($args);
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";
			
			foreach($hubs as $hub)
			{
				$html	.= "<option value='".$hub->ID."' ".selected($hub->ID, $params['selected'], false).">".$hub->post_title."</option>";
			}
			$html		.= "</select>";
			return $html;
		}
		
		
		function get_location_accounts($location_id=0, $is_unlocked=1)
		{
			global $Soling_Metagame_Constructor;			
			$meta_query						= array();	
			$meta_query['relation']			= 'AND';
				$arr						= array();
				$arr['value']				= $location_id;
				$arr['key']					= 'owner_id';
				$arr['compare']				= '='; 
				array_push($meta_query, $arr);		
				
				$arr						= array();
				$arr['value']				= !$is_unlocked;
				$arr['key']					= 'is_lock';
				$arr['compare']				= '='; 
				array_push($meta_query, $arr);		
			
			$ar				= array(										
											'numberposts'	=> -1,
											'offset'    	=> 0,
											'orderby'  		=> 'id',
											'order'     	=> 'DESC',
											'post_type' 	=> SMP_CURRENCY_ACCOUNT,
											'post_status' 	=> 'publish',
											'meta_query' 	=> $meta_query
										);
			$location_accounts				= get_posts($ar);
			return $location_accounts;
		}
		function get_user_accounts($user_id=0, $is_unlocked=1)
		{
			global $Soling_Metagame_Constructor;
			global $users_smp_accounts;
			//if(isset($users_smp_accounts) && $user_id==0) return $users_smp_accounts;
			if(!is_user_logged_in())		return array();
			$ow_locations_ids				= $Soling_Metagame_Constructor->all_user_locations($user_id);
			if(count($ow_locations_ids)==0)	
				return array();
			$meta_query						= array();
			$meta_query['relation']			= 'OR';
			foreach($ow_locations_ids as $idd)
			{
				$arr						= array();
				$arr['value']				= $idd;
				$arr['key']					= 'owner_id';
				$arr['compare']				= '='; 
				array_push($meta_query, $arr);
			}
			
			$ar				= array(										
											'numberposts'	=> -1,
											'offset'    	=> 0,
											'orderby'  		=> 'id',
											'order'     	=> 'DESC',
											'post_type' 	=> SMP_CURRENCY_ACCOUNT,
											'post_status' 	=> 'publish',
											'meta_query' 	=> $meta_query
										);
			$users_smp_accounts				= get_posts($ar);
			return $users_smp_accounts;
		}
		
		function my_accounts_form()
		{
			$accnts					= '';		
			$currency_accounts		= $this->get_user_accounts();			
			$accnts				.= "
			<div id='col1' class='smp-cl1'>";
			$i=10;
			foreach($currency_accounts as $ca)
			{
				$accnts			.=  "<div class='smp-pr-title' style='text-align:right;' id='production-btn-" . $ca->ID . "' button_id=" . $i++ . "># " . $ca->post_title . "<span id='smp-pr-title_summ_". $ca->ID ."' class='smp-colorized' style='font-weight:700;'> (".get_post_meta($ca->ID, "count", true).")</span></div>";
			}
			$accnts				.= "
				</div>
				<div id='col2' class='smp-cl2'>";
				
			$ii = 10;
			foreach($currency_accounts as $ca)
			{			
				$count			= get_post_meta($ca->ID, "count", true);
				$ctype_id		= get_post_meta($ca->ID, "ctype_id", true);
				$accnts			.= "<div class='smp-pr-main' id='production-".$ca->ID."' button_id='".$ii++."' factory_id='".$ca->ID."' style=''>";
				$accnts			.= '<h3>'. __("Currency Account", "smp").' <span style=\'font-weight:700; color:'.$user_iface_color.'!important\'>'. $ca->post_title.'</span></h3>';
				$accnt			= '
				<div>
					<!--input type="text" class="payee_id" num_ca="'.$ca->ID.'" name="ca_'.$ca->ID.'" value="" style="width:100%;" placeholder="'.__("Set number of Currency Account for remittance", "smp").'"/-->
					<span>' . __("Set number of Currency Account for remittance", "smp")."</span>".
					SMP_Account::wp_dropdown( array("class"=>"payee_id", "style"=>"width:100%; padding:10px 20px;", "args"=>array("ctype_id"=>$ctype_id) )).
				'</div>
				<!--div style="margin-bottom:10px;">'.
					__("Current count: ", "smp")." <span style='font-family:Open Sans Condensed; font-size:20px; font-weight:900;' id='ccount-".$ca->ID."'>0". '</span><span> $</span>
				</div-->
				<div>
					<span style="margin-top:20px; display:block;">' . __("Set summ", "smp") . '</span>
					<input type="range" class="smc_vertical_align" style="width:57%;" name="currency_'.$ca->ID.'"  id="currency_'.$ca->ID.'" count_id="' . $ca->ID . '" max="'.$count.'" value="0" n="'.$i.'" ttg="cur_nm_'.$ca->ID.'"/>
					<input type="number" class="smc_vertical_align" style="width:37%;" name="cur_nm_'.$ca->ID.'"   id="cur_nm_'.$ca->ID.'"   count_id="' . $ca->ID . '" max="'.$count.'" min="0" ttg="currency_'.$ca->ID.'" />
				</div>
				<div>
					<span style="margin-top:20px; display:block;">' . __("Set reason", "smp") . '</span>
					<textarea name="reason" for_acc_id="' . $ca->ID . '">
					
					</textarea>
				</div>
				<div style="margin-top:20px; display:block;">
					<span class="button transfer_button" ca_id="'.$ca->ID.'">'.__("Start tranfer", "smp").'</span>
				</div>';
				$accnts			.= $accnt;
				$accnts			.= '</div>';	
			}
			return $accnts;
		}
		static function check_summae($account_id, $summ)
		{
			return get_post_meta($account_id, "count", true);
		}
		
		static function get_currency_type($summae, $currency_type_id =- 1)
		{
			if($currency_type_id==-1)
			{
				$currency_type_id		= $this->options['default_currency_type_id'];
			}
			$rate						= get_post_meta($currency_type_id, "rate", true);
			$abbreviation				= get_post_meta($currency_type_id, "abbreviation", true);
			return array('summae'=>$summae*$rate, "abbreviation"=>$abbreviation);
		}
		static function get_default_currency_type()
		{
			global $default_currency_type;
			if( !isset( $default_currency_type ) )
				$default_currency_type	= get_post( $this->options[ 'default_currency_type_id' ] );
			return $default_currency_type;
		}	
		static function by_location($summae, $location_id)
		{
			$term_meta					= SMC_Location::get_term_meta($location_id);
			$currency_type_id			= $term_meta['currency_type'];
			return self::get_currency_type($summae, $currency_type_id);
		}
		function smp_my_tools($args)
		{
			if(!$this->options['finance_enabled']) 	return $args;
			$fin	= "<h1>" . __("Finance Statistics", "smp")."</h1>";
			$curen	= "<h1>".__("Currency circulation", "smp"). "</h1>".SMP_Currency_Type::get_currency_statistics();
			$markets= "<div class='smp-comment'>" . __("Этой фичи ещё нет", "smp") . "</div>";
			$fin	.= Assistants::get_switcher(
													apply_filters("smp_finance_statistics",  
																array(
																		array("title" => __("Currency Analitics", "smp"), "slide" => $curen),
																		array("title" => __("Markets", "smp"), "slide" => $markets),
																	 )
														 ),
													"finst"
												);
			//$fin	.= "<div class=smp-comment>".__("This is adult tool for calculate you personal needs per current Playing Circle. ", "smp"). "</div>";
			
			$args[]	= array("title" => "<div class='smp_tool_icon'><img src='" . SMP_URLPATH . "img/stat_ico_op.png'></div>", "hint" => __("Finance Statistics", "smp"), "slide" => $fin);
			return $args;
		}
		
		//Factory
		 function my_extra_fields_factory($post_id)
		{
			global $SMP_Currency;
			$ct					= Factory::get_factory($post_id);
			$ct_meta			= $ct->get_post_meta("cost_price");
			//$ct				= SMP_Currency_Type::get_instance($post_id);
			$currency_type_id	= $ct->get_post_meta("currency_type_id");
			?>
				<div class="h0">
					<div class='h'>
						<label  class="h2" for="currency_type_id"><?php _e("Default Currency Type", "smp"); ?></label><br>
						<?php echo $SMP_Currency->wp_droplist_currency_type(array("name"=>"currency_type_id", "id"=>"currency_type_id", "class"=>"chosen-select h2", "selected"=>$currency_type_id)) ?>
					</div>
					<div class='h'>
						<label  class="h2" for="cost_price"><?php _e("Amortization", "smp"); ?></label><br>
						<input type='number' min=0 name='cost_price' class='h2' value="<?php echo $ct_meta; ?>"/>
					</div>
				</div>
				
			<?php
		}
		 function true_save_box_data_factory ( $post_id ) 
		{
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;			
			update_post_meta($post_id, 'cost_price', 		(INT)$_POST['cost_price']);
			update_post_meta($post_id, 'currency_type_id', 	$_POST['currency_type_id']);					
			return $post_id;
		}
		 function add_views_column_factory( $columns )
		{
			$columns['cost_price'] = __("Amortization", "smp");
			return 	$columns;	
		}
		 function add_views_sortable_column_factory($sortable_columns){
			$sortable_columns['cost_price'] 			= 'cost_price';
			return $sortable_columns;
		}	
		 // заполняем колонку данными	
		function fill_views_column_factory($column_name, $post_id)
		{
			switch( $column_name) 
			{				
				case 'cost_price':
					echo get_post_meta($post_id, 'cost_price', 1);
					break;
			}		
		}
		
		function get_price($currency_type_id, $summae_ue, $summ_class="", $rounded=true)
		{
			if($currency_type_id == "" || !isset($currency_type_id) || $currency_type_id == -1)
				return "<st class='".$summ_class."'>". $summae_ue."</st> " . "<img class='coin' src='" . SMP_URLPATH ."icon/UE.png'/>";
			$currency_type		= SMP_Currency_Type::get_instance($currency_type_id);
			return $currency_type->get_price($summae_ue, $summ_class, $rounded);
		}
		
		
		function smc_myajax_submit($params)
		{
			global $user_iface_color;
			global $Soling_Metagame_Constructor;
			global $SMP_Currency;
			global $SMC_Invoice_Manager;
			global $start;
			$start		= getmicrotime();
			
			switch($params[0])
			{
				case "get_outers__":
					$d					= array(	
													$params[0], 
													array(
															'text' 			=> $this->my_accounts_form(),
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'my_currency_accounts':					
					$d					= array(	
													'my_currency_accounts', 
													array(
															'text' 			=> $this->my_accounts_form(),
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "new_transfer":
				
					$p_id			= $params[1];
					$n_account_id	= $params[2];
					$summ			= (int)$params[3];
					$reason			= $params[4];
					if($summ < 0)
					{
						print json_encode(array($params[0], array("text"=>"error summ")));
						break;
					}
					$n_account		= get_post($n_account_id);
					
					if($n_account->post_type==SMP_CURRENCY_ACCOUNT)
					{
						
						if(self::is_owner(get_current_user_id(), $p_id ))
						{
							$ost			= self::transfer_from($p_id, $summ, $n_account_id, $reason);
							if(!$ost)		continue;
							self::transfer_to($n_account_id, $ost, $p_id, $reason);
							$comm_visible	.= "block";
							$serv			.= __("Transfer complete successful.", 'smp');							
						}
					}
					else
					{
						$comm_visible		.= "block";
						$serv				.= __("This Number of account not present", 'smp');
						$class				.= "lp-comment-error";
					}
					
					$d					= array(	
													'new_transfer', 
													array(
															'text' 			=> $serv,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
			}
		}
		
		function smp_factory_get_need($arr, $factory_id)
		{
			global $GoodsType;
			$factory					= Factory::get_factory($factory_id);
			$owid						= $factory->get_owner_id();
			$ctid						= $factory->get_currency_type();
			$av_summ					= SMP_Account::see_summ_location($owid, $ctid);
			$fact_cost_price			= $factory->get_post_meta("cost_price");
			$gtid						= $factory->get_post_meta("goods_type_id");
			$goods_price				= (int)$GoodsType->get_price( $gtid);
			$summ_produce				= $fact_cost_price + (int)( $factory->get_productivity() * $goods_price);
			$summ						= $av_summ - $summ_produce; 
			if($summ<0)
			{
				$ct						= SMP_Currency_Type::get_instance( $ctid );
				$arr[]					= array(
													'goods_type'	=> $ct->get("post_title"),
													"value"			=> $ct->convert($summ), 
													"av" 			=> $ct->convert($av_summ),
													"currency_type"	=> $ctid,
													
												);
			}
			return $arr;
		}
		function smp_factory_rize_need($bool, $factory_id)
		{
			//return true;
			global $GoodsType;
			$factory					= Factory::get_factory($factory_id);
			$fact_cost_price			= $factory->get_post_meta("cost_price");
			$goods_price				= $GoodsType->get_price( $factory->get_post_meta("goods_type_id"));
			$summ_produce				= $fact_cost_price + (int)( $factory->get_productivity() * $goods_price);
			$access						= SMP_Account::remove_summ_from_location(
																					$factory->get_post_meta("owner_id"), 
																					$factory->get_post_meta("currency_type_id"), 
																					(int)$summ_produce,
																					sprintf(__("Auto payment for produce in %s", "smp"), $factory->body->post_title)
																				 );
			if(!$access)			echo sprintf(__("write off %s on production needs.", "smp"), "<b>".$this->get_price($factory->get_post_meta( "currency_type_id" ), $summ_produce) . "</b>");
			return $access;
		}
		function smp_factory_price_production( $args, $factory_id )
		{
			global $GoodsType;
			$factory				= Factory::get_factory( $factory_id );			
			//$cost_price			= $factory->get_post_meta( "cost_price" );					
			$price					= $GoodsType->get_price( $factory->get_post_meta( "goods_type_id") );
			$ct						= SMP_Currency_Type::get_instance( $factory->get_post_meta( "currency_type_id" ) );	
			if($ct->id!=-1 && !is_bool($ct))
			{
				$summae					= $ct->convert($price);
				return array ( "summae" =>$summae, "abbreviation" =>  $ct->get_meta("abbreviation"));
			}
			else
			{
				return array ( "summae" =>$price, "abbreviation" =>  "<img class='coin' src='" . SMP_URLPATH ."icon/UE.png'/>" );
			}
			
		}
		function smp_factory_form_info($text, $factory_id)
		{	
			global $Soling_Metagame_Constructor, $GoodsType;
			$factory				= Factory::get_factory( $factory_id );	
			$fact_cost_price		= $factory->get_post_meta("cost_price");
			$ct						= SMP_Currency_Type::get_instance( $factory->get_post_meta( "currency_type_id" ) );	
			
			$goods_price			= $GoodsType->get_price( $factory->get_post_meta("goods_type_id"));
			$summ_produce			= $fact_cost_price + (int)( $factory->get_productivity() * $goods_price);
			
			
			return $text.'
			<div class="smp-pr-cur-good">
				<span class="smp-table1">'.
					$Soling_Metagame_Constructor->assistants->get_hint_helper(__("Cost Price for working this factory.", "smp")).__("Costs", "smp"). 
				'</span>
				<span class="smp-goods-1">'.
					'<span class="smp_power_cnnt">' . $ct->convert($fact_cost_price) . " " . $ct->get_meta("abbreviation") ."</span> " .
				'</span>
			</div>
			<div class="smp-pr-cur-good">
				<span class="smp-table1">'.
					$Soling_Metagame_Constructor->assistants->get_hint_helper(__("Full Cost for working factory and Cost Prices for Goods Type Produse.", "smp")).__("Full costs", "smp"). 
				'</span>
				<span class="smp-goods-1">'.
					'<span class="smp_power_cnnt">' . $ct->convert($summ_produce) . " " . $ct->get_meta("abbreviation") ."</span> " .
				'</span>
			</div>';
		}
		function smp_factory_add_batch($arr, $goods_batch_type, $factory_id)
		{
			insertLog("SMP_Currency_Type.smp_factory_add_batch", $factory_id);
			$factory					= Factory::get_factory($factory_id);
			$ctd						= $factory->get_currency_type();
			$arr['currency_type_id']	= $ctd;
			return $arr;
		}
		function smp_batch_card_price($text, $goods_batch_id)
		{
			$price						= (int)get_post_meta($goods_batch_id, "price", true);
			$count						= get_post_meta($goods_batch_id, "count", true);
			$currency_type_id			= get_post_meta($goods_batch_id, "currency_type_id", true);
			if($currency_type_id=="" || $price == 0)	
				return $text;
			$curr						= SMP_Currency_Type::get_instance($currency_type_id);
			return isset($curr) ? __("Price", "smp"). " <span>" . $curr->convert($price). "/" . $curr->convert($price * $count) . "</span> " . $curr->get_abbreviation() : $text;
		}
		
		static function update_unknown_turnover($summ)
		{
			$turnover					= (int)get_option("unknown_turnover");
			update_option("unknown_turnover", $turnover + $summ);
			return $turnover + $summ;
		}
	}
?>