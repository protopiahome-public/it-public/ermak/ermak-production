<?php
	class SMP_Routh
	{
		public $id;
		public $data;
		public $option;
		public $type;
		
		
		
		function __construct($id)
		{
			$this->id				= $id;
			$this->data				= get_term($id, "smp_routh");
			$this->option			= get_option("smp_routh_$id");
		}
		static function get_term_meta($id)
		{
			$routh					= self::get_instance($id);
			return $routh->option;
		}
		static function update_term_meta($id, $term_array, $is_insert=false)
		{
			update_option("smp_routh_$id", $term_array);
			//$this->option			= $term_array;
		}
		static function get_instance($id)
		{
			global $routes;			
			if(!isset($routes))	$routes	= array();
			if(!$routes[$id])
			{
				$routes[$id]		= new SMP_Routh($id);
			}
			return $routes[$id];
		}
		static function get_routh_speed($routh_data)
		{
			// id or smp_routh_$id
			if(is_array($routh_data))
			{
				return $routh_data['speed'];
			}
			else
			{
				$r			= get_option("smp_routh_$routh_data");
				return $r['speed'];
			}
			
		}
		static function get_all_rouths($fields="all")
		{
			global $all_rouths;
			if($all_rouths)		return $all_rouths;
			$args				= array(
											 'number' 		=> 1000
											,'offset' 		=> 0
											,'orderby' 		=> 'name'
											,"hide_empty"	=> false
											,'fields'		=> $fields
										);
			$all_rouths			= get_terms('smp_routh', $args);
			return $all_rouths;
		}
		static function wp_dropdown_rouths($params=null)
		{
			global $hubs_rouths;
			global $hub_rouths;
			$hub_rouths	= array();
			//if(!isset($hubs_rouths))		
			$hubs_rouths = array();
			$params		= isset($params)	? $params : array("name"=>"routh_");
			$args		= array(
									 'number' 		=> 0
									,'offset' 		=> 0
									,'orderby' 		=> 'name'
									,"hide_empty"	=> false									
								);
			if($params['include'])
				$args["post__in"]	= $params['include'];
			
			$rouths		= get_terms('smp_routh', $args);
			$p			= "<select ";
			if(isset($params['id']))
				$p		.= 'id="'.$params['id'].'" ';
			if(isset($params['name']))
				$p		.= 'name="'.$params['name'].'" ';
			if(isset($params['class']))
				$p		.= 'class="'.$params['class'].'" ';
			if(isset($params['style']))
				$p		.= 'style='.$params['style'].'" ';		
			if(isset($params['multile']))
				$p		.= 'multiple ';
			$p			.= '><option value="-1"> -- </option>';
			foreach($rouths as $routh)
			{			
				if($params['hub_id'])
				{
					$op	= get_option("smp_routh_".$routh->term_id);
					$gg	= false;
					foreach($params['hub_id'] as $hub_id)
					{
						if($op['start_hub_id'] 	== $hub_id || $op['finish_hub_id'] == $hub_id)
						{
							$gg 				= true;
							$hubs_rouths[]		= $routh;
							$hub_rouths[]		= $routh;
							break;
						}
					}
					if(!$gg)	continue;
				}
				//$h		 = array_unique($hubs_rouths);
				//$hubs_rouths = $h;
				$p		.= "<option value='" . $routh->term_id . "' " . selected($routh->term_id, $params['selected'], false) . " >" .  $routh->name. "</option>";
			}
			$p			.= '</select>';
			return $p;
		}
		static function wp_dropdown_hub_rouths($disl_id, $params)
		{			
			$arg			= array(
										'numberposts'	=> 0,
										'offset'    	=> 0,
										'orderby'  		=> 'id',
										'order'     	=> 'DESC',
										'post_type' 	=> 'smp_hub',
										'post_status' 	=> 'publish',
										'meta_query' 	=> array(
																	array(
																			"key"			=> "dislocation_id",
																			"value"			=> $disl_id,
																			"compare"		=> "="
																		  ),
																 ),
									);
			$hubs				= get_posts($arg);
			$hub_ids			= array();
			foreach($hubs as $hub)
				$hub_ids[]		= $hub->ID;
			$params["hub_id"]	= $hub_ids;
			return SMP_Routh::wp_dropdown_rouths( $params );
		}
		function get_option()
		{
			if(!isset($this->option))
				$this->option = get_option("smp_routh_".$this->id);
			return $this->option;
		}
		function get_type()
		{
			if(!isset($this->type))
			{
				$o				= $this->get_option();
				$this->type 	= get_post($o['type']);
			}
			return $this->type;
		}
		public function draw()
		{
			$routh_data	= $this->get_option();
			$geometry	= $routh_data['geometry'];
			$type_id	= $routh_data['type'];
			$color		= get_post_meta($type_id, "color", true);
			$during		= (int)$routh_data['speed'];
			$capacity	= (int)$routh_data['capacity'];
			$dot		= $geometry ? $this->get_center($geometry, $this->data->slug, $this->data->name, $color, $during, $type_id, $capacity) : "";
			if(!$color)	$color = '000000';
			$html		= '			
				<g>
					<path svg_elem stroke="#' . $color . '" class="smp_routh_geometry1" stroke-opacity="1" stroke-dasharray="9,2" stroke-width="2" fill-opacity="0" id="svg_'.$this->id.'" d="'.$geometry.'"/>
				</g>';
			return array($html, $dot);
		}		
		function get_center($geometry, $slug, $name, $color, $during, $type_id, $capacity)
		{
			global $SolingMetagameProduction;
			$type		= get_post($type_id);
			$g			= preg_replace('/ L/', ",", $geometry);
			$g			= preg_replace('/M/', "", $g);
			$g_array	= explode(',',$g);
			$g_array	= array_filter($g_array, array($this, 'not_empty'));
			$cargo		= $this->is_cargoed();
			$batches	= "<table style='width:100%; dispaly:inline-block; position:relative;'><tr><td style='background:#333; padding:2px;'>";
			$bid		= 	"";
			$batch_ids	= array();
			$y=0;
			$nbatches	= "			
				
						<div style='color:#EFEFEF; padding:1px 4px;'>" . __("Wait permissions", "smp")."</div>";
			foreach($cargo as $batch)
			{
				$b			= SMP_Goods_Batch::get_picto($batch);
				$bb			= SMP_Goods_Batch::get_instance($batch);
				if($bb->get_meta("is_permission"))
				{
					$finish_time= $bb->get_meta("finish_time");
					$left		= (int)(($finish_time - time()));					
					array_push($batch_ids, $batch. "+".$left);
					$batches	.= $b;
				}
				else
				{
					$y++;
					//array_push($batch_ids, $batch. "+".$left);
					$nbatches	.= $b;
				}				
			}
			$batches	.= "</td></tr>";
			if($y>0)
				$batches	.= "<tr><td style='background:#111; padding:2px; '>$nbatches</td></tr>";
			$batches	.= "</table>";
			if(count($batch_ids))
				$bid	= "route_batchs=". implode(",", $batch_ids);
			$data_hint	= 
			"<div id='routhe_help_".$this->id."' class='lp-hide' style='width:260px;'>
				<center>
					<div style='font-size:15px; text-transform: uppercase; line-height:2; font-family:Open Sans Condensed,Arial; '>" . $type->post_title . "</div>
					<B>". $name . "</b><BR>" . 
					__("Speed", "smp"). " - " .  $during . " " . __("minuts", "smp"). "<br>".
					__("Capacity", "smp"). " - ". $capacity. " " . __("unit", "smp"). 
					$batches.
				"</center>
			</div>";
			$cnt 		= count($g_array);
			switch(true)
			{
				case $cnt == 4:
					$xx	= $g_array[0] + ($g_array[2] - $g_array[0])/2;
					$yy	= $g_array[1] + ($g_array[3] - $g_array[1])/2;
					break;
				case $cnt < 7:
					$xx = $g_array[2];
					$yy = $g_array[3];
					break;
				case $cnt < 12:
					$xx	= $g_array[4];
					$yy	= $g_array[5];
					break;
				case $cnt < 4:
					$xx	= -100;
					$yy	= -110;
					break;
				default:
					$xx	= $g_array[6];
					$yy	= $g_array[7];
				
			}
			$cargo_class	=	count($cargo) > 0 ? " is_cargoed"  : "";
			//return  '<circle  svg_elem smp_route="' . $g . '" cx="' . $xx . '" cy="' . $yy . '" r="6" width="6"  height="6" href="/?smp_routh=' . $slug . '" class="lp_eventer smp_route_info hinter" data-hint="'. $data_hint .'" fill="#'.$color.'" rid='.$this->id.'/>';
			$radius		= $SolingMetagameProduction->options['route_hint_radius'];
			return '<div '.$bid.' href="' . get_term_link((int)$this->id, 'smp_routh') . '" class="div_hinter lp_eventer smp_route_info '.$cargo_class.' " data-hint="routhe_help_' . $this->id . '" rid="'.$this->id.'" style="top: ' . ($yy - $radius/2) . 'px; left:' . ($xx - $radius/2) . 'px; background-color:#'.$color.'; font-size:'.$radius.'px;">'  . $during . '</div>'.$data_hint;
		}
		public function get_panel_form()
		{
			$cur_term		= get_term_by("name", single_term_title("",0), "location");
			$trum_img		= SMP_URLPATH."img/route.jpg"; 
			return "open_metagame_panel('special_filtered_panel', '" . 
			"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img);><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
			__("Routh", "smp") . "</div></center></div>')";
		}
		function is_cargoed()
		{
			
			$args		= array(
									"numberposts"		=> -1,
									"offset"			=> 0,
									'orderby'  			=> 'ID',
									'order'     		=> 'ASC',
									'post_type' 		=> GOODS_BATCH_NAME,
									'post_status' 		=> 'publish',
									'fields'			=> "ids",
									'meta_query'		=> array(
																	'relation'			=> 'AND',
																	array(
																			"key"		=> "transportation_id",
																			"value"		=> $this->id,
																			"compare"	=> "="
																		),
																		/*
																	array(
																			"key"		=> "is_permission",
																			"value"		=> true,
																			"compare"	=> "="
																		)
																		*/
																)
								);
			$batches		= get_posts($args);
			return $batches;
			/*
			global 	$wpdb;
			$resutl 	= $wpdb->get_results("
			SELECT post_id FROM `" . $wpdb->prefix."postmeta` WHERE meta_key='transportation_id' AND meta_value=".$this->id);
			return $resutl;
			*/
		}
		function not_empty($var)
		{
			return  $var !== "";
		}
		
		static function install()
		{
			$my_post = array(
								  'post_title'   		=> __("My Rouths", "smp"),
								  'post_type' 			=> 'page',
								  'post_content' 		=> "[smp_my_routes]"  ,
								  'post_status'  		=> 'publish',
								  'comment_status'		=> 'closed',
								);

			return wp_insert_post( $my_post ); 
		}
		function get_routh_hub_ids()
		{
			return array($this->option["start_hub_id"], $this->option["finish_hub_id"]);
		}
		static function get_all_rouths_by_hub($hub_id, $fields='ids')
		{
			global $all_rouths;
			$all_rouths			= self::get_all_rouths($fields);			
			$hub_rouths			= array();
			foreach($all_rouths as $routh)
			{
				$routh_id		= !is_object($routh) ? $routh : $routh->term_id;
				$meta			= get_option("smp_routh_".$routh_id);
				//insertLog("get_all_rouths_by_hub", $meta);
				if($hub_id == $meta['start_hub_id'] || $hub_id == $meta['finish_hub_id'])
				{
					$hub_rouths[] = $routh;
				}
			}
			//insertLog("get_all_rouths_by_hub", $hub_rouths);
			return $hub_rouths;
		}
		//ROUTE Type
		static function dropdown_routh_type($params)
		{
			$args		= array(
									"numberposts"		=> -1,
									"offset"			=> 0,
									'orderby'  			=> 'title',
									'order'     		=> 'ASC',
									'post_type' 		=> 'smp_route_type',
									'post_status' 		=> 'publish',									
								);
			$hubs		= get_posts($args);
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";
			
			foreach($hubs as $hub)
			{
				$html	.= "<option value='".$hub->ID."' ".selected($hub->ID, $params['selected'], false).">".$hub->post_title."</option>";
			}
			$html		.= "</select>";
			return $html;
		}
		function get_picto()
		{
			$tl			= get_term_link((int)$this->id, 'smp_routh') ;
			return "
			<div smp_route_p rid='" . $this->id . "' href='$tl'  class='smp_route_picto'><span>".get_post($this->option['type'])->post_title."</span>
			".
				$this->data->name.
			"</div>";
		}
		static function get_widget_form($routh_object)
		{
			global $Soling_Metagame_Constructor, $user_iface_color, $moveble_types;
			//$op		= get_option("smp_routh_".$routh_object->id);
			$op			= $routh_object->option;
			
			//all goods batchs
			$args		= array(
										'numberposts'	=> 0,
										'offset'    	=> 0,
										'orderby'  		=> 'id',
										'order'     	=> 'DESC',
										'post_type' 	=> $moveble_types,
										'post_status' 	=> 'publish',
										'meta_query' 	=> array(
																	'relation'		=> 'AND',
																	array(
																			"key"		=> "transportation_id",
																			"value"		=> $routh_object->id,
																		),
																	array(
																			"key"		=> "is_permission",
																			"value"		=> true,
																		),
																),
								);
			$batches	= get_posts($args);
			
			
			$html		= "
			<div id='route_widget_form".$routh_object->id."' titled='".$routh_object->data->name."' style='display:block;'>";
			$html		.= "<div id='route_".$routh_object->id."' style='position:relative; display:block;'>";
			$html		.= "<div class='klapan3_subtitle'>".__("all Hubs", "smp")."</div><div class='smp_bevel_form'>";
			$html		.= SMP_Hub::get_pictogramm($op['start_hub_id']).SMP_Hub::get_pictogramm($op['finish_hub_id']);
			$html		.= "</div><div class='lp_clapan_raze'>
					<div class='klapan3_subtitle'>".
						__("Capacity", "smp").
				'	</div>
					<div class="smp_bevel_form">
						<div class="lp-button-2 lp-inline-block lp-padding-block lp_pointer">'.
							(int)$op['capacity'].
					'	</div>
					</div>
				</div>';
			$html		.= "<div class='lp_clapan_raze'>
					<div class='klapan3_subtitle'>".
						__("Cost", "smp").
				'	</div>
					<div class="smp_bevel_form">
						<div class="lp-button-2 lp-inline-block lp-padding-block lp_pointer">'.
							(int)$op['cost'].
						'</div>
					</div>
				</div>';
			$html		.= "<div class='lp_clapan_raze'>
					<div class='klapan3_subtitle'>".
						__("Speed", "smp").
				'	</div>
					<div class="smp_bevel_form">
						<div class="lp-button-2 lp-inline-block lp-padding-block lp_pointer">'.
							$op['speed'] . " " . __(" minutes", "smp") .
						'</div>
					</div>
				</div>';
			$html		.= "<div class='lp_clapan_raze'>
					<div class='klapan3_subtitle'>".
						__("all Goods batches", "smp").
				'	</div>			
					<div class="smp_bevel_form">';
			foreach($batches as $batch)
			{
				$gb			= SMP_Goods_Batch::get_instance($batch->ID);
				$html		.= $gb->get_widget_picto(2, false, array("your_bottom"=>2, "your_right"=>-10, "size"=>40));
			}
			
			
			
			$html		.= '</div>';
			$html		.= "</div></div>";
			return $html;
		}
	}
?>