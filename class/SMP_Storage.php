<?php
	class SMP_Storage
	{
		function __construct()
		{
			
		}
		function draw($batches, $max=20, $offset, $all_offsets, $object_id, $object_type='SMC_Location')
		{
			global $smc_height, $SolingMetagameProduction;
			switch($object_type)
			{
				case "SMC_Location":
					$location_id		= $object_id;
					break;
				case "Factory":
					$location_id		= get_post_meta($object_id, "dislocation_id", true);
					break;
			}
			
			$h				= $smc_height > 370 ? ($smc_height-195) : 270;
			$html			= "
				<style>
					.storage_container
					{
						background:url(". ( (int)$SolingMetagameProduction->options['storage_design'] == 1 ? SMP_URLPATH."/img/storage_fon2.jpg " : SMP_URLPATH."/img/storage_fon.jpg" ) ."); 
					}
				</style>
				<div class='storage_container' style='height:" . $h . "px; width:384px; ' id='st_angar_$offset'>
				<svg width='0px' height='0px' viewBox='0 0 0 0' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>
					<defs>
					  <pattern id='rja' patternUnits='userSpaceOnUse' width='110' height='60'>
						<image xlink:href='" . SMP_URLPATH . "img/rja.jpg' x='0' y='0' width='110' height='60' />
					  </pattern>
					</defs>
				</svg>";
			$html			.= count($batches) < 1 ? "<div class='smp_comment_widget'>".__("The storage is empty", "smp")."</div>" : "";
			$matrix			= $this->get_matrix();
			$nn=0;
			$mm = 0;
			foreach($batches as $batch)
			{
				++$nn;
				if($nn	< $max * $offset)	continue;
				$mm++;
				$batch1		= Batch::get_instance($batch);
				if( $batch1->get_meta( "is_blocked") != 1 )
				{
					$pos		= current($matrix);
					$html		.= $batch1->draw_container($pos);
				}
				next($matrix);
				if($nn > $max * (1 + $offset))	break;
			}
			if($all_offsets>0)
			{
				$html		.= "<div class='stor_tabs' locid='$object_id' obtype='$object_type'>";
				for($i=0; $i<$all_offsets; $i++)
				{
					$class		= $i== $offset ? "cur_stor_tab" : "stor_tab";
					$angar		= $i== $offset ? " <span style='font-size:0.75em; font-weight:normal;'>" . __("angar", "smp") ."</span>" : "";
					$html		.= "<div class='$class' i=$i>" . ($i + 1). $angar ."</div>";
				}
				$html		.= "</div>";
			}
			$html			.= "
					<div class='storage_car'></div>
					<div class='storage_cnt_btchs hint hint--top' data-hint='" . sprintf(__("%s goods batchs in this angar", "smp"), $mm) . "'>".$mm."</div>
					<div id='storage_search_form'>".
						SMP_Assistants::search_goods_batch_form($location_id, false) .
				"	</div>
				</div>";
			return $html;
		}
		function get_matrix()
		{
			return array(
								array("x" => 320,		"y" => 34,		"f"=>1,	'o' => 0),
									array("x" => 266,	"y" => -46,		"f"=>0,	'o' => 02),
									array("x" => 300,	"y" => -30,		"f"=>0,	'o' => 03),
									array("x" => 352,	"y" => 56,		"f"=>0,	'o' => 6),
									array("x" => 226,	"y" => 116,		"f"=>0,	'o' => 7),
									array("x" => 256,	"y" => 132,		"f"=>0,	'o' => 8),
									array("x" => 256,	"y" => 104,		"f"=>0,	'o' => 81),
									array("x" => 288,	"y" => 148,		"f"=>0,	'o' => 9),
								array("x" => 288,		"y" => 52,		"f"=>1,	'o' => 1),
								array("x" => 256,		"y" => 70,		"f"=>1,	'o' => 2),
									array("x" => 256,	"y" => 42,		"f"=>1,	'o' => 21),
									array("x" => 320,	"y" => 12,		"f"=>1,	'o' => 01),
								array("x" => 224,		"y" => 88,		"f"=>1,	'o' => 3),								
								array("x" => 10,		"y" => 70,		"f"=>0,	'o' => 4),
								//	array("x" => 320,	"y" => -16,		"f"=>1,	'o' => 02),
									array("x" => 256,	"y" => 14,		"f"=>1,	'o' => 22),
									array("x" => 256,	"y" => -14,		"f"=>1,	'o' => 23),
									array("x" => 10,	"y" => 42,		"f"=>0,	'o' => 41),
									array("x" => 224,	"y" => 64,		"f"=>1,	'o' => 31),
									array("x" => 224,	"y" => 36,		"f"=>1,	'o' => 32),
									array("x" => 288,	"y" => 24,		"f"=>1,	'o' => 11),
									array("x" => 10,	"y" => 14,		"f"=>0,	'o' => 42),
									array("x" => 324,	"y" => 40,		"f"=>0,	'o' => 04),
									array("x" => 324,	"y" => 10,		"f"=>0,	'o' => 05),
									array("x" => 324,	"y" => -18,		"f"=>0,	'o' => 06),
									array("x" => 324,	"y" => -46,		"f"=>0,	'o' => 07),
									array("x" => 10,	"y" => -14,		"f"=>0,	'o' => 43),
								array("x" => 42,		"y" => 88,		"f"=>0,	'o' => 44),
									array("x" => 42,	"y" => 60,		"f"=>0,	'o' => 45),
									array("x" => 42,	"y" => 32,		"f"=>0,	'o' => 46),
									array("x" => 42,	"y" => 4,		"f"=>0,	'o' => 47),
									array("x" => 42,	"y" => -24,		"f"=>0,	'o' => 48),
								array("x" => 74,		"y" => 106,		"f"=>0,	'o' => 51),
								array("x" => 74,		"y" => 78,		"f"=>0,	'o' => 52),
								array("x" => 74,		"y" => 50,		"f"=>0,	'o' => 53),
								array("x" => 74,		"y" => 22,		"f"=>0,	'o' => 54),
								array("x" => 74,		"y" => -6,		"f"=>0,	'o' => 55),
								array("x" => 74,		"y" => -34,		"f"=>0,	'o' => 56),
								array("x" => 106,		"y" => 124,		"f"=>0,	'o' => 57),
								array("x" => 106,		"y" => 96,		"f"=>0,	'o' => 58),
								array("x" => 106,		"y" => 70,		"f"=>0,	'o' => 59),
								array("x" => 106,		"y" => 42,		"f"=>0,	'o' => 60),
								array("x" => 106,		"y" => 14,		"f"=>0,	'o' => 61),
								array("x" => 106,		"y" => -14,		"f"=>0,	'o' => 62),
								array("x" => 106,		"y" => -42,		"f"=>0,	'o' => 63),
									array("x" => 224,	"y" => 8,		"f"=>1,	'o' => 33),
									array("x" => 288,	"y" => -4,		"f"=>1,	'o' => 12),
								array("x" => 352,		"y" => 28,		"f"=>0,	'o' => 67),
								array("x" => 352,		"y" => 0,		"f"=>0,	'o' => 68),
								array("x" => 352,		"y" => -28,		"f"=>0,	'o' => 69),
									array("x" => 224,	"y" => -20,		"f"=>1,	'o' => 34),
									//array("x" => 288,	"y" => -32,		"f"=>1,	'o' => 13),
							
						);
		}
		function draw_pictos($batches, $max=6, $offset, $all_offsets, $object_id, $object_type)
		{
			$html 							= "";
			$i=0;
			foreach($batches as $gb)
			{
				$batch						= SMP_Goods_Batch::get_instance($gb);
				$transportation_id			= $batch->get_meta("transportation_id");
				$is_permission				= $batch->get_meta("is_permission");
				$trans						= $transportation_id != -1 && $transportation_id != "" && $is_permission;
				if($is_permission)
				{
					$html					.= $batch->get_widget_picto( 2);
				}
				else
				{
					$html					.= $batch->get_widget_picto();
				}
				if(++$i>$max)	break;
			}
			if(count($batches)>$max)
			{
				$html			.= "<div class=black_button_2 style='width: 345px;' id='goto_to_storage'>".__("goto to storage", "smp"). "</div>";
			}
			return $html;
		}
	}
?>