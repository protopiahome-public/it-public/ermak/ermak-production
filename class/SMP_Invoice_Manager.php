<?php
	class SMP_Invoice_Manager
	{
		function __construct()
		{
			//register direct message type
			add_action( 'init', 					array($this, 'add_Invoices_type'), 13 );
			add_filter( 'post_updated_messages', 	array($this, 'Invoices_messages') );
			
			// мета-поля в редактор
			add_action('admin_menu',				array($this, 'my_extra_fields_smp_invoices'));
			add_action('save_post_smp_invoice',		array($this, 'true_save_box_data'));
			
			
			add_filter('manage_edit-smp_invoice_columns', array($this, 'add_views_column'), 4);
			add_filter('manage_edit-smp_invoice_sortable_columns', array($this,'add_views_sortable_column'));
			add_filter('manage_smp_invoice_posts_custom_column', array($this,'fill_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
			add_action('admin_head',				array($this,'add_column_css'));
			add_filter('pre_get_posts',				array($this,'add_column_views_request'));
			
			
			//
			add_action('smc_myajax_submit', 		array($this, 'smc_myajax_submit'));
			add_action('smc_myajax_admin_submit', 	array($this, 'smc_myajax_admin_submit'));
		}
		
		function add_Invoices_type()
		{
			$labels = array(
				'name' => __('Invoice', "smp"),
				'singular_name' => __("Invoice", "smp"), // админ панель Добавить->Функцию
				'add_new' => __("add Invoice", "smp"),
				'add_new_item' => __("add new Invoice", "smp"), // заголовок тега <title>
				'edit_item' => __("edit Invoice", "smp"),
				'new_item' => __("new Invoice", "smp"),
				'all_items' => __("all Invoices", "smp"),
				'view_item' => __("view Invoice", "smp"),
				'search_items' => __("search Invoice", "smp"),
				'not_found' =>  __("Invoice not found", "smp"),
				'not_found_in_trash' => __("no found Invoice in trash", "smp"),
				'menu_name' => __("Invoices", "smp") // ссылка в меню в админке
			);
			$args = array(
				'labels' => $labels,
				'description' => 'Invoice betwin Players and Masters',
				'exclude_from_search' =>true, 
				'public' => true,
				'show_ui' => true, // показывать интерфейс в админке
				'has_archive' => true, 
				'menu_icon' =>'dashicons-admin-site', //smp_URLPATH .'/img/pin.png', // иконка в меню
				'menu_position' => 24, // порядок в меню			
				'supports' => array('editor', 'author')
				,'show_in_nav_menus' => true
				,'show_in_menu' => "Metagame_Finance_page"
				//,'capabilities' => 'manage_options'
				,'capability_type' => 'page'
			);
			register_post_type('smp_invoice', $args);
		}
		
		// see http://truemisha.ru/blog/wordpress/post-types.html
		function Invoices_messages( $messages ) 
		{
			global $post, $post_ID, $Soling_Metagame_Constructor;
		 
			$messages['smp_invoice'] = array( // smp_invoice - название созданного нами типа записей
				0 => '', // Данный индекс не используется.
				1 => sprintf( __('Invoice is updated', "smp"). '<a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
				2 => 'Параметр обновлён.',
				3 => 'Параметр удалён.',
				4 => 'Invoice type обновлена',
				5 => isset($_GET['revision']) ? sprintf( 'Invoice type восстановлена из редакции: %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
				6 => sprintf( 'Invoice опубликована на сайте. <a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
				7 => 'Функция сохранена.',
				8 => sprintf( 'Отправлено на проверку. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
				9 => sprintf( 'Запланировано на публикацию: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Просмотр</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
				10 => sprintf( 'Черновик обновлён. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
			);
		 
			return $messages;
		}
		
		// мета-поля в редактор
		
		function my_extra_fields_smp_invoices() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smp"), array(&$this, 'extra_fields_box_func'), 'smp_invoice', 'normal', 'high'  );
		}
		function extra_fields_box_func( $post )
		{	
			global $Soling_Metagame_Constructor, $SMP_Currency;
			$is_repaid	= (int)get_post_meta($post->ID, "is_repaid", true);
			$html		.= '
			<div class="h">
				<label for="summae">'.__("Summae", "smp").'</label>
				<br>
				<input  class="h2" name="summae" type="number" min="0" step="1" value ="'.get_post_meta($post->ID, "summae", true).'"/>
			</div>	
			<div class="h">
				<label for="currency_type">'.__("Currency Type", "smp").'</label><br>'.
				$SMP_Currency->wp_droplist_currency_type(array("name"=>"currency_type", "id"=>"currency_type", "class"=>"chosen-select", 'selected' => get_post_meta($post->ID, "currency_type", true))).
				'
			</div>	';
							
			$html 	.= '
			<div class="h">
				<label for="payer">'.__('Payer', 'smp').' </label>
				<p>'.
				$SMP_Currency->wp_droplist_accounts(
									array(														
											'name' 				=> 'payer', 
											'selected'			=> get_post_meta($post->ID, 'payer',true),
											'name'				=> 'payer',
											'class'				=> 'chosen-select'
										  )
								  ).
			'</div> 
			<div class="h">
				<label for="payee">'.__('Payee', 'smp').' </label>
				<p>'.
				$SMP_Currency->wp_droplist_accounts(
									array(														
											'name' 				=> 'payee', 
											'selected'			=> get_post_meta($post->ID, 'payee',true),
											'name'				=> 'payee',
											'class'				=> 'chosen-select'
										  )
								  ).
			'</div> 
			<div class="h">
				<input class="css-checkbox" type="checkbox"  name="is_repaid" id="is_repaid" ' . checked(1, $is_repaid, 0).' />
				<label for="is_repaid"  class="css-label">'.__("Is Repaid", "smp").'</label>
			</div>
			<div class="h">
				<label for="repaid_time">'.__('Repaid time', 'smp').' </label><BR>
				<input class="datetimepicker_unixtime" type="text" name="repaid_time"  value="'.get_post_meta($post->ID, 'repaid_time',true).'"/>
			</div>
			<div class="h">
				<label for="delivery_time">'.__('Delivery time', 'smp').' </label><BR>
				<input class="datetimepicker_unixtime" type="text" name="delivery_time"  value="'.get_post_meta($post->ID, 'delivery_time',true).'"/>
			</div>
			';
			
			
			echo $html;
			wp_nonce_field( basename( __FILE__ ), 'smpi_metabox_nonce' );
		}
		
		function true_save_box_data ( $post_id ) 
		{
			// проверяем, пришёл ли запрос со страницы с метабоксом
			if ( !isset( $_POST['smpi_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['smpi_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;
			
			global $wpdb;
			$wpdb->update(
							$table_prefix."posts",
							array("post_title" 			=> $post_id),
							array("ID" 					=> $post_id)
						);
			
			
			update_post_meta($post_id, 'summae', 		esc_attr($_POST['summae']));				
			update_post_meta($post_id, 'currency_type',	esc_attr($_POST['currency_type']));				
			update_post_meta($post_id, 'payer', 		esc_attr($_POST['payer']));				
			update_post_meta($post_id, 'payee', 		esc_attr($_POST['payee']));				
			update_post_meta($post_id, 'is_repaid', 	($_POST['is_repaid']) ? 1 : 0);			
			update_post_meta($post_id, 'delivery_time', $_POST['delivery_time']);			
			update_post_meta($post_id, 'repaid_time', 	$_POST['repaid_time']);			
			return $post_id;
		}
		
		
		//==================================================
		//
		// редактируем колонки вкладки "Сообщения" 	
		//
		//=====================================================
		
		function add_views_column( $columns ){
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
				  "do" 				=> __(""),
				  "summae" 			=> __("Summae", 'smp'),
				  "currency_type" 	=> __("Currency Type", 'smp'),
				  "payee" 			=> __("Payee", 'smp'),
				  "payer" 			=> __("Payer", 'smp'),
				  "is_repaid"		=> __("Is Repaid", "smp"),
				  "delivery_time" 	=> __("Delivery time", "smp"),
				  "repaid_time" 	=> __("Repaid time", "smp"),
			   );
			return $posts_columns;
			
		}
		// добавляем возможность сортировать колонку
		function add_views_sortable_column($sortable_columns){
			$sortable_columns['do'] 			= 'do';
			$sortable_columns['summae'] 		= 'summae';
			$sortable_columns['currency_type'] 	= 'currency_type';
			$sortable_columns['payer'] 			= 'payer';
			$sortable_columns['payee'] 			= 'payee';
			$sortable_columns['is_repaid'] 		= 'is_repaid';
			$sortable_columns['delivery_time'] 	= 'delivery_time';
			$sortable_columns['repaid_time'] 	= 'repaid_time';
			return $sortable_columns;
		}		
		// заполняем колонку данными	
		function fill_views_column($column_name, $post_id) {
			global $Direct_mesage_autor_types;
			$post		= get_post($post_id);
			switch( $column_name) 
			{
				case 'do':
					echo '
					<a class="button" href = "/wp-admin/post.php?post=' . $post_id. '&action=edit" class="button-smp" title="'.__("Edit").'"><i class="fa fa-gear"></i></a>
					<a class="button" href = "/wp-admin/post.php?post=' . $post_id. '&action=trash" class="button-smp" title="'.__("Delete").'"><i class="fa fa-remove"></i></a>
					<a class="button" href = "/?smp_invoice=' . $post->post_title. '" class="button-smp" title="'.__("Goto").'"><i class="fa fa-eye"></i></a>';
					break;
				case 'summae':
					echo get_post_meta($post_id, 'summae', 1);
					break;
				case 'currency_type':
					$ctid	= get_post_meta($post_id, 'currency_type', 1);
					$ct		= get_post($ctid);
					echo $ct->post_title . " (" . round(1/ (float)get_post_meta($ctid, "rate", 1) * (int)get_post_meta($post_id, 'summae', 1), 1). " " . get_post_meta($ctid, "abbreviation", 1) .")";
					break;
				case 'delivery_time':
					echo get_post_meta($post_id, 'delivery_time', 1);
					break;
				case 'repaid_time':
					echo get_post_meta($post_id, 'repaid_time', 1);
					break;
				case 'payer':
					echo get_post(get_post_meta($post_id, 'payer', 1) )->post_title;
					break;
				case 'payee':
					echo get_post(get_post_meta($post_id, 'payee', 1) )->post_title;
					break;
				case 'is_repaid':
					echo get_post_meta($post_id, 'is_repaid', 1) ? '<img src="'.SMP_URLPATH.'img/check_checked.png">' : '<img src="'.SMP_URLPATH.'img/check_unchecked.png">';
					break;
			}		
		}
		// изменяем запрос при сортировке колонки	
		function add_column_views_request( $object )
		{
			if($object->post_type!="smp_invoice") return $object;
			switch( $object->get('orderby'))
			{
				case 'summae':
					$object->set('meta_key', 'summae');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'currency_type':
					$object->set('meta_key', 'currency_type');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'delivery_time':
					$object->set('meta_key', 'delivery_time');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'repaid_time':
					$object->set('meta_key', 'repaid_time');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'payer':
					$object->set('meta_key', 'payer');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'payee':
					$object->set('meta_key', 'payee');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'is_repaid':
					$object->set('meta_key', 'is_repaid');
					$object->set('orderby', 'meta_value_num');
					break;
			}
			return  $object;
		}
		
		
		// подправим ширину колонки через css
		function add_column_css(){
			echo '<style type="text/css">.column-is_repaid{width:70px;} #do{width:140px;}</style>';
		}
		
		//========================================		
		//
		//	
		//
		//========================================
		function is_user_can($invoice_id, $role)
		{
			global $Soling_Metagame_Constructor;
			$account_id		= get_post_meta($invoice_id, $role, true);
			$owner			= get_post_meta($account_id, "owner_id", true);			
			return $Soling_Metagame_Constructor->cur_user_is_owner($owner);
		}
		function get_form($invoice, $params)
		{
			global $Soling_Metagame_Constructor;
			if(!isset($params))	$params = array();
			$payee_id		= get_post_meta($invoice->ID, "payee", true);
			$payee_account	= get_post($payee_id);
			$payer_id		= get_post_meta($invoice->ID, "payer", true);
			$payer_account	= get_post($payer_id);
			$repaid_time	= get_post_meta($invoice->ID, "repaid_time", true);
			if($repaid_time)
			{
				$repaid_t	= '
						<tr class="lp-batch-table-coll-setting" >
							<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
								<span class="lp-batch-comment">'.
									__("Repaid time", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Repaid time", "smp")) .
								'</span>
							</td>
							<td class="lp-batch-table-coll-setting lp-batch-col2">
								<span class="smp-goods-1">'.										
										$repaid_time.
									'</span>
							</td>
						</tr> 		
				';
			}
			else
			{
				$repaid_t	= '';
			}
			$ctid			= get_post_meta($invoice->ID, "currency_type", true);
			$ct				= get_post($ctid);
			$html			= '
			<div class="smp-batch-card" style="padding:4px;" type="invoice" id="invoke_form_'.$invoice->ID.'">
				<h3>'.
					__("Invoice", "smp")."# ". $invoice->ID.
				'</h3>
				<div class="smp-batch-card-table-cont"
					style="
					position:absolute;
					top:65px;
					left:5px;
					padding:1px!impotant;
					display:inline-block;
					">
					<table class="lp-batch-table" cellpadding="0" cellspacing="0" align="left" style="margin:1px!importing; display:table;" > 			
						<tr class="lp-batch-table-coll-setting" >
							<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
								<span class="lp-batch-comment" style="margin-bottom:3px; padding-bottom:3px; border-bottom:1px dotted white;">'.
									__("Purpose of payment", 'smp') .
								'</span>
							</td>
							<td class="lp-batch-table-coll-setting lp-batch-col2">
								<span class="smp-goods-1" style="margin-bottom:3px; padding-bottom:3px; border-bottom:1px dotted grey;">'.
										$invoice->post_content.
									'</span>
							</td>
						</tr> 								
						<tr class="lp-batch-table-coll-setting" >
							<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
								<span class="lp-batch-comment">'.
									__("Payee", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Owners of this location presents the bill to pay. Type of currency must be correct.", "smp")) .
								'</span>
							</td>
							<td class="lp-batch-table-coll-setting lp-batch-col2">
								<span class="smp-goods-1">'.
										__("Currency Account", "smp"). " " .$payee_account->post_title.
									'</span>
							</td>
						</tr> 					
						<tr class="lp-batch-table-coll-setting" >
							<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
								<span class="lp-batch-comment">'.
									__("Delivery time", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Delivery time", "smp")) .
								'</span>
							</td>
							<td class="lp-batch-table-coll-setting lp-batch-col2">
								<span class="smp-goods-1">'.										
										get_post_meta($invoice->ID, "delivery_time", true).
									'</span>
							</td>
						</tr> 			
						<tr class="lp-batch-table-coll-setting" >
							<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
								<span class="lp-batch-comment">'.
									__("Payer", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Owners of this location must to pay", "smp")) .
								'</span>
							</td>
							<td class="lp-batch-table-coll-setting lp-batch-col2">
								<span class="smp-goods-1">'.
										__("Currency Account", "smp"). " " .$payer_account->post_title.
									'</span>
							</td>
						</tr>'.
						$repaid_t.
						'<tr class="lp-batch-table-coll-setting" >
							<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
								<span class="lp-batch-comment">'.
									__("Summae", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Count of ", "smp")) .
								'</span>
							</td>
							<td class="lp-batch-table-coll-setting lp-batch-col2">
								<span class="smp-goods-1">'.
										get_post_meta($invoice->ID, "summae", true).
									'</span>
							</td>
						</tr>
						<tr class="lp-batch-table-coll-setting" >
							<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
								<span class="lp-batch-comment">'.
									__("Currency Type", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Currency type ", "smp")) .
								'</span>
							</td>
							<td class="lp-batch-table-coll-setting lp-batch-col2">
								<span class="smp-goods-1">'.
										$ct->post_title.
									'</span>
							</td>
						</tr>'.
						
					'</table>
			</div>
			<div id="invoice_form_params_'.$invoice->ID.'" style="position:absolute; bottom:5px; left:5px; display:inline-block;">';
			if(get_post_meta($invoice->ID, "is_repaid", true))
			{
				$html	.= '<div class="is_repaid" data-deg="'.(20 + rand(0,10)).'deg">'.__("Is Repaid", "smp").'</div>';
			}
			else
			{
				switch($params['type'])
				{
					case 'payee':
						break;
					case "payer";
						$txt	= sprintf(__("Delete Invoice #%s?", 'smp'), $invoice->ID);
						$html	.= '
							<div class="black_button_2 pay_the_invoice"  invoice_id="'.$invoice->ID.'" ask="'. sprintf(__("Pay Invoice #%s?", 'smp'), $invoice->ID).'" onclick="actions.pay_the_invoice(this)">'.
								__("Pay the Invoice", "smp").
							'</div>
							<div class="black_button_2 refuse" invoice_id="'.$invoice->ID.'" ask="'.$txt.'">'.
								__("Refuse", "smp").
							'</div>
							';
					
						break;
				}
			}
			$html			.= '
					
				</div>
			</div>';
			return $html;
			
		}
		function get_new_invoic_form()
		{
			global $SMP_Currency;
			$html	='
			<div id="new_invoice_form" style="display:none;">
				<div  style="display:inline-block;">
					<span for="new_invoice_payee">'.__("Payee", "smp").'</span>
					<BR>'.		
					$SMP_Currency->wp_droplist_accounts(array('id'=>'new_invoice_payee', "class"=>"chosen-select", 'user_id' => get_current_user_id())).
					'
					<hr/>
					<span style="width:52%; display:inline-block; position:relative;">'.__("Summae by UE", "smp").'</span>
					<span style="width:42%;">'.__("Summae by chosen currency", "smp").'</span><br>
					<input type="number" id="new_invoice_summae" min=0 class="chosen-select" style="width:42%;"/>
					<input type="number" id="new_invoice_summ_ct" min=0 step="0.01" class="chosen-select" style="width:42%;"/>
					
					<span for="new_invoice_ctype">'.__("Choose Currency Type for pay", "smp").'</span><br>'.
					$SMP_Currency->wp_droplist_currency_type(array("name"=>"new_invoice_ctype", "id"=>"new_invoice_ctype", "class"=>"chosen-select", 'selected' => $SMP_Currency->options['default_currency_type_id'])).
					'
					<hr/>
					<span for="new_invoice_text">'.__("Purpose of payment", "smp").'</span>
					<textarea name="new_invoice_text" id="new_invoice_text" style="height:100px;width:95%;"></textarea>				
					
					<hr/>
					<span for="new_invoice_payer">'.__("Payer", "smp").'</span>
					<BR>'.		
					$SMP_Currency->wp_droplist_accounts(array("class"=>"chosen-select", 'id'=>'new_invoice_payer')).
					'
					<hr/>
					<span class="button" id="new_invoice_button" ask="'.__("Create new Invoice?", 'smp').'">'.
						__("Create", "smp").
					'</span>
				</div>
			</div>';
			return $html;
		}
		
		function after_smp_invoice_table($name)
		{
			echo "
			<div  style='display:block;' id='drop_users'>".
			"aaaaaaaaaaaaaa".
			
			"</div>
			<script>
				jQuery('#drop_users').insertBefore('#posts-filter');
			</script>";
			}
		//
		function smc_myajax_admin_submit($params)
		{
			if(!(is_admin && current_user_can("manage_options")))
				return;
			global $user_iface_color;
			global $Soling_Metagame_Constructor;
			global $SMP_Currency;
			global $SMC_Invoice_Manager;
			
			switch($params[0])
			{
				case "change_ca_by_ct":
					$cid		= $params[1];
					$pers		= $params[2];
					$ct			= get_post($cid);
					$ca_args	= array(
											'numberposts'	=> -1,
											'offset'    	=> 0,
											'post_type' 	=> 'smc_currency_accaunt',
											'fields'		=> "ids",
											'post_status' 	=> 'publish',
											'meta_query' 	=> array(
																		array(
																				"key"		=> "ctype_id",
																				"value"		=> $cid,
																				'compare'	=> '='		
																			),
																	),
										);
					$cas		= get_posts($ca_args);
					foreach($cas as $ca)
					{
						$count			= get_post_meta($ca, 'count', true);
						update_post_meta($ca, 'count', (int)($count + $count/100*$pers));
					}
					$d					= array(	
													$params[0], 
													array(
															'text' 			=> sprintf( __("Successful change all Currensy Accounts by %s to %s persents.", "smp" ), "<b>".$ct->post_title."</b>", $pers ),
															'data'			=> SMP_Currency_Type::get_currency_statistics(),
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
			}
		}
		function smc_myajax_submit($params)
		{
			global $user_iface_color;
			global $Soling_Metagame_Constructor;
			global $SMP_Currency;
			global $SMC_Invoice_Manager;
			global $start;
			
			switch($params[0])
			{
				case "new_invoice_form":
					$ow_locations_ids				= $Soling_Metagame_Constructor->all_user_locations(get_current_user_id());
					if(count($ow_locations_ids)==0 || !is_user_logged_in())	
						$form = __("You have no one Location", "smp");
					else
						$form				= $this->get_new_invoic_form();
					$d					= array(	
													'new_invoice_form', 
													array(
															'text' 			=> $form,
															'title'			=> __("add new Invoice",'smp'), 
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case 'get_inners':
					/**/
					$all_my_accounts					= $SMP_Currency->get_user_accounts();
					if(count($all_my_accounts)==0)	
					{					
						$inner							.= '
						<div class=smp-comment>'.
							__("There are no Invoices", "smp").
						'</div>';
					}
					else
					{
						$meta_query							= array();
						$meta_query['relation']				= 'OR';
						foreach($all_my_accounts as $idd)
						{
							$arr							= array();
							$arr['value']					= $idd->ID;
							$arr['key']						= 'payer';
							$arr['compare']					= 'LIKE'; 
							$meta_query[]					= $arr;
						}
						$ar				= array(										
														'numberposts'	=> -1,
														'offset'    	=> 0,
														'orderby'  		=> 'id',
														'order'     	=> 'DESC',
														'post_type' 	=> 'smp_invoice',
														'post_status' 	=> 'publish',
														'meta_query' 	=> $meta_query
													);
													
						$users_invoices						= get_posts($ar);
						foreach($users_invoices as $invoice)
						{
							$inner							.= $SMC_Invoice_Manager->get_form($invoice, array("type" => "payer"));
						}	
						if(count($users_invoices)==0)					
						{
							$inner							.= '
							<div class=smp-comment>'.
								__("There are no Invoices", "smp").
							'</div>';
						}
					}
					$d					= array(	
													'get_inners', 
													array(
															'text' 			=> $inner,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case 'get_outers':
					$all_my_accounts					= $SMP_Currency->get_user_accounts();
					if(count($all_my_accounts)==0)	
					{					
						$outer							.= '
						<div class=smp-comment>'.
							__("There are no Invoices", "smp").
						'</div>';
					}
					else
					{
						$meta_query							= array();
						$meta_query['relation']				= 'OR';
						foreach($all_my_accounts as $idd)
						{
							$arr							= array();
							$arr['value']					= $idd->ID;
							$arr['key']						= 'payee';
							$arr['compare']					= 'LIKE'; 
							array_push($meta_query, $arr);
						}
						$ar				= array(										
														'numberposts'	=> 0,
														'offset'    	=> 0,
														'orderby'  		=> 'id',
														'order'     	=> 'DESC',
														'post_type' 	=> 'smp_invoice',
														'post_status' 	=> 'publish',
														'meta_query' 	=> $meta_query
													);
						$users_invoices						= get_posts($ar);
						
						foreach($users_invoices as $invoice)
						{
							$outer							.= $SMC_Invoice_Manager->get_form($invoice, array("type" => "payee"));
						}	
							
						if(count($users_invoices)==0)
						{
							$outer							.= '
							<div class=smp-comment style="">'.
								__("There are no Invoices", "smp").
							'</div>';
						}
					}
					$d					= array(	
													'get_outers', 
													array(
															'text' 			=> $outer,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case 'new_invoice':
					//payee_np, payer_np, inv_text_np, inv_summae
					$dat			= $params[1];
					if((int)$dat[3]<0)
					{
						$new_post_id	= new WP_Error(__("Enter a positive number in the 'Summae'","smp"));
					}
					else
					{
						$new_post_id	= wp_insert_post(array(
																				'post_name'			=> ''
																				, 'post_status' 	=> 'publish'
																				, "post_title"		=> ''
																				, "post_content"	=> strip_tags( stripslashes($dat[2]))
																				, "post_type"		=> "smp_invoice"
																				)
																		);
						update_post_meta($new_post_id, 'payee', $dat[0]);
						update_post_meta($new_post_id, 'payer', $dat[1]);
						update_post_meta($new_post_id, 'summae', $dat[3]);
						update_post_meta($new_post_id, 'currency_type', $dat[4]);
						update_post_meta($new_post_id, 'delivery_time', date_i18n( 'G:i j.m.Y ' ));
					}
					$d					= array(	
													'new_invoice', 
													array(
															'text' 			=> is_wp_error($new_post_id) ? __("Error craeting Invoce", "smp"): __("Success create Invoice", ""),
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case "smp_refuse":
					if( $this->is_user_can($params[1], "payer"))
					{
						$del			= wp_delete_post($params[1]);
						$answ			= __("Succsessfull delete","smp");
					}
					else
					{
						$answ			= __("You can't do this operation.","smp");
					}
					$d					= array(	
													'smp_refuse', 
													array(
															'text' 			=> $answ,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
				case "pay_the_invoice":
					if( $this->is_user_can($params[1], "payer"))
					{
						$inv			= get_post($params[1]);
						$account_id		= get_post_meta($params[1], "payer", true);
						$payee_id		= get_post_meta($params[1], "payee", true);
						$summae			= get_post_meta($params[1], "summae", true);
						if(get_post_meta($account_id, "count", true) >= $summae)
						{
							$summ		= SMP_Currency::transfer_from( $account_id, $summae, $payee_id, $inv->post_content );
							$new_summ	= get_post_meta($account_id, 'count', true);
							SMP_Currency::transfer_to( $payee_id, $summ, $account_id, $inv->post_content );
							$date		= date_i18n("G:i d.m.Y");
							update_post_meta($params[1], "repaid_time", $date);
							update_post_meta($params[1], "is_repaid", 1);
							$ans		= 	array($date, $params[1], __("Is Repaid", "smp"), $account_id, $new_summ);
						}						
						else
						{
							$account_id	= get_post_meta($params[1], "payer", true);
							$new_summ	= get_post_meta($account_id, 'count', true);							
							$date		= date_i18n("G:i d.m.Y");
							$ans		= array( $date, $params[1], __("You are no owner!", "smp"), $account_id, $new_summ );
						}
					}
					$d					= array(	
													'pay_the_invoice', 			//
													array(
															'text' 			=> $ans,
															'exec' 			=> 'alert',
															'args' 			=> $params[1],
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode($d);
					print $d_obj;
					break;
			}
		}
	}
?>