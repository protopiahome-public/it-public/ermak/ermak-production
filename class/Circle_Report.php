<?php
	
	class Circle_Report
	{
		protected $players;
		protected $current_circle;
		protected $i=1;
		function __construct()
		{
			$this->players			= array();
			$this->impement_current_circle();
		}
		
		function impement_current_circle()
		{
			$this->current_circle	= get_option('current_circle');			
			if(!isset($this->current_circle))
			{
				$this->current_circle = array(
												"number"		=> 0,
												"time"			=> time(),
											 );
			}
			(int)$this->current_circle['number']++;
			update_option('current_circle', $this->current_circle);
			
		}
		public function get_user_report($user_id)
		{
			return $this->players["".$user_id];
		}
		public function send_for_player($user_id, $text="")
		{
			if(!isset($this->players["".$user_id]))
			{
				$this->players["".$user_id]		= "<div>".__("Report of production results.", "smp")."</div>";
				$this->players["".$user_id]		.= "<div>".__("Number of circle - ", "smp"). $this->current_circle['number']."</div>";
				$this->players["".$user_id]		.= "<div>".__("Time of finish - ", "smp").date_i18n( __( 'M j, Y @ G:i' ), time())."</div>";
				/*
				$this->players["".$user_id]		.= "
				<table border='1' cellspacing='10', cellpadding='10' class='smp-padding-1' style='width:80%;'>
					<tr bgcolor='grey'>
						<td width='50'>#
						</td>
						<td>".__("Factory", "smp").
						"</td>
						<td width='200'>".__("Goods type", "smp").
						"</td>
						<td width='80'>".__("Prodused", "smp").
						"</td>						
						<td width='80'>".__("Total", "smp").
						"</td>
					</tr>";
				*/
			}

			$this->players["".$user_id]			.= $text;
			return $this->players["".$user_id];
		}
		
		function add_batch($factory, $batch)
		{		
			$owners			= $factory->get_owners();						
			$owner_id		= $factory->get_owner_id();
			$owner			= SMC_Location::get_instance($owner_id);
			$permalink		= SMC_Location::get_term_link($owner_id);
			$goods_type		= get_post(get_post_meta($factory->id, 'goods_type_id', true));
			$pl				= "<p>".strtr( __("%F% that owned by %N% producted %C% units of %B%", "smp"), 
											array(
													"%F%"	=> "<a href='/?factory=" . $factory->id . "'>" . $factory->get("post_title") . "</a>",
													"%N%"	=> "<a href='$permalink'>" . $owner->name . "</a>",
													"%C%"	=> "" . $factory->get_productivity(),
													"%B%"	=> "<a href='/?goods_type=" . $goods_type->post_name . "'>" . $goods_type->post_title . "</a>",
												  )
											)."</p>";
			foreach($owners as $owner)
			{
				if (!isset($owner) || $owner == -1 || $owner == "" )	continue;	
				$this->send_for_player($owner, $pl);			
			}		
			return $pl;
		}
		function send_factory_message($factory, $text)
		{		
			$owners				= $factory->get_owners();
			
			foreach($owners as $owner)
			{
				if (!isset($owner) || $owner== -1)	continue;				
				$this->send_for_player($owner, $text);
			}
			//echo $text;
		}

		function send_messages()
		{
			global $Direct_Message_Menager;
			global $Direct_mesage_autor_types;
			//var_dump($this->players);
			foreach($this->players as $player=>$val)
			{
				$post_data		= array(
											'post_title'		=> strtr(__("Report about established products for %FF% circle" , "smp"), array("%FF%"=> $this->current_circle['number'])),
											'post_content'		=> $val,
										);		
				
				$Direct_Message_Menager->send_dm($post_data, $player, 4);
			}
			
		} 

		static function save_global($report_id, $report_type, $start_time, $content)
		{
			global $wpdb;
			return $wpdb->insert(
				$wpdb->prefix.REPORT_DB_PREFIX,
				array(
						"report_id"		=> $report_id,
						"report_type"	=> $report_type,
						"start_time"	=> $start_time,
						"content"		=> $content
					 ),
				array( '%d', '%d', '%d', '%s')
			 );
									
		}
		static function get_global_count()
		{
			global $wpdb;
			return $wpdb->get_row ( "SELECT MAX(report_id) AS current_circle FROM ". $wpdb->prefix.REPORT_DB_PREFIX . " WHERE report_type=".PRODUCTION_CIRCLE_TYPE, ARRAY_A );
		}
		static function get_global($circle_id)
		{
			global $wpdb;
			return $wpdb->get_results ( "SELECT * FROM ". $wpdb->prefix.REPORT_DB_PREFIX . " WHERE report_id='".$circle_id."' AND report_type=".PRODUCTION_CIRCLE_TYPE, ARRAY_A );
		}
		static function get_globals()
		{
			global $wpdb;
			return $wpdb->get_results ( "SELECT * FROM ". $wpdb->prefix.REPORT_DB_PREFIX . "", ARRAY_A );
		}
	}
?>