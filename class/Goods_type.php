<?php

	class Goods_Type
	{
		protected $id;
		static $instances;
		function __construct()
		{
			add_action('init', 										array($this, 	'add_Goods_type'), 12 );
			add_action('admin_menu',								array($this, 	'my_extra_fields_goods_type'));
			add_action('save_post_goods_type',						array($this, 	'true_save_box_data'));
			add_filter('manage_edit-goods_type_columns',		 	array($this, 	'add_views_column'), 4);
			add_filter('manage_edit-goods_type_sortable_columns', 	array($this,	'add_views_sortable_column'));
			add_filter('manage_goods_type_posts_custom_column', 	array($this,	'fill_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
		}
		static function get_instance($id)
		{
			if(!self::$instances)	self::$instances = array();
			if(!self::$instances[$id])
				self::$instances[$id] = get_post($id);
			return self::$instances[$id];
		}
		static function wp_dropdown_goods_type($params)
		{
			$args		= array(
									"numberposts"		=> -1,
									"offset"			=> 0,
									'orderby'  			=> 'title',
									'order'     		=> 'ASC',
									'post_type' 		=> 'goods_type',
									'post_status' 		=> 'publish',									
								);
			$hubs		= get_posts($args);
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			if(isset($params['multile']))
				$html	.= 'multiple ';
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";
			
			foreach($hubs as $hub)
			{
				$html	.= "<option value='".$hub->ID."' ".selected($hub->ID, $params['selected'], false).">".$hub->post_title."</option>";
			}
			$html		.= "</select>";
			return $html;
		}
		
		function add_Goods_type()
		{
				$labels = array(
					'name' => __('Goods type', "smp"),
					'singular_name' => __("Goods type", "smp"), // админ панель Добавить->Функцию
					'add_new' => __("add Goods type", "smp"),
					'add_new_item' => __("add new Goods type", "smp"), // заголовок тега <title>
					'edit_item' => __("edit Goods type", "smp"),
					'new_item' => __("new Goods type", "smp"),
					'all_items' => __("all Goods types", "smp"),
					'view_item' => __("view Goods type", "smp"),
					'search_items' => __("search Goods type", "smp"),
					'not_found' =>  __("Goods type not found", "smp"),
					'not_found_in_trash' => __("no found Goods type in trash", "smp"),
					'menu_name' => __("Goods type", "smp") // ссылка в меню в админке
				);
				$args = array(
					'labels' => $labels,
					'public' => true,
					'show_ui' => true, // показывать интерфейс в админке
					'has_archive' => true, 
					'exclude_from_search' => true,
					'menu_position' => 21, // порядок в меню
					'show_in_menu' => "Metagame_Production_page",
					'supports' => array( 'title', 'thumbnail', "excerpt")
					//,'capabilities' => 'manage_options'
					,'capability_type' => 'post'
				);
				register_post_type('goods_type', $args);
		}
		
		
		// мета-поля в редактор
		
		function my_extra_fields_goods_type() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(&$this, 'extra_fields_box_func'), 'goods_type', 'normal', 'high'  );
		}
		function extra_fields_box_func( $post )
		{	
			$color				= get_post_meta( $post->ID, "color", true );
			$power				= (int)get_post_meta( $post->ID, "powerfull_multiplier", true );
			if($color == "") 	$color	= Assistants::HSVtoRGB(array(mt_rand(0, 100)/100, mt_rand(30, 100)/100, mt_rand(20, 40)/100));
			?>
			<div style='display:inline-block;'>
				<div style='display:block; margin-bottom:10px;'>
					<?php 
					$args = array( 
						'wpautop' => 1
						,'media_buttons' => 0
						,'textarea_name' => 'description' //нужно указывать!
						,'textarea_rows' => 3
						,'tabindex' => null
						,'editor_css' => ''
						,'editor_class' => ''
						,'teeny' => 0
						,'dfw' => 0
						,'tinymce' => 1
						,'quicktags' => 0
						,'name' => 'description'
					);
					wp_editor( get_post_meta($post->ID, "description", true), 'description', $args );
					?>
				</div>
				<div class="smp_batch_extra_field_column">
					<div class="h">
						<label for="industry"><?php _e("Industry", "smp");?></label><br>
						<?php echo SMP_Industry::wp_dropdown_industry(array('name'=>'industry', 'class'=>'h2', 'selected'=>get_post_meta( $post->ID, "industry", true ))); ?>
					</div>
					<div class="h">
						<label for="price"><?php _e("Price", "smp");?></label><br>
						<input  class="h2" name="price" type="number" min="0" step="1" value ="<?php print_r( $this->get_price( $post->ID) );?>"/>
					</div>
					<div class="h">
						<label for="usage_duration"><?php _e("Usage Duration(per play circles)", "smp");?></label><br>
						<input  class="h2" name="usage_duration" type="number" min="0" step="1" value ="<?php print_r( get_post_meta( $post->ID, "usage_duration", true ) );?>"/>
					</div>
					<div class="h">
						<label for="complexity"><?php _e("Produse Complexity", "smp");?></label><br>
						<input  class="h2" name="complexity" type="number" min="0" max= '100' step="1" value ="<?php print_r( self::get_complexity( $post->ID) );?>"/>
					</div>
					<div class="h">						
						<label for="color"><?php _e("Color", "smp");?></label><br>
						<input class="color" name="color" id="color" value="<?php echo ( $color );?>"/>
					</div>
					<div class='h'>	
						<label  class="h5" for="supported_type_name"><?php _e("Components", "smp"); ?></label>
						<?php
							$goods		= get_post_meta($post->ID, "components", true);
							//echo "<BR>"; 
							//var_dump($goods);
						?>
						<div style="vertical-align:middle;" id="scheme0">							
						<?php
							$sc		= '
							<div class="consume_scheme_box_element" id="example">'.
								self::wp_dropdown_goods_type(array("class"=>"smco_drop", "name"=>"goods_type_cs0")).
								'<input  style="width:50px; padding:6px;vertical-align:middle;" type="number"  min="1" name="goods_type_volume0" class="smco_numberer"/>
								<span class="button minus_button_gt" id="minus" style="vertical-align:middle;"> - </span>
							</div>';
							echo $sc;
							
							//var_dump($goods);
							//echo "<BR>".count($goods[0]);
							$i=0;
							if($goods != "")
								if(count($goods)>0)
								{
									foreach($goods as $gd)
									{
										//echo "<BR>goods_type=".$gd[0]["goods_type"].", goods_value=".$gd[0]["goods_value"]."<BR>";
										?>
										<div class="consume_scheme_box_element" id="example" style="display:block; ">
											<?php echo self::wp_dropdown_goods_type(array("selected"=>$gd[0]["goods_type"], "class"=>"smco_drop", "name"=>"goods_type_cs0".$i)); ?>
											<input style="width:50px; padding:6px;vertical-align:middle;" type="number" min="1" name="goods_type_volume0<?php echo $i; ?>" value="<?php echo $gd[0]["goods_value"];?>" class="smco_numberer"/>											
											<span class="button minus_button_gt" id="minus" style="vertical-align:middle;"> - </span>
										</div>
										<?php
										$i++;
									}
								}
						?>
							<input type="hidden" id="i0" name="i0" value="<?php echo $i; ?>"/>
							<div id="add_button_gt" n="0" class="button" style="font-size:20px; margin-top:10px;">
								+
							</div>
						</div>
					</div>
					<div class='h'>	
						<label  class="h5" for="supported_type_name"><?php _e("Alternative Components", "smp"); ?></label>
						<?php
							$goods		= get_post_meta($post->ID, "components2", true);
							//echo "<BR>"; 
							//var_dump($goods);
						?>
						<div style="vertical-align:middle;" id="scheme1">							
						<?php
							
							//var_dump($goods);
							//echo "<BR>".count($goods[0]);
							$i=0;
							if($goods != "")
								if(count($goods)>0)
								{
									foreach($goods as $gd)
									{
										//echo "<BR>goods_type=".$gd[0]["goods_type"].", goods_value=".$gd[0]["goods_value"]."<BR>";
										?>
										<div class="consume_scheme_box_element" id="example" style="display:block; ">
											<?php echo self::wp_dropdown_goods_type(array("selected"=>$gd[0]["goods_type"], "class"=>"smco_drop", "name"=>"goods_type_cs1".$i)); ?>
											<input style="width:50px; padding:6px;vertical-align:middle;" type="number" min="1" name="goods_type_volume1<?php echo $i; ?>" value="<?php echo $gd[0]["goods_value"];?>" class="smco_numberer"/>											
											<span class="button minus_button_gt" id="minus" style="vertical-align:middle;"> - </span>
										</div>
										<?php
										$i++;
									}
								}
						?>
							<input type="hidden" id="i1" name="i1" value="<?php echo $i; ?>"/>
							<div id="add_button_gt" n="1" class="button" style="font-size:20px; margin-top:10px;">
								+
							</div>
						</div>
					</div>	
					
				</div>
				<div class="smp_batch_extra_field_column"> 
					<div class='h h9'>						
						<!--input name="is_equipment" id="is_equipment" type="checkbox" <?php checked(get_post_meta($post->ID, "is_equipment", true), 1);?>/>
						<label for="is_"><?php _e("Is Equipment", "smp");?></label><br--> 
						<label  class="h9" for="quality_multiplier"><?php _e("Quality multiplier", "smp"); ?></label><br>
						<input  class="h9 rangerr" max='100' name="quality_multiplier" value="<?php print_r( (int)get_post_meta( $post->ID, "quality_multiplier", true ));?>"/>
						<br>
						<label  class="h9" for="powerfull_multiplier"><?php _e("Powerfull multiplier", "smp"); ?></label><br>
						<input  class="h9 rangerr" max='1000' name="powerfull_multiplier" value="<?php print_r( $power);?>"/>
						<br>
						<label  class="" for="goods_type_equipment"><?php _e("Equipment for goods type production", "smp"); ?></label><br>
						<?php echo self::wp_dropdown_goods_type(array("class"=>"h9", "name"=>"goods_type_equipment", 'style'=>'height:77px;', "id"=>"goods_type_equipment", "selected"=>get_post_meta($post->ID, "goods_type_equipment", true) )); ?>
						<br>
					</div>
				</div>
				<!--div class="smp_batch_extra_field_column"> 
					<div class='h h9'>	 					
						<label  class="h9" for="capacity_multiplier"><?php _e("Capacity multiplier", "smp"); ?></label><br>
						<input  class="h9 rangerr" name="capacity_multiplier" value="<?php print_r( (int)get_post_meta( $post->ID, "capacity_multiplier", true ));?>"/>
						<br>
						<label  class="h9" for="speed_multiplier"><?php _e("Speed multiplier", "smp"); ?></label><br>
						<input  class="h9 rangerr" name="speed_multiplier" value="<?php print_r( (int)get_post_meta( $post->ID, "speed_multiplier", true ));?>"/>
						<br>
						<label  class="h9" for="cost_multiplier"><?php _e("Cost multiplier", "smp"); ?></label><br>
						<input  class="h9 rangerr" name="cost_multiplier" value="<?php print_r( (int)get_post_meta( $post->ID, "cost_multiplier", true ));?>"/>
						<br>
					</div>
				</div-->
			</div>
			<script type="text/javascript">
				jQuery(function($)
				{	
					$(".rangerr").each(function(i, elem)
					{
						$(elem).ionRangeSlider({
								min: -100,
								max: $(elem).attr("max"), 			
								type: 'single',
								step: 5,
								postfix: "%",
								prettify: true,
								hasGrid: true
						});
					});
				});
			</script>
			<?php 
			wp_nonce_field( basename( __FILE__ ), 'goods_type_metabox_nonce' );
		}
		
		function true_save_box_data ( $post_id) 
		{	
			//echo "AA=". $_POST['is_cargo'];
			// проверяем, пришёл ли запрос со страницы с метабоксом
			if ( !isset( $_POST['goods_type_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['goods_type_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;	
			if($_POST['complexity']=="")	$_POST['complexity'] = 20;
			update_post_meta($post_id, 'description', 			$_POST['description']);				
			update_post_meta($post_id, 'industry', 				$_POST['industry']);				
			update_post_meta($post_id, 'price', 				$_POST['price']);				
			update_post_meta($post_id, 'complexity', 			$_POST['complexity']);				
			update_post_meta($post_id, 'usage_duration', 		$_POST['usage_duration']);				
			update_post_meta($post_id, 'is_cargo', 				$_POST['is_cargo']=="on" ? 1 : 0);	
			update_post_meta($post_id, 'color', 				$_POST['color']);	 			
			update_post_meta($post_id, 'quality_multiplier',	$_POST['quality_multiplier']);	 			
			update_post_meta($post_id, 'powerfull_multiplier',	$_POST['powerfull_multiplier']);	 			
			update_post_meta($post_id, 'capacity_multiplier',	$_POST['capacity_multiplier']);	 			
			update_post_meta($post_id, 'speed_multiplier',		$_POST['speed_multiplier']);	 			
			update_post_meta($post_id, 'cost_multiplier',		$_POST['cost_multiplier']);	 
			update_post_meta($post_id, 'goods_type_equipment',	$_POST['goods_type_equipment']);	 
			update_post_meta($post_id, 'is_equipment',			(
																	$_POST['quality_multiplier'] 	!= 0 || 
																	$_POST['powerfull_multiplier'] 	!= 0));
			update_post_meta($post_id, 'is_freighter',			(
																	$_POST['capacity_multiplier']	!= 0 || 
																	$_POST['speed_multiplier'] 		!= 0 ||
																	$_POST['cost_multiplier']		!= 0 ));
			
			$components					= array();
			for($i=0; $i<$_POST['i0']; $i++)
			{
				$component				= array();			
				$goods_type				= $_POST['goods_type_cs0'.$i];
				$goods_val				= $_POST['goods_type_volume0'.$i];
				if($goods_type)
				{
					$component[]		= array("goods_type"=>$goods_type, "goods_value"=>$goods_val);
				}
				if(count($component)>0)		$components[] = $component;
			}
			update_post_meta($post_id, 'components', 		$components);	
			
			///components 2
			$components					= array();
			for($i=0; $i<$_POST['i1']; $i++)
			{
				$component				= array();			
				$goods_type				= $_POST['goods_type_cs1'.$i];
				$goods_val				= $_POST['goods_type_volume1'.$i];
				if($goods_type)
				{
					$component[]		= array("goods_type"=>$goods_type, "goods_value"=>$goods_val);
				}
				if(count($component)>0)		$components[] = $component;
			}
			update_post_meta($post_id, 'components2', 		$components);	
			
			return $post_id;
		}
		function get_components($id)
		{
			$components				= get_post_meta($id, "components", true);
			if(!is_array($components))	return array();
			return $components;
			
		}
		function get_components2($id)
		{
			$components				= get_post_meta($id, "components2", true);
			if(!is_array($components))	return array();
			return $components;
			
		}
		
		function add_views_column( $columns ){
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
				  "IDs"	 			=> __("ID", 'smp'),
				  "image"	 		=> __("Image", 'smp'),
				  "title" 			=> __("Title"),				  
				  "industry" 		=> __("Industry", "smp"),				  
				  "goods_price" 	=> __("Price", 'smp'),
				  "complexity" 		=> __("Produse Complexity", 'smp'),
				  "usage_duration" 	=> __("Usage Duration", 'smp'),
				  "components"		=> __("Components", "smp"),
				  "components2"		=> __("Alternative components", "smp"),
				// "is_cargo" 		=> __("Is Cargo", 'smp'),
				// "is_equipment" 	=> __("Is Equipment", 'smp'),
				// "is_freighter" 	=> __("Is Freighter", 'smp'), 
				// "color" 			=> __('Color'),
			   );
			return $posts_columns;			
		}
		// добавляем возможность сортировать колонку
		function add_views_sortable_column($sortable_columns){
			$sortable_columns['goods_price'] 			= 'goods_price';
			$sortable_columns['complexity'] 			= 'complexity';
			$sortable_columns['usage_duration'] 		= 'usage_duration';
			$sortable_columns['IDs'] 					= 'IDs';					
			$sortable_columns['industry'] 				= 'industry';					
			//$sortable_columns['is_cargo'] 				= 'is_cargo';					
			//$sortable_columns['is_equipment'] 			= 'is_equipment';					
			//$sortable_columns['is_freighter'] 			= 'is_freighter';					
			//$sortable_columns['color'] 					= 'color';					
			return $sortable_columns;
		}	
		// заполняем колонку данными	
		function fill_views_column($column_name, $post_id)
		{
			$post			= get_post($post_id);
			switch( $column_name) 
			{		
				case 'IDs':
					echo "<br><div class='ids'><span>ID</span>".$post_id."</div>";
					break;	
				case 'image':
					echo self::get_trumbnail($post_id, 56, array('margin'=>5, 'padding-top'=>4)); //get_the_post_thumbnail( $post_id, array(56, 56));
					break;	
				case 'goods_price':
					echo $this->get_price($post_id);
					break;
				case 'complexity':
					echo self::get_complexity($post_id);
					break;
				case 'usage_duration':
					echo "<span style=font-weight:900;font-size:17px;>".(int)get_post_meta($post_id, 'usage_duration', true)."</span> <br>".__("circles", "smp");
					break;
				case 'is_equipment':
					$gt	= get_post_meta( $post_id, 'is_equipment', true );
					echo '<img style="display:inline-block; margin-top:5px; " src="'.SMP_URLPATH.'img/'.($gt ? 'check_checked' : 'check_unchecked').'.png">' ;
					break;
				case 'is_freighter':
					$gt	= get_post_meta( $post_id, 'is_freighter', true );
					echo '<img style="display:inline-block; margin-top:5px; " src="'.SMP_URLPATH.'img/'.($gt ? 'check_checked' : 'check_unchecked').'.png">' ;
					break; 
				case 'is_cargo':
					$gt	= get_post_meta( $post_id, 'is_cargo', true );
					echo '<img style="display:inline-block; margin-top:5px; " src="'.SMP_URLPATH.'img/'.($gt ? 'check_checked' : 'check_unchecked').'.png">' ;
					break;
				case "components":
					echo Goods_Type::get_components_form_2($post_id);
					break;
				case "components2":
					echo Goods_Type::get_components_form_2($post_id, 1);
					break;
				case "industry":
					$ind_meta	= get_post_meta($post_id, "industry", true);
					$ind 		= $ind_meta ? SMP_Industry::get_instance($ind_meta) : "";
					echo 		$ind_meta ? $ind->body->post_title."<br><div class='ids'><span>ID</span>".$ind_meta."</div>" : "";
					break;
			}		
		}
		function get_price($id)
		{
			return get_post_meta($id, "price", true);
		}
		static function get_complexity($id)
		{
			return get_post_meta($id, 'complexity', true);
		}
		static function get_industry_id($id)
		{
			return get_post_meta($id, 'industry', true);
		}
		static function the_content($post, $content, $before, $after)
		{
			$content	= "<h1>".__("Market", "smp")."</h1><BR>";
			$farg		= array(
									'numberposts'	=> 0,
									'offset'    	=> 0,
									'orderby'  		=> 'title',
									'order'     	=> 'ASC',
									'post_type' 	=> 'goods_batch',
									'post_status' 	=> 'publish',
									'meta_query' 	=> array(
																"key" 		=> "goods_type_id",
																"value" 	=> $post->ID,
																"compare" 	=> "LIKE",
															  ),
								);
			
			$goods_batchs	= get_posts($farg);
			$i=0;
			foreach($goods_batchs as $goods_batch)
			{
				if(get_post_meta($goods_batch->ID, "goods_type_id", true) != $post->ID)continue;
				$i++;	
				$gb			= SMP_Goods_Batch::get_instance($goods_batch->ID);
				if($gb->user_is_owner()) 
				{
					$post_data			= 1;
				}
				else
				{				
					$post_data			= -1;
				}
				$content 				.= $gb->get_stroke($goods_batch, $post_data);
			} 
			$args		= array(
									'numberposts'	=> 0,
									'offset'    	=> 0,
									'orderby'  		=> 'title',
									'order'     	=> 'ASC',
									'post_type' 	=> 'factory',
									'post_status' 	=> 'publish',
									'meta_query' 	=> array(
																
																"key" 		=> "goods_type_id",
																"value" 	=> $post->ID,
																"compare" 	=> "=",
															  ),
								);
			$factories				= get_posts($args);				
			
			$content					.= "<h1>".__("Factories, that produse now", "smp")."</h1><BR>";
			foreach($factories as $factory)
			{				
				if(get_post_meta($factory->ID, "goods_type_id", true) != $post->ID)	continue;
				$i++;
				$complexity			= self::get_complexity($post->ID);
				$powerfull			= get_post_meta($factory->ID, "powerfull", true);
				$count				= floor ($powerfull/ $complexity);
				$content 			.= "<div class='smp-colorized smp-owner-goods factoring' factory_id='" . $factory->post_name . "'>" . $factory->post_title  . " - <span class='smp-goods-2'>". $count . "</span> " . __("unit", "smp") . "</div>";
			}
			if($i==0)
				$content				.= "<div class='smp-comment'>".__("Factories, that produse this Goods type not present.", 'smp')."</div>";

			return $content.'<BR>';
		}
		//=========================
		//
		//	set on global all Goods types for usege
		//
		//=========================
		static function get_global() 
		{
			global $all_goods_types;
			if(isset($all_goods_types))	return $all_goods_types;
			$gtargs				= array(
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'title',
										'order'     	=> 'ASC',
										'post_type' 	=> 'goods_type',
										'post_status' 	=> 'publish'
										
								    );
			$all_goods_types		= get_posts($gtargs);
			return $all_goods_types;
		}
		static function get_all_gt_ids()
		{
			global $all_goods_types_ids;
			if(isset($all_goods_types_ids))	return $all_goods_types_ids;
			$gtargs				= array(
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'fields'		=> 'ids',
										'orderby'  		=> 'ID',
										'order'     	=> 'ASC',
										'post_type' 	=> 'goods_type',
										'post_status' 	=> 'publish'
										
								    );
			$all_goods_types_ids		= get_posts($gtargs);
			return $all_goods_types_ids;
		}
		static function get_picto($id, $size=78, $params=-1, $count=1)
		{
			$sz2		= $size + 6;
			if($params==-1)
				$params	= array("is_hint"=>true, "class"=>"smp_goods_trumbnail");		
			if(isset($params['class']))
				$class	= $params['class'];
			$margin		= $params['margin'];
			if($margin!='') 
				$margin = 'margin-top:'.$margin.'px; margin-left:'.$margin.'px';
			$tru_id		= get_post_thumbnail_id($id);
			$fill		= get_post_meta($id, 'color', true); 
			$ind_id		= get_post_meta($id, 'industry', true); 
			$ind		= SMP_Industry::get_instance($ind_id);
			$ind_title	= $ind ? $ind->get("resourse_name") : "";
			if(!$fill)	$fill = "333333";
			$title		= get_post($id)->post_title;
			if($count>=0)
			{
				$cnt_text	= "<div class='goods_count' style='background:#" . $fill . ";'>$count</div>";
			}
			$hint_class	= $params['is_hint'] ? "div_hinter" : "";
			return "<center class='factory_widg_goods_type goods_type_trumb  $class' style='opacity:1; margin:2px; width:" .
					$sz2 . 
					"px; height:". 
					$sz2 .
					"px; background-color:#" . $fill . "; display:inline-block; position:relative;" . 
					$margin. "; ' data-hint='gt__".$id."' width='500'>".
						get_the_post_thumbnail( $id, array($size, $size), array("class"=>"smp_goods_trumbnail"), $params).$cnt_text.
					"</center>";
		
		}
		static function get_trumbnail($id, $size=78, $params=-1, $count=1)
		{
			$sz2		= $size + 6;
			if($params==-1)
				$params	= array("is_hint"=>true, "class"=>"smp_goods_trumbnail", "component_type"=>0);
			if(!isset($params['is_hint']))
				$params['is_hint'] = true;
			if(isset($params['class']))
				$class	= $params['class'];
			$margin		= $params['margin'];
			if($margin!='') 
				$margin = 'margin-top:'.$margin.'px; margin-left:'.$margin.'px';
			$tru_id		= get_post_thumbnail_id($id);
			$fill		= get_post_meta($id, 'color', true); 
			$ind_id		= get_post_meta($id, 'industry', true); 
			$ind		= SMP_Industry::get_instance($ind_id);
			$ind_title	= $ind ? $ind->get("resourse_name") : "";
			if(!$fill)	$fill = "333333";
			$title		= get_post($id)->post_title;
			$hint		= $params['is_hint'] ? '
			<div class="lp-hide" id="gt__'.$id.'" style=" width:350px;">
				<h2>'.$title.'</h2>
				<h3>'.
					$ind_title.
				'</h3>
				<div>'.
					self::get_components_form_2($id, (int)$params['component_type'], $count).
				'</div>
			</div>': "" ;
			
			$hint_class	= $params['is_hint'] ? "div_hinter" : "";
			return "<center class='factory_widg_goods_type goods_type_trumb $hint_class $class' style='opacity:1; margin:2px; width:" .
					$sz2 . 
					"px; height:". 
					$sz2 .
					"px; background-color:#" . $fill . "; display:inline-block; position:relative;" . 
					$margin. "; ' data-hint='gt__".$id."' width='500'>".
						get_the_post_thumbnail( $id, array($size, $size), array("class"=>"smp_goods_trumbnail"), $params).$hint.
					"</center>";
		}
		static function get_panel_form($id)
		{
			$pp			= get_post($id);
			$fill		= get_post_meta($id, 'color', true);
			if(!$fill)	$fill = "333333";
			$gt_components	= Goods_Type::get_components_form($id);
			return "<center class=lp_opn_mtgm_title><div  class=smc_special_title_cont>" . 
							__("Goods type", "smp") . 
						"</div> <div class=smc_special_title style=background:#$fill; >" . 
							$pp->post_title . 
							"</div>" . Goods_Type::get_trumbnail($id).
						"</center>$gt_components";
		}
		static function get_components_form($id, $techology_type=0)
		{
			$gt_components	= "<div class=gt_components_cont><div>";
			$comp		= $techology_type == 0 ? get_post_meta($id, 'components', true) : get_post_meta($id, 'components2', true);
			$comp		= is_array($comp) 	? $comp : array();
			$count	= 	count($comp);
			if($count)
			{
				$gt_components	.= "<board_title>". __("Components", "smp")."</board_title> ";
				foreach($comp as $c)
				{
					$gid			= $c[0]["goods_type"];
					$gt				= get_post($gid);
					$gttrum_id		= get_post_thumbnail_id($gid);
					$gttrum			= wp_get_attachment_image_src($gttrum_id, array(78, 78));
					$gt_cnt			= $c[0]["goods_value"];
					$fill			= get_post_meta($gid, 'color', true);
					if(!$fill)		$fill = "333333";
					//for($i=0; $i < $gt_cnt; $i++)
						$gt_components	.= "
						<div style=background-color:#" . $fill . "; class=hint hint--top  data-hint=" . $gt->post_title . ">
							<img src=".$gttrum[0].">
							<div class=gt_count_label>".$gt_cnt."</div>
						</div>";
				}					
			}
			else
			{
				$gt_components	.= __("This goods type not have components.",'smp');
			}
			$price				= get_post_meta($id, 'price', true);
			$gt_components		.= "</div>";
			if($price>0)
				$gt_components	.= "<div><board_title>".__("Cost Price", "smp")."</board_title><div class=gt_count_label><span class=gt_price>".$price."</span> ".__("UE","smp")."</div></div>";
			$gt_components		.= "</div>";
			return $gt_components;
		}
		static function get_components_form_2($id, $component_type=0, $count_=1)
		{
			$gt_components	= "<div style='padding-top:0;'>";
			$comp		= $component_type ==0 ? get_post_meta($id, 'components', true) : get_post_meta($id, 'components2', true);
			$comp		= is_array($comp) 	? $comp : array();
			$count	= 	count($comp);
			if($count)
			{
				$gt_components	.= '<div>'.__("Components", "smp").'</div>';
				foreach($comp as $c)
				{
					$gid			= $c[0]["goods_type"];
					$gt				= get_post($gid);
					$gttrum_id		= get_post_thumbnail_id($gid);
					$gttrum			= wp_get_attachment_image_src($gttrum_id, array(32,32), array("class"=>$scgt));
					$gt_cnt			= $c[0]["goods_value"] * $count_;
					$fill			= get_post_meta($gid, 'color', true);
					$gt_components	.= "
					<div class='gt_compon2'>".
						"<div class='smp_gb_rounded_60'>".
							Goods_Type::get_trumbnail( 
								$gid,52, 
								array(
									'class'		=> 'smp_rounded', 
									'margin'	=> '0'
								)
							).
						"</div>
						<br>".
						$gt->post_title.
						"<span>". $gt_cnt ."</span>".
					"</div>";
					//for($i=0; $i < $gt_cnt; $i++)
					//	$gt_components	.= "<div style=background-color:#" . $fill . "; margin:3px; float:left; ><img src=".$gttrum[0]."></div>";
					//	$gt_components	.= "<div class='smp_gb_rounded_42'>".Goods_Type::get_trumbnail( $gid, 42, array('class'=>'smp_good_type_picto_rnd', 'margin'=>-2))."</div>";
				}					
			}
			else
			{
				$gt_components	.= __("This goods type not have components.",'smp');
			}
			$price				= get_post_meta($id, 'price', true);
			if($price != "")	
			{
				$gt_components	.= "<div style=width:100%;><h5>".__("Cost Price", "smp")."</h5>".$price." ".__("UE","smp")."</div>";
			}
			$gt_components		.= "</div>";
			return $gt_components;
		}
		static function all_ids_by_industry($industry_id)
		{
			$args		= array(
									"numberposts"		=> -1,
									"offset"			=> 0,
									'order'     		=> 'ASC',
									'post_type' 		=> 'goods_type',
									'post_status' 		=> 'publish',
									'fields'			=> 'ids',
									'meta_query' 		=> array(
																	array(
																		"key" 		=> "industry",
																		"value" 	=> $industry_id,
																		"compare" 	=> "=",
																	  )
																 ),
								);
			return get_posts($args);
		}
		static function get_form($gid, $class="", $is_table=true)
		{
			$html				= 	$is_table ? "<tr class='$class'><td style='width:80px;'>" : "<div class='gt_form_stroke'><span>";
			$html				.= 	self::get_trumbnail($gid) . "<div><board_title>". self::get_instance($gid)->post_title."</board_title></div>";
			$html				.= 	$is_table ? "</td>" : "</span><span>";
			$html				.= 	$is_table ? "<td>" : "<span>";			
			$html				.= 	self::get_components_form($gid);
			$html				.= 	$is_table ? "</td>" : "</span><span>";
			$html				.= 	$is_table ? "<td>" : "<span>";			
			$html				.= 	self::get_complexity($gid);
			$html				.= 	$is_table ? "</td>" : "</span><span>";
			$html				.= 	$is_table ? "<td>" : "<span>";
			$prod				= 	Factories::get_produse_by_goods_type($gid);
			$html				.= 	"<b>".$prod['produce'] . "</b> / ". $prod['unproduce'];
			$html				.= 	$is_table ? "</td>" : "</span><span>";
			$html				.= 	$is_table ? "<td>" : "<span>";
			$html				.= 	apply_filters("smp_goods_type_table", "", $gid);
			$html				.= 	$is_table ? "</td>" : "</span>";
			$html				.= 	$is_table ? "</tr>" : "</div>";
			return $html;
		}
		
		static function smp_my_tools($args)
		{
			
			$gap			=  "<h1>" . __("Goods and Products", "smp") . "</h1>";
			$ind_ids		= SMP_Industry::get_all_indastry_ids();
			foreach($ind_ids as $industry_id)
			{
				$gtids		= self::all_ids_by_industry($industry_id);
				$industry	= SMP_Industry::get_instance($industry_id);
				
				$gap		.= "<div  class='smp_goods_type_table'>
				<h3>" . $industry->body->post_title .  " (" .$industry->get_resourse_name() . ")</h3>";
				if(!count($gtids))
				{
					$gap		.= "<div class='smp-comment'>".__("No goods type in this Industry", "smp")."</div>";
				}
				else
				{
					$gap		.= "
					<table>
						<thread>
							<th>".
								__("Goods type", "smp").
							"</th>
							<th style='width:25%;'>".
								__("Expenses","smp"). 
							"</th>
							<th>".
								__("Produse Complexity","smp"). 
							"</th>
							<th>".
								__("Full production","smp"). " /<br>	". __("Real production", "smp").
							"</th>
							<th>".
								__("","smp").
							"</th>
						</tread>";
					$i=0;
					foreach($gtids as $gtid)
					{
						$gap	.= self::get_form($gtid, ($i++%2==0) ? "ob":"ab");
					}	
				}
				$gap		.= "</table></div>";
			}
			$args[]		= array("title" => "<div class='smp_tool_icon'><img src='" . SMP_URLPATH . "img/flot_ico_op.png'></div>", "hint" => __("Goods and Products", "smp"), "slide" => $gap);
			return $args;
		}
		static function get_store_list($goods_type_id)
		{
			global $user_iface_color;
			
				$arg		= array(
									'numberposts'	=> -1,
									'offset'    	=> 0,
									'orderby'  		=> 'title',
									'order'     	=> 'ASC',
									'post_type' 	=> GOODS_BATCH_NAME,
									'post_status' 	=> 'publish',
									"meta_query"	=> array(
																'relation'				=> "AND",
																array(
																			"key"		=> "store",
																			"value"		=> 1,
																			"compare"	=> "=",
																	 ),
																array(
																			"key"		=> "goods_type_id",
																			"value"		=> $goods_type_id,
																			"compare"	=> "=",
																	 )
															)
								);
			$goods_batchses	= get_posts($arg);
			if(!count($goods_batchses))
			{
				return "<div class=smp-comment>" . __("No Goods of this type", "smp") . "</div>";				
			}
			$goods_type	= self::get_instance($goods_type_id);
			$slide		.= "<div class='smp-pr-main' id='production-".$goods_type_id."' button_id='".$ii++."' factory_id='".$goods_type_id."' style=''>";
			$slide		.= '<h3>'. __("Goods type", "smp").' <span style=\'font-weight:700; color:'.$user_iface_color.'!important\'><a href=\'/?goods_type='.$goods_type->post_name.'\'>' . $goods_type->post_title.'</a></span></h3>';
			$slide		.= "<div class='smp-store-batch-list'>";
			foreach($goods_batchses as $goods_batch)
			{
				$gb		= SMP_Goods_Batch::get_instance($goods_batch->ID);
				if($gb->user_is_owner())
				{
					$post_data			= 1;
				}
				else
				{				
					$post_data			= 0;
				}
				$slide	.= $gb->get_stroke($gb->body, $post_data);	
			}
			$slide		.= "</div>";
			$slide		.= "</div>";	
			return $slide;
		}
	}
	
?>