<?php
	class SMP_Currency_Type extends SMC_Post
	{
		static function get_type()
		{
			return SMP_CURRENCY_TYPE;
		}
		static function add_location_type_to_location($tax_name)
		{
			global $SMP_Currency;
			?>
			<div class="form-field">
				<label for="term_meta[currency_type]"><?php _e("Default Currency Type", "smp"); ?></label>
				<?php
					echo $SMP_Currency->wp_droplist_currency_type(array("name"=>"term_meta[currency_type]", "id"=>"term_meta[currency_type]", "class"=>"chosen-select", 'selected' => $SMP_Currency->options['default_currency_type_id']))
				?>
			</div>
			<?php
		}
		static function smc_location_tab_over_params($text, $location_id, $tax)
		{
			$term_meta 		= SMC_Location::get_term_meta( $location_id );
			$ct_id			= $term_meta['currency_type'];
			$ct				= SMP_Currency_Type::get_instance($ct_id);
			return $text . __("Default Currency Type", "smp") . ": <b>" . $ct->get("post_title")."</b><br>";
		}
		static function edit_location_type_to_location($term, $tax_name)
		{		
			global $SMP_Currency;	
			$t_id 			= $term->term_id;	 
			$term_meta 		= SMC_Location::get_term_meta( $t_id );//get_option( "taxonomy_$t_id" ); 
			?>
			<tr class="form-field">
				<th scope="row" valign="top">
					<label for="term_meta[currency_type]"><?php _e("Default Currency Type", "smp"); ?></label></th>
				</th>
				<td>
				<?php
					echo $SMP_Currency->wp_droplist_currency_type(array("name"=>"term_meta[currency_type]", "id"=>"term_meta[currency_type]", "class"=>"chosen-select", "selected"=>$term_meta[currency_type]))
				?>
				</td>
			</tr>
			<?php
		}
		static function location_columns($theme_columns) {
			$first					= array_slice($theme_columns,0, 4);
			$first['curr_type']		= __("Default Currency Type", "smp");
			$last					= array_slice($theme_columns,4);
			$theme_columns			= array_merge($first, $last);
			return $theme_columns;
		}
		static function manage_location_columns($out, $column_name, $theme_id) 
		{
			$tax	= SMC_Location::get_term_meta($theme_id);//get_option("taxonomy_$theme_id");
			switch ($column_name) 
			{
				case 'curr_type':
					$mt			= (int)$tax["currency_type"];
					$ct			= self::get_instance($mt);
					$out 		.= $ct->body->post_title;
					break;
			}
			return $out; 
		}
		
		//Currency Type
		static function add_SMP_Currency_Type()
		{
				$labels = array(
					'name' => __('Currency Type', "smp"),
					'singular_name' => __("Currency Type", "smp"), // ����� ������ ��������->�������
					'add_new' => __("add Currency Type", "smp"),
					'add_new_item' => __("add Currency Type", "smp"), // ��������� ���� <title>
					'edit_item' => __("edit Currency Type", "smp"),
					'new_item' => __("add Currency Type", "smp"),
					'all_items' => __("all Currency Types", "smp"),
					'view_item' => __("view Currency Type", "smp"),
					'search_items' => __("search Currency Type", "smp"),
					'not_found' =>  __("Currency Type not found", "smp"),
					'not_found_in_trash' => __("no found Currency Type in trash", "smp"),
					'menu_name' => __("Currency Type", "smp") // ������ � ���� � �������
				);
				$args = array(
					'labels' => $labels,
					'public' => true,
					'show_ui' => true, // ���������� ��������� � �������
					'has_archive' => true, 
					'exclude_from_search' => true,
					'menu_position' => 21, // ������� � ����
					'show_in_menu' => "Metagame_Finance_page",
					'supports' => array(  'title', 'thumbnail')
					//,'capabilities' => 'manage_options'
					,'capability_type' => 'post'
				);
				register_post_type(SMP_CURRENCY_TYPE, $args);
		}
		
		static function my_extra_fields_ca_type() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(SMP_Currency_Type, 'extra_fields_box_func_ca_type'), SMP_CURRENCY_TYPE, 'normal', 'high'  );
		}
		static function extra_fields_box_func_ca_type( $post )
		{
			$ct			= self::get_instance($post->ID);
			$rate		= $ct->get_meta('rate');
			$rate		= $rate=="" ? 1 : $rate;
			$turnover	= $ct->get_turnover();
			$abbrev		= $ct->get_meta('abbreviation');
			?>				
				<div class="h">
					<label for="abbreviation"><?php echo __("Abbreviation", "smp") ;?></label><br>
					<input name="abbreviation" value='<?php echo $abbrev;?>'/>
				</div>			
				<div class="h">
					<label for="rate"><?php echo __("Rate", "smp") ;?></label><br>
					<input name="rate" type="number" step="0.01" value="<?php echo $rate;?>"/>
				</div>				
				<div class="h">
					<label for="turnover"><?php echo __("Turnover", "smp") ;?></label><br>
					<input name="turnover" type="number" step="1" value="<?php echo $turnover;?>"/>
				</div>	
			<?php
			wp_nonce_field( basename( __FILE__ ), 'goods_ca_type_metabox_nonce' );
		}
		
		static function true_save_box_data_ca_type ( $post_id) 
		{					
			global $table_prefix;
			// ���������, ������ �� ������ �� �������� � ����������
			if ( !isset( $_POST['goods_ca_type_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['goods_ca_type_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// ���������, �������� �� ������ ���������������
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// ���������, ����� ������������, ����� �� �� ������������� ������
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;				 	
			
			update_post_meta($post_id, 'rate',			$_POST['rate']);
			update_post_meta($post_id, 'abbreviation',	$_POST['abbreviation']);
			update_post_meta($post_id, 'turnover',		$_POST['turnover']);
					
			return $post_id;
		}
		
		static function add_views_column_type( $columns )
		{
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
					"IDs"	 		=> __("ID", 'smp'),
				  "title" 			=> __("Title"),
				  "abbreviation" 	=> __("Abbreviation", "smp"),
				  "rate" 			=> __("Rate", "smp"),
				  "turnover" 		=> __("Turnover", "smp"),
				 
			   );
			return $posts_columns;			
		}
		// ��������� ����������� ����������� �������
		static function add_views_sortable_column_type($sortable_columns){
			$sortable_columns['ids'] 			= 'IDs';	
			$sortable_columns['rate'] 			= 'rate';	
			$sortable_columns['abbreviation'] 	= 'abbreviation';
			$sortable_columns['turnover'] 		= 'turnover';
			return $sortable_columns;
		}	
		// ��������� ������� �������	
		static function fill_views_column_type($column_name, $post_id)
		{
			$post			= get_post($post_id);
			$ct				= self::get_instance($post);
			switch( $column_name) 
			{				
				case "IDs":	
					echo "<br>
					<div class='ids'><span>ID</span>".$post_id. "</div>
					<p>
					<div class='button' add_bid='$post_id' post_type='SMP_Currency_Type'>" . __("Double", "smc") . "</div>";
					break;
				case "rate":
					$t		= $ct->get_rate();
					echo $t;
					break;	
				case "abbreviation":
					$t		=  $ct->get_abbreviation();
					echo $t;
					break; 		
				case "turnover":
					$t		= $ct->get_turnover();
					echo $t;
					break; 			
			}	
		}
		
		//Factory
		static function my_extra_fields_factory($post_id)
		{
			
		}
		static function true_save_box_data_factory ( $post_id ) 
		{					
			return $post_id;
		}
		static function add_views_column_factory( $columns )
		{
			$columns['defct'] = __("Default Currency Type", "smp");
			return 	$columns;	
		}
		static function add_views_sortable_column_factory($sortable_columns){
			$sortable_columns['defct'] 			= 'defct';
			return $sortable_columns;
		}	
		static // ��������� ������� �������	
		function fill_views_column_factory($column_name, $post_id)
		{
			switch( $column_name) 
			{				
				case 'defct':
					$tid	= get_post_meta($post_id, 'currency_type_id', 1);
					$t 		= SMP_Currency_Type::get_instance($tid);
					echo $tid ? $t->body->post_title."<br><div class='ids'><span>ID</span>".$tid."</div>" : "";
					break;
			}		
		}
		static function wp_dropdown($params)
		{
			global $SMP_Currency;
			if(!is_array($params))
				$params	= array();
			$cts		= self::get_all();
			if(count($cts) == 0)
			{
				return "<span style='margin-left:10px;'>". "<img class='coin' src='" . SMP_URLPATH ."icon/UE.png'/>"."</span>";
			}
			else if(count($cts) == 1)
			{
				$ct 	= static::get_instance($cts[0]);
			
				return "<input type='hidden' name='".$params['name']."' id='".$params['id']."' value='".$ct->id."'><span style='margin-left:10px;'>". $ct->get_abbreviation()."</span>";
			}
			else
				return "<label class='smc_input'>".parent::wp_dropdown($params)."</label>";
		}
		//Batch
		static function my_extra_fields_batch($text, $post_id)
		{
			global $SMP_Currency;
			$batch					= Batch::get_instance($post_id);
			$currency_type_id		= $batch->get_meta('currency_type_id');
			$currency_type_id		= $currency_type_id ? $currency_type_id: $SMP_Currency->options['default_currency_type_id'];
			return $text . "
			<div>
				<div class='h'>
					<label  class='h2' for='quality_modificator'>".__("Currency Type", "smp")."</label><br>
					<div class='styled-select state rounded'>".
					$SMP_Currency->wp_droplist_currency_type(array("name"=>"currency_type_id", "id"=>"currency_type_id", "class"=>"h2", "selected"=>$currency_type_id)).
				"	</div>
				</div>
			</div>";
		}
		static function true_save_box_data_batch ( $post_id ) 
		{
			// ���������, �������� �� ������ ���������������
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// ���������, ����� ������������, ����� �� �� ������������� ������
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;			
			update_post_meta($post_id, 'currency_type_id', 	$_POST['currency_type_id']);					
			return $post_id;
		}
		static function add_views_column_batch( $columns )
		{
			$columns['defct'] = "<i class='fa fa-money fa-2x hinter' data-hint='" . __("Currency Type", 'smp') . "' height=20></i>";
			return 	$columns;	
		}
		static function add_views_sortable_column_batch($sortable_columns){
			$sortable_columns['defct'] 			= 'defct';
			return $sortable_columns;
		}	
		static // ��������� ������� �������	
		function fill_views_column_batch($column_name, $post_id)
		{
			switch( $column_name) 
			{				
				case 'defct':
					$tid	= get_post_meta($post_id, 'currency_type_id', 1);
					$t 		= get_post($tid);
					echo $tid ? $t->post_title : "";
					break;
			}		
		}
		static function smp_butch_slide_5($arr, $batch_id)
		{
			if (!SolingMetagameProduction::is_finance())	return "";
			if (is_array($arr))
			{
				//return "";
			}
			$batch			= SMP_Goods_Batch::get_instance( $batch_id );
			return array( "title" => __("Sale", "smp"), "slide" => $batch->get_paying_form() );
			
			
			return array("title" => __("Currency Type", "smp"), "slide" => self::my_extra_fields_batch("<div class=smp-batch-tab-slide><div class=smp-table-null>", $batch_id) . "</div><div class='smp-table-null smc_margin_top'> <div class='button smc_padding'>" . __("Change Currency Type", "smp") . "</div></div></div>");
		}
		
		static function get_flots()
		{
			
		}
		static function get_ct_flots()
		{
			global $wpdb;
			$cts	= get_posts(array("numberposts"=>-1, "post_type"=>SMP_CURRENCY_TYPE, "fields" => "ids"));
			foreach($cts as $ct_id)
			{
				
			}
		}
		
		
		static function after_install_sticker()
		{
			if(get_current_screen()->base != 'dashboard' && get_current_screen()->base != 'toplevel_page_Metagame_Finance_page')	return;
			echo  "
			<div id='curr_stat' class='updated notice smc_notice_ is-dismissible1' id_btn='install_ermak_production_notice' >
				<board_title>".__("Currency statistics", "smp") . "</board_title>
				<p>".
					self::get_currency_statistics().
				"</p>
				<span class='smc_desmiss_button' setting_data='no_supportedd'>
					<span class='screen-reader-text'>".__("Close")."</span>
				</span>
			</div>";
		}
		static function get_currency_statistics()
		{
			$can		= current_user_can("administrator") && is_admin();
			$ss			= array();
			$cts		= self::get_all_ids();
			$text		.= "
			
			<table>
				<tread>
					<th>" . __("Currency Type","smp"). "</th>
					<th>" . __("Rate","smp"). "</th>
					<th>" . __("all Invoices","smp") . "</th>
					<th>" . __("all Currency Accounts", "smp") . "</th>
					<th>" . __("Turnover", "smp") . "</th>
					<th>" . __("Summae", "smp") . "</th>";
			if($can)
				$text	.= "<th style='width:360px;'>" . __("Change summaes in Accounts", "smp") . "</th>";
			
			$text		.= "	</tread>";
			if(!count($cts))	return __("No one Currency Type enabled", "smp");
			foreach($cts as $ct)
			{
				$ss[$ct]			= array();
				$ss[$ct]['invoice']	= 0;
				$ss[$ct]['ca']		= 0;
			}
			
			$arg				= array(
														
											'numberposts'	=> 0,
											'offset'    	=> 0,
											'post_type' 	=> 'smp_invoice',
											'fields'		=> 'ids',
											'post_status' 	=> 'publish',
										);
			$smp_invoices		= get_posts($arg);
			foreach($smp_invoices	as $smp_invoice)
			{
				$currency_type			= get_post_meta($smp_invoice, "currency_type", true);
				//if(!isset($ss[$currency_type]))
				//	$ss[$currency_type] 		= array();
				$summae							= (int)get_post_meta($smp_invoice, "summae", true);
				if(!isset($ss[$currency_type]['invoice']))
					$ss[$currency_type]['invoice']= 0;
				$ss[$currency_type]['invoice']	+= $summae;
			}
			$ca_args	= array(
									'numberposts'	=> -1,
									'offset'    	=> 0,
									'post_type' 	=> SMP_CURRENCY_ACCOUNT,
									'post_status' 	=> 'publish',
									'fields'		=> 'ids',
									'meta_query' 	=> array(
																array(
																		"key"		=> "is_lock",
																		"value"		=> 1,
																		'compare'	=> '!='		
																	),
															),
								);
			$cas		= get_posts($ca_args);
			foreach($cas	as $ca)
			{
				$currency_type			= get_post_meta($ca, "ctype_id", true);
				//if(!isset($ss[$currency_type]))
				//	$ss[$currency_type] 		= array();
				$summae							= (int)get_post_meta($ca, "count", true);
				if(!isset($ss[$currency_type]['ca']))
					$ss[$currency_type]['ca']= 0;
				$ss[$currency_type]['ca']	+= $summae;
			}
			$i=0;
			foreach($ss as $s=>$val)
			{
				$class		= $i++%2==0 ? "ob" : "ab";
				$ct			= SMP_Currency_Type::get_instance($s);
				if($ct)
				{
					$ct_title	= $ct->get("post_title");
					$ct_rate	= $ct->get_meta("rate");
				}
				$text		.= 
				"<tr class=$class>
					<td>" . $ct_title . "</td>
					<td>" . $ct_rate . "</td>
					<td><b>". $val['invoice'] . "</b> ".__("UE", "smp")."</td>
					<td><b>". $val['ca'] . "</b> ".__("UE", "smp")."</td>
					<td><b>". $ct->get_turnover() . "</b> ".__("UE", "smp")."</td>
					<td><b>". ((int)$val['invoice'] + (int)$val['ca']) . "</b> ".__("UE", "smp")."</td>";
				if($can)
					$text	.= "<td><div class='change_sa_range'><input name='change_sa_$s' class='change_sa_range1' style='width:300px;' ctid='$s'/></div><span class='button change_ct' ctid='$s'><i class='fa fa-check'></i></span></td>";
				$text		.= "</tr>";
			}
			$text			.= "
			</table>";
			return $text;
		}
		function get_abbreviation()
		{
			return $this->get_meta("abbreviation");
		}
		
		function get_rate()
		{
			$rate	=$this->get_meta("rate");
			if($rate==0)	$rate=1;
			return $rate;
		}
		function convert($summ_ue, $rounded=true)
		{
			$s		= ($summ_ue / $this->get_rate());
			return $rounded ? (int)$s : ((int)($s * 10))/10;
		}
		function get_price($summae_ue, $summ_class="", $rounded=true)
		{
			return "<st class='".$summ_class."'>". $this->convert($summae_ue, $rounded)."</st> " . $this->get_abbreviation();
		}
		static function get_object_price($currency_type_id, $summ_ue, $summ_class="", $rounded=true)
		{
			$ct		= static::get_instance($currency_type_id);
			if(!$ct->body->ID)
				return "<st class='".$summ_class."'>". ((int)$summ_ue) . "</st> " .  "<img class='coin' src='" . SMP_URLPATH ."icon/UE.png'/>"; 
			else
				return $ct->get_price($summ_ue, $summ_class, $rounded);
		}
		//
		function by_ue($summ)
		{
			return $summ_ue * $this->get_rate();
		}
		function get_turnover()
		{
			return (int)$this->get_meta("turnover");
		}
		function update_turnover($summ)
		{
			$turnover	= $this->get_turnover();
			$turnover 	+= $summ;
			$this->update_meta("turnover", $turnover);
			return $turnover;
		}
	}
?>