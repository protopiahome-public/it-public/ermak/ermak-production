<?php
	class Batch extends SMC_Post
	{
		/*
		TODO: change post_meta Table to smp_goods_batch Table in DB
		*/
		public $type;
		function __construct($id, $type=-1)
		{
			parent::__construct($id);
			$this->type		= $type != -1 ? $type : GOODS_BATCH_NAME;
		}
		
		function get_post_meta($key)
		{
			return get_post_meta($this->id, $key, true);
		}
		function update_post_meta($key, $value)
		{
			update_post_meta($this->id, $key, $value);
		}
		
		function get_owner_id()
		{
			return $this->get_meta("owner_id");
		}
		function get_dislocation_id()
		{
			return $this->get_meta("dislocation_id");
		}
		function get_goods_type_id()
		{
			return $this->get_meta("goods_type_id");
		}
		function get_count()
		{
			return (int)$this->get_meta("count");
		}
		function get_price()
		{
			return (int)$this->get_meta("price");
		}
		//=====================================
		//
		//	
		//
		//=====================================
		
		function update_metas($metas, $batch_type=-1)
		{
			global $SolingMetagameProduction, $SMC_Object_Type;
			if($batch_type==-1)	$batch_type = GOODS_BATCH_NAME;	
			include_once (SMC_REAL_PATH."class/SMC_Object_type.php");
			$SMC_Object_Type	= SMC_Object_Type::get_instance();
			$gb_metas			= array_keys($SMC_Object_Type->get_meta($batch_type));
			foreach($gb_metas as  $key)
			{
				if(isset($metas[$key]) || $key == "transportation_id" || $key == "is_permission")
				{
					continue;
				}
				if($key == "price" || $key == "waybill_tact" || $key == "count")						continue;
				if(!$SolingMetagameProduction->get_gb_value("best_before") 	&& $key=="best_before") 	continue;
				if(!$SolingMetagameProduction->get_gb_value("store") 		&& $key=="store") 			continue;
				if(!$SolingMetagameProduction->get_gb_value("factory_id") 	&& $key=="factory_id") 		continue;
				if(!$SolingMetagameProduction->get_gb_value("quality") 		&& $key=="quality") 		continue;
				$metas[$key]	= 	$this->get_meta( $key );
			}
			//insertLog("update_metas", $metas);
			return $metas;
		}
		
		static function insert2( $metas, $batch_type=-1 )
		{
			global $SolingMetagameProduction;
			if($batch_type==-1)	$batch_type = GOODS_BATCH_NAME;	
			$usage_duration		= get_post_meta($goods_type_id, "usage_duration", true);
			
			$new_batch = array(
								  'post_title'   			=> __("Batch", "smp"),
								  'post_type' 				=> $batch_type,
								  'post_content' 			=> ""  ,
								  'post_status'  			=> 'publish',
								);
			$batch_id		= wp_insert_post($new_batch);	
			$b				= self::get_instance($batch_id);
			update_post_meta($batch_id, "goods_type_id", 	$goods_type_id);
			foreach($metas as $key=>$val)
			{		
				//if(!$SolingMetagameProduction->isBatchOpt($key)) continue;
				switch($key)
				{	
					case "factory_id":
						$factory		= Factory::get_factory($val);
						if(!isset($factory))	continue;
						$quality		= $factory->get_post_meta('quality');
						if($quality == 0)	$quality = 100;
						$best_before	= (int)(($usage_duration-1) / 100 * $quality) + 1;
						update_post_meta( $batch_id, $key, $val );
						//update_post_meta( $batch_id, 'best_before', $best_before );
						break;
					case "quality":
					case "dislocation_id":
					case "store":
					case "owner_id":
					case "count":
					case "best_before":
					case "goods_type_id":
					default:
						(int)update_post_meta($batch_id, $key, $val);
						break;
				}				
			}
			return $b;
			
		}
		static function add_batch2( $metas, $batch_type=-1 )
		{ 
			global $SolingMetagameProduction;
			if($batch_type==-1)	$batch_type = GOODS_BATCH_NAME; 
			//insertLog("add_batch2", $metas);
			$gb				= $SolingMetagameProduction->get_gb_options();
			$r				= $gb["combine"]	? self::find_one_batch(  $metas , $batch_type) : null;
			//insertLog("Batch.php", array("action" => "add_batch2", 'r'=>$r));
			if(isset($r))
			{
				$batch		= self::get_instance($r->ID);
				$batch->add_goods($metas['count']);
				//insertLog("Batch.php", array("action" => "add_batch2", "id" => $batch->id, "count" => $metas['count'], 'is_new'=>false ));
			}
			else
			{
				$batch		= self::insert2( $metas,$batch_type );				
				//insertLog("Batch.php", array("action" => "add_batch2", "id" => $batch->id, "count" => $metas['count'], 'is_new'=>true ));
			}
			//apply_filters("smp_goods_batch_change_meta", $batch->id, $metas['owner_id'], $metas['dislocation_id']);
			return $batch;
		}		
		
		
		static function find_all_batch2( $meta, $count = -1, $batch_type=-1 ) 
		{
			global $SolingMetagameProduction;
			if(!is_array($meta))	return array();
			if($batch_type==-1)	$batch_type = GOODS_BATCH_NAME; 
			$meta_query	= array(
				'relation'		=> 'AND',
				
				array(
					'value'		=> $meta['owner_id'],
					'key' 		=> 'owner_id',
					'compare' 	=> '='
					),
				
			);
			foreach($meta as $key => $val)
			{
				global $SolingMetagameProduction;
				//if(! $SolingMetagameProduction->isBatchOpt($key)) continue;	
				if($key=='store') 		$val	= (int)$val;
				if($key	== "count")		continue;
				if($key == "owner_id")	continue;
				if(!$SolingMetagameProduction->get_gb_value['best_before'] && $key== "best_before") continue;
				switch($key)
				{
					case "best_before":
						$compare	= ">";
						break;
					default:
						$compare	= "=";
						break;
				}
				$meta_query[] = array(
					'key' 		=> $key,
					'value'		=> $val,
					'compare' 	=> $compare
				);
			}			
			$args = array(
				'numberposts'	=> $count,
				'offset'     	=> 0,
				'orderby' 		=> 'id',
				'order'  		=> 'ASC',											
				'post_status'	=> 'publish',											
				'post_type'		=> $batch_type,
				'meta_query'	=> $meta_query,
			);		
			//insertLog("Batch.find_all_batch2 1", array("action" => "find_all_batch2", "args"=>$args));
			$batches			= get_posts( $args );			
			//insertLog("Batch.find_all_batch2 2", array("action" => "find_all_batch2", "count_batches"=>count($batches)));
			//insertLog("Batch.find_all_batch2 3", array("action" => "find_all_batch2", "batches"=>$batches));
			return $batches;
		}
		
		static function find_one_batch( $meta , $batch_type=-1 )
		{
			global $SolingMetagameProduction;
			if(!is_array($meta))	return array();
			if($batch_type==-1)	$batch_type = GOODS_BATCH_NAME; 
			$meta_query	= array(
				'relation'		=> 'AND',
				
				array(
					'value'		=> $meta['owner_id'],
					'key' 		=> 'owner_id',
					'compare' 	=> '='
					),
				
			);
			if(!$SolingMetagameProduction->get_gb_value['best_before'])
			{
				unset($meta['best_before']);
			}
			foreach($meta as $key => $val)
			{
				if($key	== "count")		continue;
				if($key == "owner_id")	continue;
				switch($key)
				{
					case "best_before":
						$compare	= ">";
						break;
					case "is_blocked":
					case "currency_type_id":
					case "store":
						$val		= (int)$val;
						$compare	= "=";
						break;
					default:
						$compare	= "=";
						break;
				}
				$meta_query[] = array(
					'key' 		=> $key,
					'value'		=> $val,
					'compare' 	=> $compare
				);
			}			
			$args = array(
				'numberposts'	=> 1,
				'offset'     	=> 0,
				'orderby' 		=> 'id',
				'order'  		=> 'ASC',											
				'post_status'	=> 'publish',											
				'post_type'		=> $batch_type,
				'meta_query'	=> $meta_query,
			);		
			$batches			= get_posts( $args );
			//insertLog("find_one_batch", array('args'=>$args, "batches"=>$batches));				
			return $batches[0];
		}
		
		static function search( $meta, $batch_type=-1, $fields="all" )
		{			
			global $SolingMetagameProduction;
			if(!is_array($meta))	return array();
			if($batch_type==-1)	$batch_type = GOODS_BATCH_NAME; 
			//insertLog("batch search", $meta);
			$meta_query	= array( 'relation' => 'AND' );
			$include			= array();
			foreach($meta as $key => $value)
			{
				if($key	== "count")		continue;
				if($value == "-1" || !isset($value) || $value=="")	continue;
				if($key == "id")
				{
					$include[] = $value;
					continue;
				}
				//if($key == "owner_id")	continue;
				switch($key)
				{
					case "best_before":
						$compare	= ">";
						break;
					case "dislocation_id":
						$value		= !is_array($value)	? $value = array($value): $value;
						$operator	= "OR";
						break;
					case "is_blocked":
					case "currency_type_id":
					case "store":
						$value		= (int)$value;
						$compare	= "=";
						break;
					default:
						$compare	= "=";
						break;
				}
				$meta_query_11	= array(
					'key' 		=> $key,
					'value'		=> $value,
					'compare' 	=> $compare
				);
				if($operator)
					$meta_query_11['operator']	= $operator;
				if(!$meta_query_11['compare'])
					unset($meta_query_11['compare']);
				$meta_query[]		= $meta_query_11;
				unset($operator);
				unset($compare);
			}	
			if(count($meta_query) == 1) $meta_query = array();
			$args = array(
				'numberposts'	=> -1,
				'offset'     	=> 0,
				'orderby' 		=> 'id',
				'order'  		=> 'ASC',											
				'post_status'	=> 'publish',											
				'post_type'		=> $batch_type,
				"fields"		=> $fields,
				'meta_query'	=> $meta_query,
			);	
			if(count($include))
				$args['include']= implode(",", $include);
			//insertLog("batch search", $args);
			$batches			= get_posts( $args );
			//insertLog("batch search", count($batches));
			wp_reset_postdata();
			//insertLog("find_one_batch", array('args'=>$args, "batches"=>$batches));				
			return $batches;
		}
		
		public function remove_goods($count)
		{
			if($count < 0)		return;
			$oldCount	= get_post_meta($this->id, "count", true);
			if($count >= $oldCount)
			{
				$count			= $oldCount;
			}
			$new_count			= $oldCount - $count;
			update_post_meta($this->id, 'count', $new_count);			
			//insertLog("Batch.php", array("action" => "remove_goods", "from_id" => $this->id, "count" => $count, "left" => $new_count));
			return $count;
		}
		
		function clear_null()
		{
			$count			= get_post_meta($this->id, "count", true);
			if($count<=0)
			{
				wp_delete_post($this->id, true);
				return false;
			}
			return $this;
		}
		function remove()
		{
			wp_delete_post($this->id, true);
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/*	удалить часть партии */
		public function rize_batch_delete($count) 
		{
			$ost 					= $this->remove_goods($count);
			do_action("smp_rize_batch", $batch->ID);
			$this->clear_null();
			return $ost;
		}
		
		
		/* разделить пачку и отдать часть другому владельцу */
		static function rize_batch_owner2(
											$old_batch,				// Batch Object
											$new_location_owner_id,	// Location tax term_id
											$count					// count
										 )		
		{	
			//insertLog("Batch.rize_batch_owner2", array("change owner"=>$new_location_owner_id, "count"=>$count, "id"=>$old_batch->id));
			$new_count	= $old_batch->remove_goods($count);
			$new_owner	= SMC_Location::get_instance($new_location_owner_id);
			$meta		= array();
			$meta["owner_id"]			= $new_location_owner_id;
			$meta["count"]				= $new_count;
			$meta["store"]				= 0;
			$meta["transportation_id"]	= "";
			$b		= self::add_batch2(	
										$old_batch->update_metas( $meta), $old_batch->body->post_type	 
									  );
			//insertLog("Batch.rize_batch_owner2", array("NEW BATCH"=>$b));
			$old_batch->clear_null();
			//apply_filters("smp_goods_batch_change_meta", $b->id, $new_location_owner_id, $b->get_meta("dislocation_id"));
			return array($b, $new_count);					
		}
		
		/*	часть партии - в магазин	*/
		static function rize_batch_store2(
											$batch,
											$count,
											$price
										  )
		{
			$st							= (bool)get_post_meta($batch->id, "store", true);
			$new_count					= $batch->remove_goods($count);		
			$meta						= array();
			$meta["owner_id"]			= $batch->get_meta("owner_id");
			$meta["count"]				= $new_count;
			$meta["store"]				= !$st;
			$meta["price"]				= $price;
			$b		= self::add_batch2(	
										$batch->update_metas( $meta),
										$batch->body->post_type	
									  );
			//insertLog("Batch.php", array("action" => "rize_batch_store2", "from_id" => $batch->id, 'to_id'=>$b->id, "is_store"=>$meta["store"]));
			do_action("smp_rize_batch", $batch->id); 
			$batch->clear_null();
			return array($b, $new_count);
		}
		
		/* разделить и отдать на транспортировку */
		function rize_batch_transportation2( $count, $routh_id, $batch_type )		
		{			
			//insertLog("Batch.rize_batch_transportation2", $batch_type);
			$new_count			= $this->remove_goods($count);
			$meta				= array(
											"count"			=> $new_count,
											"store"			=> false,
											"is_permission"	=> false
										);									
			$metas				= $this->update_metas( $meta, $batch_type );			
			$batch				= self::insert2( $metas, $batch_type );			
			$batch->update_meta( 'transportation_id', $routh_id );
			do_action("smp_batch_start_transportation", $this->id, $batch->id);
			$this->clear_null();
			return array($batch, $new_count);					
		}
		/*окончаие траспортировки - прибытие к месту назначения*/
		function change_dislocation($new_dislocation_id)
		{
			global $SolingMetagameProduction;
			$gb				= $SolingMetagameProduction->get_gb_options();
			$new_count	= $this->get_meta("count");
			//insertLog("Batch.change_dislocation", $new_count);
			//$this->remove_goods($new_count);
			//$metas		= $this->update_metas( array('dislocation_id'=>$new_dislocation_id, 'count'=>$new_count) );	
			//$b			= self::add_batch2( $metas, $this->body->post_type  ); 
			//$this->clear_null(); 
					
			$metas			= $this->update_metas( array('dislocation_id'=>$new_dislocation_id) );
			//insertLog("Batch.change_dislocation", $metas);
			$batch_obj		= $gb["combine"]	? self::find_one_batch(  $metas, GOODS_BATCH_NAME) : null;
			if(isset($batch_obj))
			{
				$batch		= self::get_instance($batch_obj->ID);
				$batch->add_goods($new_count); 
				$this->remove();
				//insertLog("Batch.php", array("action" => "add_batch2", "id" => $batch->id, "count" => $metas['count'], 'is_new'=>false ));
				$result		= sprintf(__("Goods batch combine to identical batch #%s in target Location.", "smp"), $batch_obj->ID);
			}
			else
			{
				$this->update_meta('is_permission', false);
				$this->update_meta('transportation_id', -1);	
				$this->update_meta("dislocation_id", $new_dislocation_id);
				$result		= __("Search this batch in Storage of target Location.", "smp");
			}
			apply_filters("smp_batch_change_dislocation", $b); 
			
			//add log to dislocation Location 
			$goods_type		= Goods_Type::get_instance($this->get_meta("goods_type_id"));
			$owner			= SMC_Location::get_instance($this->get_meta("owner_id"));
			$disloc			= SMC_Location::get_instance($new_dislocation_id);
			$content		= sprintf(
				__("Goods Batch #%s arrived to Storage of %s. Goods type that is %s, owner is %s. ", "smp"),
				"<B>".$this->id."</B>",
				"<B>".$disloc->name."</B>",
				"<B>".$goods_type->post_title."</B>",
				"<B>".$owner->name."</B>"
			). $result;
			SMC_Location::add_log($new_dislocation_id, $content, CHANGE_BATCH_DISLOCATION, get_current_user_id());			
			//insertLog("change_dislocation", $content);
			return array($this->id, $new_count);
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//	OLD CODE
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/*
		static function insert(
									$goods_type_id,
									$factory_id,
									$dislocation_id,
									$owner_id,
									$store,
									$count,
									$best_before = ''
								)
		{
			
			$fac_n			= get_post($factory_id);
			$good_n			= get_post($goods_type_id);
			$usage_duration	= get_post_meta($goods_type_id, "usage_duration", true);
			$qual			= get_post_meta($factory_id, "quality", true);
			$new_batch = array(
								  'post_title'   			=> $fac_n->post_title." (".$good_n->post_title.")",
								  'post_type' 				=> GOODS_BATCH_NAME,
								  'post_content' 			=> ""  ,
								  'post_status'  			=> 'publish',
								);
			$batch_id		= wp_insert_post($new_batch);			
			
			update_post_meta($batch_id, "goods_type_id", 	$goods_type_id);
			update_post_meta($batch_id, "factory_id", 		$factory_id);
			update_post_meta($batch_id, "dislocation_id", 	$dislocation_id);
			update_post_meta($batch_id, "owner_id", 		$owner_id);
			update_post_meta($batch_id, "store", 			$store);
			update_post_meta($batch_id, "count", 			$count);
			update_post_meta($batch_id, "quality", 			$qual);
			if($best_before == '')
				update_post_meta($batch_id, "best_before", (int)(( $usage_duration - 1 ) * $qual / 100)) + 1;
			else
				update_post_meta($batch_id, "best_before", $best_before);
			return new Batch($batch_id);
		
		}
		*/
		static function find_all_batch(
											$goods_type_id,
											$factory_id=-1,
											$dislocation_id=-1,
											$owner_id = -1,
											$store=-1,
											$quality=-1,
											$best_before=-1
										)
		{
			global $SolingMetagameProduction;
			$SMP = $SolingMetagameProduction;
			$options						= $SolingMetagameProduction->options;
			$meta_query						= array(
													'relation'		=> 'AND',
													array(
														'value'		=> $goods_type_id,
														'key' 		=> 'goods_type_id',
														'compare' 	=> '='
													),	
													
												);
			if($owner_id != -1)
			{
				$meta_query[]					= array(
														'value'		=> $owner_id,
														'key' 		=> 'owner_id',
														'compare' 	=> '='
													);
			}
			if($SMP->isBatchOpt('factory_id') && ($factory_id != -1))
			{
				$meta_query[]					= array(
														'value'		=> $factory_id,
														'key' 		=> 'factory_id',
														'compare' 	=> '='
													);
			}
			if($SMP->isBatchOpt('store') && ($store != -1))
			{
				$meta_query[]					=  array(
														'value'		=> $store,
														'key' 		=> 'store',
														'compare' 	=> '='
													);
			}
			if($SMP->isBatchOpt('dislocation_id') && ($dislocation_id != -1))
			{
				$meta_query[]					=  array(
														'value'		=> $dislocation_id,
														'key' 		=> 'dislocation_id',
														'compare' 	=> '='
													);
			}
			if($SMP->isBatchOpt('quality') && ($quality != -1))
			{
				$meta_query[]					=  array(
														'value'		=> $quality,
														'key' 		=> 'quality',
														'compare' 	=> '='
													);
			}
			if($SMP->isBatchOpt('best_before') && ($best_before != -1) && ($best_before < MAX_BEST_BEFORE))
			{
				// TODO: ОШИБКА ЗДЕСЬ, все свалит в кучу
				$meta_query[]					=  array(
														'value'		=> $best_before,
														'key' 		=> 'best_before',
														'compare' 	=> '>'
													);
			}			
			$args = array(
							'numberposts'     	=> -1,
							'offset'          	=> 0,
							'orderby'        	=> 'id',
							'order'           	=> 'ASC',											
							'post_status'     	=> 'publish',											
							'post_type' 		=> GOODS_BATCH_NAME,
							'meta_query' 		=> $meta_query,
						);		
			$batches		= query_posts( $args );						
			return $batches;
		}
		
		static function find_good_type(
											$goods_type_id,
											$factory_id=-1,
											$dislocation_id=-1,
											$owner_id=-1,
											$store=-1,
											$quality=-1,
											$best_before=-1
										)
		{
			$posts		= self::find_all_batch2(
													
													array(
															"goods_type_id"		=> $goods_type_id,
															"factory_id"		=> $factory_id,
															"dislocation_id"	=> $dislocation_id,
															"owner_id"			=> $owner_id,
															"store"				=> $store,
															"quality"			=> $quality,
															"best_before"		=> $best_before
														 ),
													1
												);
			return $posts[0];
		}
		
		
		
		// @$count			- (int)		
		function add_goods($count)
		{
			if($count<0)	return;	
			global $wpdb;
			global $table_prefix;
			$count			= (int)$count;
			$qw				= "UPDATE ".$table_prefix."postmeta SET meta_value= (meta_value+$count) WHERE post_id='" . $this->id . "' AND meta_key='count'";
			$res			= $wpdb->query($qw);
			return $res;			
		}
		
		
		
		/* разделить пачку и отдать часть другому владельцу */
		static function rize_batch_owner(
										$old_batch,				// Batch Object
										$new_location_owner_id,	// Location tax term_id
										$count					// count
								)		
		{
			
			$new_count	= $old_batch->remove_goods($count);
			$b		= Batch::add_batch(	
										$old_batch->get_meta("goods_type_id"),
										$old_batch->get_meta("factory_id"),
										$old_batch->get_meta("dislocation_id"),
										$new_location_owner_id,
										$new_count,
										0,
										$old_batch->get_meta("best_before")	
								   );
			//insertLog("Batch.php", "rize gb #". $old_batch->id . " for new owner. ");
			$old_batch->clear_null();
			return array($b, $new_count);					
		}
		
		
		function rize_batch_transportation(
												$count,					// count
												$routh_id
										  )		
		{
			$goods_type_id		= get_post_meta($this->id, "goods_type_id", true);
			$factory_id			= get_post_meta($this->id, "factory_id", true);
			$dislocation_id		= get_post_meta($this->id, "dislocation_id", true);
			$owner_id			= get_post_meta($this->id, "owner_id", true);
			$store				= 0; 
			$best_before		= get_post_meta($this->id, "best_before", true);
			$new_count			= $this->remove_goods($count);
	
			$batch		= Batch::insert(
										$goods_type_id,
										$factory_id,
										$dislocation_id,
										$owner_id,
										$store, 
										$new_count,
										$best_before
									  );
			$batch->update_meta( 'transportation_id', $routh_id );
			$this->clear_null();
			return array($batch, $new_count);					
		}
		
		function rize_batch_store(
									$batch,
									$count,
									$price
								  )
		{
			$st							= (bool)get_post_meta($batch->id, "store", true);
			$new_count					= $batch->remove_goods($count);			
			$b		= Batch::add_batch(	
									get_post_meta($batch->id, "goods_type_id", true),
									get_post_meta($batch->id, "factory_id", true),
									get_post_meta($batch->id, "dislocation_id", true),
									get_post_meta($batch->id, "owner_id", true),
									$new_count,
									!$st,
									get_post_meta($batch->id, "best_before", true)
									
							   );
			update_post_meta($b->id, "price", $price);
			do_action("smp_rize_batch", $batch->id); 
			$batch->clear_null();
			return array($b, $new_count);
		}
		public function rize_batch_pay(
										$batch,
										$count,
										$new_owner_id
									  )
		{
			$bprice					= get_post_meta($batch->ID, "price", true);
			do_action("smp_rize_batch", $batch->ID);
			return $this->rize_batch_owner($batch, $new_owner_id, $count);
		}
		
		
		
		
		static function add_batch(
									$goods_type_id,
									$factory_id,
									$dislocation_id,
									$owner_id,
									$count,
									$store,
									$best_before
								  )
		{ 
			
			$r				= Batch::find_good_type(
														$goods_type_id,
														$factory_id,
														$dislocation_id,
														$owner_id,
														$store,
														$best_before
													);															
			if(isset($r))
			{
				$batch		= new Batch($r->ID);
				$batch->add_goods($count);
			}
			else
			{
				$batch		= Batch::insert(
											$goods_type_id,
											$factory_id,
											$dislocation_id,
											$owner_id,
											$store, 
											$count,
											$best_before
										  );
			}
			
			return $batch;
		}
		function get_help($is_owner=0)
		{
			global $Soling_Metagame_Constructor;
			global $SolingMetagameProduction;
			$options		= $SolingMetagameProduction->options;
			$id				= $this->id;
			$is_block		= $this->get_meta( "is_blocked");
			$dis_id			= $this->get_meta( 'dislocation_id');
			$owner_id		= $this->get_meta( 'owner_id');
			$quality		= $this->get_meta( 'quality');
			$location		= SMC_Location::get_instance($dis_id);
			$owner			= SMC_Location::get_instance($owner_id);
			$count			= $this->get_meta('count');
			$goods_type_id	= $this->get_meta('goods_type_id');			
			$best_before	= $this->get_meta("best_before");
			$is_trash		= $best_before<0 && $SolingMetagameProduction->is_best_before();
			$color			= get_post_meta($goods_type_id, "color", true);
			$bg				= !$is_trash ? "#FFF"  : "url(".SMP_URLPATH."img/rja.jpg)";
			$gt				= Goods_Type::get_instance($goods_type_id);
			$best_before_text = $best_before > MAX_BEST_BEFORE ? __("infinitely", "smp") :  $best_before . " " . __("circles", "smp");
			$your			= $is_owner ? '<div style="height:20px;"></div><div class=smc-your-label style="bottom:0; right:-10px;">'.__("It's your", "smc").'</div>' : "";
			$help		= 	"
			<div style='background:$bg; display:inline-block; position:relative;width:400px;'>
				<h3 class='help'>".__('Goods batch','smp').": ".$gt->post_title . "</h3>" .
				"<table>
					<tr>
						<td>".
							( 
								Goods_Type::get_trumbnail( $goods_type_id,  70, array("is_hint"=>false)) 
								).
						"</td>
						<td>".
							__("Count", "smp").": <span class='help_select'>". $count."</span><BR>".
							( $options['gb_settings']['quality'] 		? __('Quality', 'smp').": <span>" . SMP_Assistants::get_quality_diagramm( $quality ) . "</span><BR>" : "<br>" ).
							__('Owner', 'smp').": <span>".$owner->name."</span><BR>".
							( $options['gb_settings']['dislocation_id']  ? __("Dislocation", "smp").": <span>". $location->name."</span><BR>" 		: "" ).
							( $options['gb_settings']['best_before']	? __("Best before", "smp").": <span>". $best_before_text . "</span><BR>"	: "" ).
							apply_filters("smp_goods_batch_hint", "", $this->id) .
							"<span class='help_select'>".
							//__("Press pictogramm for execute goods batch", "smp").
							"</span>$your
						</td>
					</tr>
				</table>
			</div>";
			$help		= !$is_trash ? $help : $help.'<div class="is_repaid" data-deg="'.(20 + rand(0,10)).'deg">'.__("Is garbage", "smp").'</div>';
			return $help;
		}
		function draw_container($pos)
		{
			global $Soling_Metagame_Constructor;
			$id				= $this->id;
			$is_block		= $this->get_meta( "is_blocked");
			$owner_id		= $this->get_meta( 'owner_id');
			$count			= $this->get_meta('count');
			$count			= $count>900 ? ">900" : $count;
			$goods_type_id	= $this->get_meta('goods_type_id');
			$best_before	= $this->get_best_before();
			$color			= get_post_meta($goods_type_id, "color", true);
			$color			= $best_before > 0 ? "#".$color  : "url(#rja)";
			$best_before_text = $best_before > MAX_BEST_BEFORE ? __("infinitely", "smp") :  $best_before . " " . __("circles", "smp");			
			$is_owner		= is_user_logged_in() && $Soling_Metagame_Constructor->cur_user_is_owner($owner_id);
			
			if($pos['f'])
			{
				$flip		= "batch_cont_flip";
				$tflip		= "bct_flip";
				$your_		= "<path stroke='none' fill='#999999' fill-opacity='1' d='M51.4 53.85 Q51.4 55.25 50.85 56.35 50.35 57.4 49.5 57.95 L48.5 58.2 47.65 57.5 47.65 59 44.95 60.65 44.95 52.2 47.75 50.5 47.75 55.75 Q47.9 56.1 48.25 55.9 48.75 55.6 48.75 54.45 L48.75 49.9 51.4 48.3 51.4 53.85 M39.55 59.5 Q39 59.85 38.6 59.5 38.25 59.15 38.25 58.2 38.25 57.35 38.65 56.55 39 55.75 39.55 55.4 40.55 54.8 41.1 56.4 L41.1 54.55 43.9 52.85 43.9 61.3 41.1 63 41.1 59.85 Q41.1 58.35 40.7 58.05 40.2 59.1 39.55 59.5 M65.2 39.95 L63.3 47.8 64.4 51.25 61.4 53.1 58.45 44.05 61.15 42.4 61.8 44.3 62.45 41.65 65.2 39.95 M55.55 49.25 L55.1 49.2 Q54.8 49.35 54.65 49.75 L54.45 50.55 54.65 51.1 55.1 51.15 55.55 50.6 55.7 49.85 55.55 49.25 M57.2 45.8 Q58.05 46.5 58.05 48.45 58.05 50.4 57.2 52.1 56.4 53.85 55.1 54.65 L52.95 54.7 Q52.1 54 52.1 52.05 52.1 50.1 52.95 48.4 53.75 46.7 55.1 45.9 L57.2 45.8'/>";
			}
			else
			{
			
				$flip		= "batch_cont";
				$tflip		= "bct";
				$your_		= "<path stroke='none' fill='#999999' fill-opacity='1' d='M55.75 45.65 L55.75 54.1 53.05 55.7 53.05 54.2 52.2 55.9 51.2 56.9 49.85 56.95 Q49.3 56.45 49.3 55.05 L49.3 49.5 51.95 47.95 51.95 52.5 Q51.95 53.65 52.45 53.35 52.8 53.15 52.95 52.6 L52.95 47.35 55.75 45.65 M59.6 48.65 L59.6 51.8 56.8 53.5 56.8 45.05 59.6 43.35 59.6 45.2 Q60.15 43 61.15 42.4 61.7 42.05 62.05 42.4 62.45 42.8 62.45 43.65 62.45 44.6 62.1 45.35 L61.15 46.5 60 46.4 Q59.6 47.15 59.6 48.65 M39.55 55.4 L42.25 53.8 39.3 66.35 36.3 68.15 37.4 63.4 35.5 57.85 38.25 56.15 38.9 58.05 39.55 55.4 M45.6 54.9 L45.15 55.5 Q45 55.85 45 56.3 L45.15 56.85 Q45.35 57.05 45.6 56.85 L46.05 56.3 46.25 55.55 46.05 54.95 45.6 54.9 M47.75 51.55 Q48.6 52.25 48.6 54.2 48.6 56.15 47.75 57.85 46.95 59.6 45.6 60.35 L43.5 60.4 Q42.65 59.7 42.65 57.75 42.65 55.8 43.5 54.1 44.3 52.4 45.6 51.6 L47.75 51.55'/>";
			}
			
			$help		= $this->get_help($is_owner);
			$your		= $is_owner ?
							"<g>
								 <path stroke='none' fill='#00FF00' fill-opacity='0.5' d='M19.9 72.3 L27.25 64.6 27.25 75.75 26 75.75 19.9 72.3'/>
								 <path stroke='none' fill='#00FF00' fill-opacity='1' d='M19.9 72.3 L16.8 70.55 27.25 59.65 27.25 64.6 19.9 72.3'/>".
								$your_ . 
							"</g>"
							:
							"";
			return "
			<div id='help_".$id."' class='lp-hide help_centered' style='height:145px;'>" . $help .  "</div>
			
			<div class='$flip' style='top:".$pos['y']."px; left:".$pos['x']."px; z-index:".$pos['o']."'>
				<div style='position:absolute;'>
					<svg cbid='$id' data-hint='help_".$id."' width='84.25px' height='74.75px' viewBox='0 0 84.25 74.75' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>
					  <defs/>
					  <g>
						<path stroke='none' fill='" . $color . "' d='M85 15 L85.75 41.75 27.5 75.75 26 75.75 1.75 62 1.5 34.75 59.75 1 85 15'/>
					  </g>
					</svg>	
				</div>
				<div class='batch_container' >
					<div class=$tflip >
					".					
						$count . 
					"
					</div>
				</div>
				<div style='position:absolute; pointer-events: none;'>
					<svg width='84.25px' height='74.75px' viewBox='0 0 84.25 74.75' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>
					  $your
					</svg>
				</div>".
				apply_filters("smp_draw_storage_goods_batch_container", "", $this->id) .
			"</div>";
		}
		
		/////////////////////////////////////////////////////////////////
		//
		//	FROM Moveble.php
		//
		/////////////////////////////////////////////////////////////////
		
		
		function user_is_owner()
		{
			global $Soling_Metagame_Constructor;
			return $Soling_Metagame_Constructor->cur_user_is_owner($this->get_owner_id());
			
		}
		function get_best_before()
		{
			global $SolingMetagameProduction;
			if(!$SolingMetagameProduction->get_gb_value("best_before"))	return 10000;	
			return $this->body->post_type == GOODS_BATCH_NAME ? $this->get_meta("best_before"): 100000;
		}
		
		//elemet for front tables
		public function get_stroke(
								$goods_batch,
								$user_is_owner = -1,
								$params = ''
							)
		{
			global $Soling_Metagame_Constructor, $SolingMetagameProduction, $SMP_Assistants;
			if($params =='')
			{
				$params					= array(
													"width"		=> 380,
													"height"	=> 250,
													'is_price'	=> true,
												);
			}
			$batch_id					= $goods_batch->ID;
			$batch						= SMP_Goods_Batch::get_instance($batch_id);
			$op							= get_option(SMP);
			$is_block					= $batch->get_meta("is_blocked");
			if($is_block)				$user_is_owner = -2;
			$goods_type_id 				= $batch->get_meta("goods_type_id" );
			$batch_count				= $batch->get_meta("count" );
			$factory_id					= $batch->get_meta("factory_id");
			$batch_owner_id				= $batch->get_meta("owner_id");
			$batch_dislocation_id		= $batch->get_meta("dislocation_id");
			$store						= $batch->get_meta("store");
			$price						= $batch->get_meta("price");
			$qual						= $batch->get_meta("quality");
			$qual						=  !$qual ? 50 : $qual;
			$price_text					= "";	
			$factory					= Factory::get_factory($factory_id);
			$fac_name					= $factory_id!="" ? $factory->body->post_title : "-";
			$goods_type					= Goods_Type::get_instance($goods_type_id);
			$owner						= SMC_Location::get_instance($batch_owner_id); 
			$best_before				= $this->get_best_before();//
			$bckgr 						= $best_before < 1 ? 'background:url('.SMP_URLPATH.'img/rja.jpg)' : '';
			$bckgr						= $user_is_owner == -2 || $user_is_owner == -3 ? "background:#CCC; " : $bckgr;
			$sale_text					= $store ? '<div class="smp_batch_sale lp_bg_color_hover  hint  hint--left" data-hint="'. __("Sale", "smp").'"><i class="fa fa-briefcase"></i></div>' : "";			
			$disloc						= SMC_Location::get_instance($batch_dislocation_id); 
			$disloc_type				= $Soling_Metagame_Constructor->get_location_type($batch_dislocation_id);
			$disl_text					= '<span class="lp-batch-location_type">'.
												$disloc_type->post_title.
											'</span>
											<span class="lp-batch-data">'.
												$disloc->name.
											'</span>';
			
			if(!isset($price) || $price=="" )
			{
				$price					= $this->set_price($batch_id);
			}
			$pr_text					= $params['is_price'] && $store ? __("Price", "smp"). "<span> " . $price."/" . $price * $batch_count . "</span> " . "<img class='coin' src='" . SMP_URLPATH ."icon/UE.png'/>" :  '';
			$price_text					= apply_filters("smp_batch_card_price", $pr_text, $batch_id);
			$clr						= (get_post_meta($goods_type_id, 'color', true)!= "") ? "#".get_post_meta($goods_type_id, 'color', true) : "#888";
			$owner_type					= $Soling_Metagame_Constructor->get_location_type($batch_owner_id);
			
			
			$btm_pay			= '';
			$btn_settings		= '<i class="fa fa-cogs fa-2x"></i> ';//.__("Settings");
			switch($user_is_owner)
			{
				case 0:
					$exec		= $this->you_are_not_owner($goods_batch, $owner);
					break;
				case 1:
					$exec		= $this->you_are_owner_cont($goods_batch, $owner);
					break;
				case -1:
					$exec		= "";
					break;
				case -2:
					$exec		= "<div class='gb_blocked_label'>".__("Goods batch is blocked", "smp")."</div>";
					break;
				case -3:
					$exec		= "<div class='gb_blocked_label'>".__("Goods batch is deleted", "smp")."</div>";
					break;
				default:
					$exec		= apply_filters("smp_bath_stroke_exec", "", $user_is_owner, $goods_batch, $owner);		
					break;
			}
			$best_before_text	= $SolingMetagameProduction->isBatchOpt('best_before') ? '
			<tr class="lp-batch-table-coll-setting" >
				<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
					<span class="lp-batch-comment">'.
						__("Best before", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("After the deadline will become a waste product", "smp")) .
					'</span>
				</td>
				<td class="lp-batch-table-coll-setting lp-batch-col2">
					<span class="smp-goods-1">'.
							($best_before > MAX_BEST_BEFORE ? __("infinitely", "smp") : $best_before. " " . __("circles", "smp")) . 
						'</span>
				</td>
			</tr>' : '';
			$disl_content		= $SolingMetagameProduction->is_dislocation() ? '
			<tr class="lp-batch-table-coll-setting" >
				<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
					<span class="lp-batch-comment">'.
						__("Dislocation", "smp").' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("The Goods Batch locate in this Location", "smp")) .
					'</span>
				</td>
				<td class="lp-batch-table-coll-setting lp-batch-col2">'.
						" " . $disl_text." ".
					'<span class="lp_bg_color_hover smp_batch_character_link">
						<i class="fa fa-long-arrow-right"></i> 
					</span>
				</td>
			</tr>' : '';
			$form_prod			= $SolingMetagameProduction->isBatchOpt("factory_id") ? '
			<tr class="lp-batch-table-coll-setting" >
				<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
					<span class="lp-batch-comment">'.
						__("Factory", "smp").' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("The Goods Batch made by that Factory", "smp")) .
					'</span>
				</td>
				<td class="lp-batch-table-coll-setting lp-batch-col2">'.
						" " . $fac_name." ".
					'<span class="lp_bg_color_hover smp_batch_character_link">
						<i class="fa fa-long-arrow-right"></i> 
					</span>
				</td>
			</tr>': '<tr><td></td><td></td></tr>';
			$form_quality			= $SolingMetagameProduction->isBatchOpt("quality") ? '
			<tr class="lp-batch-table-coll-setting" >
				<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
					<span class="lp-batch-comment">'.
						__("Quality", "smp").' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("The Goods Batch made by that Factory", "smp")) .
					'</span>
				</td>
				<td class="lp-batch-table-coll-setting lp-batch-col2">'.
						" " . SMP_Assistants::get_quality_diagramm($factory->get_post_meta("quality"), 110, 22)." ".
				'</td>
			</tr>': '';
			$content			= '
<div class="smp-batch-card" style="'.$bckgr.'; width:'. $params['width'] .'px; height:'. $params['height'] .'px;" id="batch_card_'.$batch_id .'">	
	<div style="background:rgba(0,0,0,0.15); height:30px;">
		<svg width="' . $params['width'] . 'px" height="160px" viewBox="20 0 ' . $params['width'] . ' 166.5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
		style="	" class="smp-goods-card-plate">
		  <defs/>
		  <g>
			<path stroke="none" fill="'.$clr.'" d="M114.6 30.5 L0.15 30.5 0.15 0.2 114.5 0.2 129.7 15.4 114.6 30.5"/>
		  </g>
		</svg>
	</div>
	<div class=bg_stroke_title>'.
			$batch_count. " " . __("unit", "smp").
	'</div>
	<span class="gbc_id">' . $batch_id. '</span>
	<div class="gbc_title">'.
			$goods_type->post_title. 	
	'</div>
	<div class="smp-batch-card-table-cont"">
		<table class="lp-batch-table" cellpadding="0" cellspacing="0" align="left" style="margin:1px!importing; display:table;" >'. 			
			$best_before_text.
			apply_filters("smp_batch_form_owner",'
			<tr style="padding:0; margin:0;" valign="top"> 
				<td width="130"  align="right"   class="lp-batch-table-coll-setting lp-batch-col1" >
					<span class="lp-batch-comment">'.
						__("Owner", "smp").' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Owners of this Location  controlled this Goods Batch.", "smp")) .
					'</span>
				</td  align="left">
				<td width="180"  class="lp-batch-table-coll-setting lp-batch-col2">
					<span class="lp-batch-location_type">'.
						" ".$owner_type->post_title.
					'</span>
					<span class="lp-batch-data">'. 
						$owner->name.
					'<span class="lp-batch-data">
					<span class="lp_bg_color_hover smp_batch_character_link">
						<i class="fa fa-long-arrow-right"></i> 
					</span>
				</td  align="left">
			</tr>', 
			$batch_id ).
			apply_filters("smp_batch_form_dislocation", $disl_content, $batch_id ). 
			apply_filters("smp_batch_form_productive", $form_prod, $batch_id). 
			apply_filters("smp_batch_form_quality", $form_quality, $batch_id ). 
			apply_filters("smp_batch_add_characteristics", "", $batch_id ). '
		</table>
	</div>
	<div class="smp_batch_trumb" style="">'.
		get_the_post_thumbnail( $goods_type_id, array(78, 78)).
	'</div>'.
		$sale_text.
	'
	<div name="exec" style="
		position:absolute;
		left:5ps;
		margin-left:5px;
		margin-bottom:0px!important;
		margin-right:20px;
		bottom:5px;
		display:inline-block;
		">'.
			$exec.
	'</div>
	<div name="price" class="smp_batch_card_price" style="color:'.$clr.';">'.
		$price_text.
	'</div>';
		if($this->user_is_owner())
			$content	.= '<div class="smc-your-label" style="">'.__("It's your", "smc").'</div>';
		if($best_before < 1)
			$content	.= '<div class="is_repaid" data-deg="'.(20 + rand(0,10)).'deg">'.__("Is garbage", "smp").'</div>';
	
	$content		.= '</div>'.
	'<!-- END OF CARD -->';
								
			return $content; 
		}		
		
		function you_are_owner_cont($goods_batch, $owner)
		{	
			return "
					<div style='padding:3px 0; width:110px;'>
						<span class='smc-alert lp-link' target_name='exec_batch_".$goods_batch->ID."' func='add_tabs'><i class='fa fa-cogs fa-2x'></i></span> ".
					"</div>". $this->exec_dialog($goods_batch, $owner, "exec_batch_" . $goods_batch->ID );
		}
		
		function exec($div_id, $visible=true)
		{
			return $this->exec_dialog($this->body, 0, $div_id, $visible);
		}
		function exec_dialog($goods_batch, $owner, $div_id, $visible=false)
		{
			global $Soling_Metagame_Constructor;
			global $SolingMetagameProduction;
			$gb				= self::get_instance($goods_batch->ID);
			$gb_id			= $gb->id;
			$count			= $gb->get_meta( "count" );
			$store			= $gb->get_meta( "store" );
			$price			= $gb->get_meta( "price" );
			$best_before	= $gb->get_best_before();
			$store_title	= $store ? __("Remove from Store", "smp") : __("Send to Store", "smp");
			
			$Sale_slide		= "
			<div class='smp-batch-tab-slide'>
				<div class='smp-table-null'>
					<label for='sale_to_location_$gb_id'>".__("Select count of units for sale","smp")."</label><br>
					<input  inbgc  bid='$gb_id' id='sale_to_location_$gb_id' name='sale_to_location_$gb_id' type='number' min='0' max='".$count."' value='".$count."' style='width:140px;'/> ".
					__("unit", "smp").
					"<p style='margin-top:10px;'></p>
					<label for='new_price_$gb_id'>".__("Set price for one unit of this batch in Store","smp")."</label><br>
					<input  price_gb_store  bid='$gb_id' id='new_price_$gb_id' name='new_price_$gb_id' type='number' min='0' value='".$price."' style='width:140px;'/>
					".
				"</div>
				<div class='smp-table-null smc_margin_top' style=''>
					<div class='button smc_padding' gbbt='to_store' bid='$gb_id'>".
						$store_title.
					"</div>					
				</div>
				<hr/>
				
			</div>";
			$transfer_slide	= "
			<div class='smp-batch-tab-slide'>
				<div class='smp-table-null'>
					<label for='transfer_to_location_$gb_id'>".__("Select count of units for transfer","smp")."</label><br>
					<input intrans bid='$gb_id'  id='transfer_to_location_$gb_id' name='transfer_to_location_$gb_id' type='number' min='0' max='".$count."' value='".$count."' style='width:140px;'/> ".
					__("unit", "smp").
				"</div>
				<div class='smp-table-null' style='display:table-cell;'>
					<label for='location_$gb_id'>".__("Choose Location-reciever","smp")."</label><br>
					<label class='smc_input'>
					".$Soling_Metagame_Constructor->get_location_selecter(
																			array(
																					"id"			=> "location_".$gb_id,  
																					"name"			=> "location_".$gb_id, 
																					"style"			=> '',
																					'selector'		=> 'seltrans',
																					'selector_name'	=> '..',
																					'selector2'		=> 'bid',
																					'selector_name2'=> $gb_id
																					
																				  )
																		  )." 
					</label>
				</div>
				<div class='smp-table-null smc_margin_top' style=''>
					<div class='button smc_padding'  gbbt='to_transfer' bid='".$goods_batch->ID."'>
						".__("Send to other Location", "smp")."
					</div>
				</div>
			</div>";
			$delete_slide	= "
			<div class='smp-batch-tab-slide'>
				<div class='smp-table-null'>
					<label for='delete_to_location_$gb_id'>".__("Select count units for deleting","smp")."</label><br>
					<input bid='$gb_id' indel  id='delete_to_location_$gb_id'  name='delete_to_location_$gb_id' type='number' min='0' max='".$count."' value='".$count."' style='width:140px;'/> ".
					__("unit", "smp").
				"</div>
				<div class='smp-table-null smc_margin_top' style=''>
					<div class='button smc_padding'  gbbt='to_delete' bid='$gb_id'>".
						__("Delete for good and all","smp").
					"</div>
				</div>
			</div>";
			$prise_slide="
			<div class='smp-batch-tab-slide'>
				
			</div>
			";
			$print	= "";/*"current_user_can("administrator") 
						? "<div style='display:inline-block; width:45%;'>
							<div class='smp_button2 print_batch_button' batch_id='". $goods_batch->ID ."'>".
								__("Print").
							"</div>
						</div> " : "";*/
			
			
			$dislocat 		= array("title" => __("The Transfer", "smp"), 	"slide" => $transfer_slide);
			if($SolingMetagameProduction->options['gb_settings']['store'])
				$sales 		= array("title" => __("The Sale", "smp"), 		"slide" => $Sale_slide);
			else
				$sales		= '';
			if($SolingMetagameProduction->options['gb_settings']['deleting'])
				$deleting 	= array("title" => __("The Deleting", "smp"), 	"slide" => $delete_slide);
			else
				$deleting	= '';
			$garbage		= $best_before < 1 ? '<div class="is_repaid" data-deg="'.(20 + rand(0,10)).'deg">'.__("Is garbage", "smp").'</div>' : "";
			//$bckgrnd		= $best_before < 1 ? "background-image:url(".SMP_URLPATH."img/rja.jpg); background-size:cover; ": "";
			return "
					<div id='".$div_id."' style='display:".($visible ? "block" : "none")."; " . $bckgrnd. "'>
						<div style='display:inline-block; width:45%;'>							
							<!--div>
								<span class='lp-batch-comment'>".__("Owner", "smp")."</span> <span class='lp-batch-data'>". $owner->name."</span>
							</div-->
						</div>".
						$print.
						"<form name='new_post_$gb_id' method='post'  enctype='multipart/form-data' id='new_post_$gb_id'>".								
								Assistants::get_switcher(
															array(
																	apply_filters("smp_butch_slide_0", "", 			$goods_batch->ID),
																	apply_filters("smp_butch_slide_1", $dislocat, 	$goods_batch->ID),
																	apply_filters("smp_butch_slide_2", $deleting, 	$goods_batch->ID),
																	apply_filters("smp_butch_slide_3", "", 			$goods_batch->ID),
																	apply_filters("smp_butch_slide_4", "", 			$goods_batch->ID),
																	apply_filters("smp_butch_slide_5", "", 			$goods_batch->ID), 
																	apply_filters("smp_butch_slide_6", "", 			$goods_batch->ID), 
																	apply_filters("smp_butch_slide_7", "", 			$goods_batch->ID), 
																), 'exec_batch_'
														).	
						"</form>". $garbage.
					"</div>";
		}
		function you_are_not_owner($goods_batch, $owner)
		{				
			return $this->pay_dialog().
					
					'<div  class="lp-batch-pay smc-alert" target_name="paysment_win_'.$goods_batch->ID.'">
						<svg width="69.75px" height="23px" viewBox="0 0 69.75 23" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin:0;">
							<defs/>
							<g>
								<path stroke="none" fill="#151414" d="M99.9 23.1 L8.25 23.1 0.15 15 0.15 0.1 69.9 0.1 69.9 23.1"/>
							</g>
						</svg>
						<div style="
							position:absolute;
							top:0;
							left:5px;
							width:70px;
							height:23px;">'.
								__("Pay", "smp"). 
						'</div>
					</div>
					';
		}
		function pay_dialog($is_hidden=true)
		{
			global $current_user_accounts;
			$count			= $this->get_meta("count");
			$price			= $this->get_meta("price");
			$ctid			= $this->get_meta("currency_type_id");
			$ct				= SMP_Currency_Type::get_instance($ctid);
			//$rate			= $ct->get_rate();
			$max			= $ct->convert($count * $price, false);
			$pr				= $ct->convert($price, false);
			$opt			= "";
			$current_user_accounts		= SMP_Currency::get_all_user_accounts();
			if($current_user_accounts)
			{
				foreach($current_user_accounts as $acc)
				{
					$accnt	= SMP_Account::get_instance($acc);
					if($accnt->get_meta("ctype_id") != $ctid) continue;
					$ac_cnt	= $accnt->get_count();
					$opt	.= "<option value='".$acc->ID."'>".$acc->post_title." ... <span class='smp-select-comment1'>" . $ct->get_price($ac_cnt) . "</span></option>";
				}
			}
			$hclass	= $is_hidden ? "lp-hide" : ""; 
			return "
					<div id='paysment_win_".$this->id."' class='paymentss $hclass'>	
						<h3>".__("Pay goods batch", "smp"). "</h3> 						
						<div class='simple-margin-top' pid='".$this->id."' price='".$pr."' >". 							
							"<input class='inputnumber' id='payment_count_".$this->id."' name='payment_count_".$this->id."' 	type='number' min='0' max= '" . $count. "' step='1' value='".$count."' style='width:70px;'/>". " " . __("unit", "smp").
							" ". __(" for ", "smp")." ". 
							"<input class='inputnumber' id='price_count_".$this->id."' 	name='price_count_".$this->id."'		type='number' min='0' max= '" . $max. "' step='".$pr."' value='".$max."' style='width:100px;'/>".
							"  ". ( !$ctid ? "<img class='coin' src='" . SMP_URLPATH ."icon/UE.png'/>" :  $ct->get_abbreviation() ) .
							"<p style='margin-top:20px;'>
							<label>".__("Currency Account", "smp")."</label><BR>
							<select name='account_".$this->id."' style='width:100%; padding:5px;'>".
								$opt.
							"</select>
							</p>".
							"<p style='margin-top:20px;'> </p>
							<div name='pay_gb' class='button smc_padding' bid='".$this->id."'>".__("Pay", "smp")."</div>
							<br>".
						"</div> 						
					</div>";
		}
		/*
		$type		0 - standart
					1 - it's your (?)
					2 - by tansit
			
		*/
		function get_widget_picto( $type=0, $forse_block=false, $params=-1)
		{
			global $SolingMetagameProduction, $Soling_Metagame_Constructor;
			if(!is_array($params))
			{
				$params		= array("your_bottom"=>0, "your_right"=>0, "size"=>40);
			}
			$gb				= $this->id;
			$is_block		= get_post_meta( $gb, "is_blocked", true );
			$best_before	= get_post_meta( $gb, "best_before", true );
			$best_before_text = $best_before > MAX_BEST_BEFORE ?__("infinitely", "smp") :  $best_before . " " . __("circles", "smp");
			$is_trash		= $best_before<1 && $SolingMetagameProduction->is_best_before();
			if($is_block && !$forse_block) return "";
			
			$dis_id			= get_post_meta($gb, 'dislocation_id', true);
			$owner_id		= get_post_meta($gb, 'owner_id', true);
			$is_owner		= is_user_logged_in() && $Soling_Metagame_Constructor->cur_user_is_owner($owner_id);
			$itsyour		= $is_owner ? 
				$Soling_Metagame_Constructor->assistants->get_short_your_label(22, array($params['your_right'], $params['your_bottom'])) : 
				"";
			$gbt			= get_post_meta($gb, 'goods_type_id', true);
			$gt				= get_post($gbt);
			$count			= get_post_meta($gb, 'count', true);
			$color			= $is_trash ? "url(".SMP_URLPATH."img/rja.jpg)" : "#" . get_post_meta($gbt, 'color', true);
			$help			= $this->get_help($is_owner);
			switch($type)
			{
				case 2:
					$opacity				= .75;
					break;
				case 0:
				case 1:
				default:
					$opacity				= 1;
					break;
			}
			
			$your		= $is_owner ? "<div style='position:absolute; display:block; top:0px; left:0px; border:4px solid red; padding:4px 4px; text-align:center; width:30px; height:30px;' ></div>" : "";
			$img		= !$is_trash ? Goods_Type::get_trumbnail( $gbt,  $params['size'], array("is_hint"=>false)) : "<div class='factory_widg_goods_type goods_type_trumb'><img class='attachment-40x40 wp-post-image' width='40' height='40' src='".SMP_URLPATH."img/rja_120_120.jpg'></div>"; 
			$trashbord	= !$is_trash ? " border:1px solid transparent" : ' border:1px solid red';
			$help		= !$is_trash ? $help : $help.'<div class="is_repaid" data-deg="'.(20 + rand(0,10)).'deg">'.__("Is garbage", "smp").'</div>';
			$html 		.= "
			<div style='position:relative; display:inline-block;'>
			<div st_gb_id='".$gb."' class='fix_box_size factory_widg_goods_type div_hinter lp-link ' data-hint='help_".$gb."' target_name='exec_batch_gb_".$gb."' func='add_tabs'  style='opacity:" . $opacity . "; $trashbord; position:relative; display:block; height:44px; margin:0; margin-bottom:3px;'>".$img;
			if($count > 900)	
			{
				$st = "letter-spacing:0;";
				$count = ">900";
			}
			else
				$st = '';
			$html		.= "<DIV class='st_gb_count' style='background:".$color."; $st'>" . $count .  "</div>$itsyour</div></div>";
			$html		.= "<div id='help_".$gb."' class='lp-hide help_centered' style='height:145px;'>" . $help .  "</div>";
			
			$html		.= $this->exec_dialog($this->body, $owner, "exec_batch_gb_".$gb);
			return $html;
		}
		
		
		function get_table_row($add_row=-1, $num=0)
		{
			global $Soling_Metagame_Constructor, $SolingMetagameProduction, $SMP_Assistants;
			if(!is_array($add_row))	$add_row = array();
			$id				= $this->id;
			$is_block		= $this->get_meta( "is_blocked");
			$dis_id			= $this->get_meta( 'dislocation_id');
			$owner_id		= $this->get_meta( 'owner_id');
			$quality		= $this->get_meta( 'quality');
			$factory_id		= $this->get_meta("factory_id");
			$is_block		= $this->get_meta("is_blocked" );
			$blocked		= $is_block ? " class='is_batchblock' ' " : "";
			$location		= SMC_Location::get_instance($dis_id);
			$owner			= SMC_Location::get_instance($owner_id);
			$owner_type		= $Soling_Metagame_Constructor->get_location_type($owner_id);
			$is_owner		= is_user_logged_in() && $Soling_Metagame_Constructor->cur_user_is_owner($owner_id);
			$count			= $this->get_meta('count');
			$factory		= Factory::get_factory($factory_id);
			$fac_name		= $factory_id!="" ? $factory->body->post_title : '<i class="fa fa-times" style="opacity:0.5;" aria-hidden="true"></i>';
			
			$goods_type_id	= $this->get_meta('goods_type_id');			
			$best_before	= $this->get_meta("best_before");
			$is_trash		= $best_before<0 && $SolingMetagameProduction->is_best_before();
			$color			= get_post_meta($goods_type_id, "color", true);
			$gt				= Goods_Type::get_instance($goods_type_id);
			$best_before_text = $best_before > MAX_BEST_BEFORE ? __("infinitely", "smp") :  $best_before . " " . __("circles", "smp");
			$your			= $is_owner ? '<div style="height:20px;"></div><div class=smc-your-label style="bottom:0; right:-10px;">'.__("It's your", "smc").'</div>' : "";
			$bg				= $is_owner	? "ob" : "ab";
			$pic			= "<div style='position:relative; display:inline-block; '><div class='button smc_padding execgbtch' style='padding:2px!important;' gbid='$id'>".SMP_Goods_Batch::get_picto($this->id, 1, array("size"=>65, "is_menu"=>false)). "</div>". ($is_block ? "<div class=is_batchblock_g>".__("Goods batch is blocked", "smp")."</div>" : "")."</div>";	
			$disloc			= SMC_Location::get_instance($dis_id); 
			$disloc_type	= $Soling_Metagame_Constructor->get_location_type($dis_id);
			$disl_text		= '<span class="lp-batch-location_type">'.
									$disloc_type->post_title.
								'</span>
								<span>'.
									$disloc->name.
								'</span>';
			$add_titles		= "";
			$add_contents	= "";
			foreach($add_row as $row)
			{
				$add_titles		.= "<td class='descr' $bg>".$row['title']."</td>";
				$add_contents	.= "<td $bg>".$row['content']."</td>";
			}
			if($SolingMetagameProduction->is_dislocation() )
			{
				$disl_content		= "
				<td width='180' class='contt $bg'>".
					$disl_text.
				'</td>';
				$disl_th	= "
				<td class='descr $bg' >".
					__("Dislocation", "smp").
				"</td>";
			}
			if( $SolingMetagameProduction->isBatchOpt("factory_id"))
			{
				$form_prod			= "
				<td class='contt  $bg'>".
					$fac_name.
				"</td>";
				$prod_th	= "
				<td class='descr $bg'>".
					__("Factory", "smp").
				"</td>";
			}
			if($SolingMetagameProduction->isBatchOpt("quality"))
			{
				$form_quality			= "
				<td class='contt $bg'>".
					SMP_Assistants::get_quality_diagramm((int)$factory->get_post_meta("quality"), 60, 30)." ".
				"</td>";				
				$qual_th	= "
				<td class='descr $bg'>".
					__("Quality", "smp").
				"</td>";
			}
			if($SolingMetagameProduction->isBatchOpt('best_before'))
			{			
				$best_before_text	=  "
				<td class='contt $bg'>".
					($best_before > MAX_BEST_BEFORE ? __("infinitely", "smp") : (int)$best_before. " " . __("circles", "smp")) . 
				"</td>";				
				$bb_th	= "
				<td class='descr $bg'>".
					__("Best before", "smp").
				"</td>";
			}
			else
			{
				$best_before_text	=  '';
				$bb_th				= "";
			}
			if($SolingMetagameProduction->is_finance())
			{
				$class		= $this->get_meta('store') ? "lp-external-background" : "";
				$ct			= SMP_Currency_Type::get_instance( $this->get_meta( "currency_type_id" ) );
				$price		= SMP_Currency_Type::get_object_price($ct->id, $this->get_meta("price"));
				$price_text	= "
				<td class='contt $bg'>".
					($this->get_meta('store') || $is_owner 	? "<div class='button smc_padding $class pay_gbbt' gbid='$id'>".$price."</div>" : "" ). 
				"</td>";
				$price_th	= "
				<td class='descr $bg'>".
					($is_owner	? __("Price per unit", "smp") : __("Buy", "smp")).
				"</td>
				";
				if(!is_user_logged_in())
				{
					$price_text	= $price_th	= "";
				}
			}
			$html			= "
			<tr  $blocked>
				<td rowspan='2' width='70' class=' $bg'>".
					$pic.
				"</td>
				<td class='contt descr $bg'>".
					__("Owner", "smp").
				"</td>".
				$disl_th . $prod_th . $qual_th . $price_th . $bb_th . $add_titles . 
			"</tr>
			<tr $blocked>".
				apply_filters("smp_batch_form_owner","
					<td width='180' class='contt $bg'>
						<span class='lp-batch-location_type'>".
							$owner_type->post_title.
						'</span>
						<span>'. 
							$owner->name.
						'</span>
					</td>', 
				$id ).
				apply_filters("smp_batch_form_dislocation", $disl_content, $id ). 
				apply_filters("smp_batch_form_productive", $form_prod, $id). 
				apply_filters("smp_batch_form_quality", $form_quality, $id ). 
				apply_filters("smp_batch_form_price", $price_text, $id ). 
				apply_filters("smp_batch_form_best_before", $best_before_text, $id ). 
				apply_filters("smp_batch_add_characteristics", "", $id ). 
				$add_contents.
				//$button.
			"</tr>
			<tr>
				<td colspan='19' class='tbhr  $bg'></td>
			</tr>";		
			
			return $html;
		}
		
		static function get_picto($id, $forse_block=false, $params=-1)
		{
			global $Soling_Metagame_Constructor;
			if(!is_array($params))
			{
				$params		= array("size"=>50, "left_text"=>"litle", "is_menu"=>true);
			}
			$is_menu		= $params["is_menu"];
			if($is_menu)
			{
				$hint_		= 'hint="'.__("Convert goods batch to real Card?", "smp").'" conv_id="'. $id . '" ';
			}
			$b				= static::get_instance($id);
			$is_block		= $b->get_meta("is_blocked" );
			if($is_block && !$forse_block) return "";
			$count			= $b->get_meta( "count" );
			$price			= $b->get_meta( "price" );
			$goods_type_id 	= $b->get_meta( "goods_type_id" );
			$color			= get_post_meta( $goods_type_id, "color", true );
			$owner_id		= $b->get_meta( "owner_id" );
			$owner			= SMC_Location::get_instance( $owner_id );
			$is_owner		= $params['is_owner'] ? '<div class="gb_picto_owner">'. $owner->name. '</div>' : "";
			$your			= is_user_logged_in() && $Soling_Metagame_Constructor->cur_user_is_owner($owner_id);
			$finish_time	= $b->get_meta("finish_time");
			if($finish_time)
			{
				$left		= $params['little'] ? (int)(($finish_time - time())/60) : (int)(($finish_time - time())/6) / 10 ;
				if($left<0)	
					$b->update_meta("finish_time","");
				else
					$left_text	= "<div class='left_batch'><i class='fa fa-truck'></i> ". $left. "</div>";
			}
			if($your)
			{
				$your_label	= Assistants::get_short_your_label(25, array($is_owner ? 56 : 3, 0));
			}
			return "<div class='gbpiccont' style='width:".$params['size']."px; '>".'
				<div batch_picto_id="' . $id . '" class="" ' . $hint_ . '>' .
					get_the_post_thumbnail( $goods_type_id, array($params['size'], $params['size']), array('class' => 'smp_good_type_picto')).
					'<div style="background-color:#'.$color.'; padding:2px 7px; font-size:15px; display:inline-block; position:absolute; top:0; left:0;color:#FFF;">'.
						$count.
					'</div>'.
					$left_text. 
					'<div class="gbid_label">'.$id.'</div>
				</div>'.
				$is_owner.
				$your_label.
			'</div>';
		}
		function set_price($id)
		{
			return;
		}
	}
?>