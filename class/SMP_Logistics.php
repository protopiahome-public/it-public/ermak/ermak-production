<?php 
	
	class SMP_Locistics
	{
		public $options;
		function __construct()
		{
			global $moved_classes, $moveble_types;
			$this->options											= get_option(SMP);
			add_action( 'admin_menu',								array($this, 'admin_page_handler'), 2);			
			if(!$this->options['logistics_enabled'])				return;
			
			require_once(SMP_REAL_PATH.'class/SMP_Routh.php');
			require_once(SMP_REAL_PATH.'class/SMP_Hub.php'); 
			require_once(SMP_REAL_PATH.'class/SMP_Routh_Type.class.php'); 
			
			if($this->options['waybill'])
			{
				global $SMP_Waybill;
				require_once(SMP_REAL_PATH.'class/SMP_Waybill.class.php'); 
				SMP_Waybill::init();
			}
			add_action( 'admin_head', 								array($this,'add_column_css'));
			add_action( 'init', 									array($this, 'add_hub_type'), 11 );
			add_action( 'init', 									array($this, 'add_permission_type'), 12 );
			add_action( 'save_post_smp_hub',						array($this, 'true_save_box_data'));
			add_action( 'save_post_smp_hub_permission',				array($this, 'true_save_box_smp_hub_permission'));
			add_action( 'init', 									array(SMP_Routh_Type, 'add_smp_route_type'), 12 );\
			add_action( 'save_post_smp_route_type',					array(SMP_Routh_Type, 'true_save_box_smp_route_type'));
			add_action( 'admin_menu',								array(SMP_Routh_Type, 'my_extra_fields_smp_route_type'));			
			add_filter( 'manage_edit-smp_route_type_columns',		array(SMP_Routh_Type, 'add_views_column_smp_route_type'), 4);
			add_filter( 'manage_edit-smp_route_type_sortable_columns', array(SMP_Routh_Type, 'add_views_sortable_column_smp_route_type'));
			add_filter( 'manage_smp_route_type_posts_custom_column',array(SMP_Routh_Type, 'fill_views_column_smp_route_type'), 5, 2); 
			
			add_action( 'init', 									array($this, 'add_routh_type'), 12 );
			add_action( 'admin_menu',								array($this, 'my_extra_fields_hub_type'));
			add_action( 'admin_menu',								array($this, 'my_extra_fields_hub_permission_type'));
			add_filter( 'manage_edit-smp_hub_columns',				array($this, 'add_views_column'), 4);
			add_filter( 'manage_edit-smp_hub_sortable_columns', 	array($this, 'add_views_sortable_column'));
			add_filter( 'manage_smp_hub_posts_custom_column', 		array($this, 'fill_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
			add_filter( 'manage_edit-smp_hub_permission_columns',	array($this, 'add_views_column_smp_hub_permission'), 4);
			add_filter( 'manage_edit-smp_hub_permission_sortable_columns', 	array($this, 'add_views_sortable_column_smp_hub_permission'));
			add_filter( 'manage_smp_hub_permission_posts_custom_column', 	array($this, 'fill_views_column_smp_hub_permission'), 5, 2); 		
			add_action( 'parent_file', 								array($this, 'recipe_tax_menu_correction'),1); //http://wordpress.org/support/topic/moving-taxonomy-ui-to-another-main-menu
			
			add_action( "delete_smp_routh",							array($this, 'delete_smp_routh_handler'));
			add_action( 'smp_routh_add_form_fields', 				array($this, 'add_smp_routh'), 10, 2 );
			add_action( 'smp_routh_edit_form_fields', 				array($this, 'edit_smp_routh'), 10, 2 );
			add_action( 'edit_smp_routh', 							array($this, 'save_smp_routh_custom_meta'), 10);  
			add_action( 'create_smp_routh', 						array($this, 'save_smp_routh_custom_meta'), 10);
			//manage column for Location screen
			add_filter("manage_edit-smp_routh_columns", 			array($this, 'smp_routh_columns')); 
			add_filter("manage_smp_routh_custom_column", 			array($this, 'manage_smp_routh_columns'), 10, 3);
			
			add_filter("smc_map_external_0",						array($this, "smc_map_external_0"), 11, 2);
			add_filter("smc_login_widget_list",						array($this, "smc_login_widget_list1"), 11);
			
			
			//add_action('smp_batch_extra_fields2',					array($this, 'smp_batch_extra_fields'), 13, 2);
			
			add_action('admin_menu',								array($this, 	'my_extra_fields_movebled'),13);
			if(is_array($moveble_types))
			{
				foreach($moveble_types as $moved_type)
				{
					add_action("save_post_{$moved_type}",						array($this, 'save_logistics_parameters'),13);
					add_filter("manage_edit-{$moved_type}_columns",		 		array($this, 'add_views_column_batch'), 5);
					add_filter("manage_edit-{$moved_type}_sortable_columns", 	array($this, 'add_views_sortable_column_batch'), 11);
					add_filter("manage_{$moved_type}_posts_custom_column", 		array($this, 'fill_views_column_batch'), 7, 2); 
				}
			}
			
			add_filter('smp_butch_slide_3',							array($this, 'smp_butch_slide_3'), 11, 2);
			add_filter('smp_bath_stroke_exec', 						array($this, 'smp_bath_stroke_exec') , 11, 4);
			add_filter('smp_batch_card_price', 						array($this, 'smp_batch_card_price'));
			add_filter('smp_send_batch_changes', 					array($this, 'smp_send_batch_changes'));
			add_filter('smp_batch_form_dislocation', 				array($this, 'smp_batch_form_dislocation'),2, 2);
			add_filter('smp_goods_batch_hint', 						array($this, 'smp_goods_batch_hint'),20, 2);
			add_filter('smp_draw_storage_goods_batch_container', 	array($this, 'smp_draw_storage_goods_batch_container'),20, 2);
			
			add_action('smc_myajax_submit', 						array($this, 'smc_myajax_submit'));
			add_filter('smc_location_archive_title', 				array($this, 'smc_location_archive_title'), 11, 2);
			add_filter('smc_edit_location_main_map', 				array($this, 'smc_edit_location_main_map'), 11, 3);
			//	Metagame menu in frontend		
			add_action( 'wp_before_admin_bar_render',				array($this,'my_admin_bar_render' ), 13);
			
			add_filter("open_location_content_widget",				array($this, "open_location_content_widget"), 13, 2);
			//cron
			add_filter( 'cron_schedules', 							array($this, 'cron_add_logistics_interval'), 11);
			add_action( 'logistics_hook', 							array($this, 'logistics_handler' ));
			if ( ! wp_next_scheduled( 'logistics_hook' ) ) 
			{
				wp_schedule_event( time(), 'logistics_interval', 'logistics_hook' );
			}
			
			add_filter("smp_my_tools",								array($this, 'smp_my_tools'),11);
			
			
		}
		function my_admin_bar_render() 
		{
			if(!current_user_can('administrator'))	return;
			global $wp_admin_bar;
			$wp_admin_bar->add_menu( array(
				'parent' => 'metagame', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'logistics_panel', // ID ссылки
				'title' => __('Ermak. Logistics', "smp"), //заголовок ссылки
				'href' => "/wp-admin/admin.php?page=Metagame_Logistics_page" //имя файла	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'logistics_panel', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'smp_route_type_panel', // ID ссылки
				'title' => __('all Route Types', 'smp'), //заголовок ссылки
				'href' => "/wp-admin/edit.php?post_type=smp_route_type" //имя файла	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'logistics_panel', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'smp_routh_panel', // ID ссылки
				'title' => __('All Rouths', 'smp'), //заголовок ссылки
				'href' => "/wp-admin/edit-tags.php?taxonomy=smp_routh" //имя файла	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'logistics_panel', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'smp_hub_panel', // ID ссылки
				'title' => __('all Hubs', 'smp'), //заголовок ссылки
				'href' => "/wp-admin/edit.php?post_type=smp_hub" //имя файла	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'logistics_panel', //'false' для корневого меню
							   //или ID нужного меню
				'id' => 'smp_hub_permission_panel', // ID ссылки
				'title' => __('all Hub permissions', 'smp'), //заголовок ссылки
				'href' => "/wp-admin/edit.php?post_type=smp_hub_permission" //имя файла	
			));
		}		
		function cron_add_logistics_interval( $schedules ) 
		{
			$schedules['logistics_interval'] = array(
				'interval'		=> $this->options['logistics_interval'] * 60,
				'display'		=> __( 'Logistics interval', 'smp' ),
			);
			return $schedules;
		}
		
		function admin_page_handler()
		{
			add_menu_page( 
						__('Ermak. Logistics', "smp"), // title
						__('Ermak. Logistics', "smp"), // name in menu
						'manage_options', // capabilities
						'Metagame_Logistics_page', // slug
						array($this, 'menu_logistics_page'), // options function name
						'none'	,//'dashicons-admin-site',//SMC_URLPATH .'/img/pin.png', // icon url
						'20.511'
						);
			add_submenu_page( 
							  'Metagame_Logistics_page'  //or 'options.php' 
							, __('All Rouths', 'smp')							
							, __('All Rouths', 'smp')
							, 'manage_options'
							, 'edit-tags.php?taxonomy=smp_routh'
						);
		}
		
		// highlight the proper top level menu
		function recipe_tax_menu_correction($parent_file) 
		{
			global $current_screen;
			$taxonomy = $current_screen->taxonomy;
			if ($taxonomy == 'smp_routh')
				$parent_file = 'Metagame_Logistics_page';
			return $parent_file;
		}
		
		function menu_logistics_page()
		{ 
			if($_POST['all_submit']) 
			{
				$this->option['gb_settings']['transportation_id'] = 
				$this->option['gb_settings']['is_permission'] = 
				$this->options['logistics_enabled']		= $_POST['enable_logistics']=="on" ? 1 : 0;	
				
				update_option(SMP, $this->options);
				
			}
			if($_POST['params_submit'])
			{
				$this->options['route_hint_radius']		= $_POST['route_hint_radius'];
				$this->options['logistics_interval']	= $_POST['logistics_interval'];
				$this->options['is_hubs_uncontrolled']	= $_POST['is_hubs_uncontrolled'];
				$this->options['waybill']				= $_POST['waybill']=='on'? 1 : 0;	
				update_option(SMP, $this->options);
			}
			if($_POST['exec'])
			{
				$this->logistics_handler();
				//echo "a";
			}
			?>
				<h2><?php _e("Metagame Logistics", "smp")?></h2>
				<form name="settings"  method="post"  enctype="multipart/form-data" id="settings">
					<div class="h">
						<input name='enable_logistics' id='enable_logistics' type="checkbox" class="css-checkbox" <?php checked(1, $this->options['logistics_enabled']);?> /> 
						<label for="enable_logistics" class="css-label"><?php _e("Enable Logistics", "smp"); ?></label><br>
						<div style="margin-top:14px;">
							<input type="submit" name="all_submit" value="<?php _e("Edit");?>"/>
						</div>
					</div>
					
					<div class="h">
						<board_title><?php _e("Parameters", "smc"); ?></board_title>
						<div>
							<label for="logistics_interval"><?php _e("Interval (per minutes) for recount all Logistics parameters(rouths ets.)", "smp"); ?></label>
							<br>
							<input type="number" min="0.2" step="0.2" name="logistics_interval" id="logistics_interval" value="<?php echo $this->options['logistics_interval'];?>"/>						
						</div>
						<hr/>
						<div style="margin:20px 0;">
							<board_title><?php _e("Hub permission mode", "smp");?></board_title>
							<input type="radio" class="css-checkbox" name="is_hubs_uncontrolled" id="is_hubs_uncontrolled2" value='2' <?php checked(2, $this->options['is_hubs_uncontrolled']);?>/>
							<label for="is_hubs_uncontrolled2" class="css-label"><?php _e("Ignore all", "smp");?></label><BR>
							<input type="radio" class="css-checkbox" name="is_hubs_uncontrolled" id="is_hubs_uncontrolled0" value='0' <?php checked(0, $this->options['is_hubs_uncontrolled']);?>/>
							<label for="is_hubs_uncontrolled0" class="css-label"><?php _e("The Hub's owner gives permission", "smp");?></label><BR>
							<input type="radio" class="css-checkbox" name="is_hubs_uncontrolled" id="is_hubs_uncontrolled1" value='1' <?php checked(1, $this->options['is_hubs_uncontrolled']);?>/>
							<label for="is_hubs_uncontrolled1" class="css-label"><?php _e("The permit is issued automatically based on the parameters of a hub", "smp");?></label><BR><BR>
							<hr/>
							<board_title><?php _e("Route hint size", "smp");?></board_title>
							<input name='route_hint_radius' id='route_hint_radius' type="text" style='width:80px;' value='<?php echo $this->options['route_hint_radius'];?>' /> 
							<label for="route_hint_radius">px</label><br>
						</div>
						<hr/>
						<div style="margin:20px 0;">
							<board_title><?php _e("WayBill", "smp");?></board_title>
							<input name='waybill' id='waybill' type="checkbox" class="css-checkbox" <?php checked(1, $this->options['waybill']);?> /> 
							<label for="waybill" class="css-label"><?php _e("Enable WayBill", "smp"); ?></label><br>
						</div>
						<hr/>
						<div>
							<input type="submit" name="params_submit" value="<?php _e("Change");?>"/>
						</div>
					</div>
					<div class="h">
						<board_title><?php _e('Times update', "smp");?></board_title>
						<?php 
							$rrr = wp_get_schedules();
							foreach($rrr as $rr)
								echo "<div>". $rr['display'] . " - " . $rr['interval']. " secs</div>";
							
						?>
						<input type="submit" name="exec" value="<?php _e("Execute logistics step", "smp"); ?>"/>
					</div>					
				</form>
			<?php
		}
		//HUB
		function add_hub_type()
		{
			global $smc_main_tor_buttons;
			if(!isset($smc_main_tor_buttons))						$smc_main_tor_buttons = array();
			$smc_main_tor_buttons[]									= array('ico'=>"", 'targ'=>__("all Hubs", "smp"), 'comm' =>'get_location_hubs');
		
			$labels 	= array
			(
				'name' => __('Hub', "smp"),
				'singular_name' => __("Hub", "smp"), // админ панель Добавить->Функцию
				'add_new' => __("add Hub", "smp"),
				'add_new_item' => __("add Hub", "smp"), // заголовок тега <title>
				'edit_item' => __("edit Hub", "smp"),
				'new_item' => __("add Hub", "smp"),
				'all_items' => __("all Hubs", "smp"),
				'view_item' => __("view Hub", "smp"),
				'search_items' => __("search Hub", "smp"),
				'not_found' =>  __("Hub not found", "smp"),
				'not_found_in_trash' => __("no found Hub in trash", "smp"),
				'menu_name' => __("Hub", "smp") // ссылка в меню в админке
			);
			$args 		= array
			(
				'labels' => $labels,
				'public' => true,
				'show_ui' => true, // показывать интерфейс в админке
				'has_archive' => true, 
				'exclude_from_search' => true,
				'menu_position' => 21, // порядок в меню
				'show_in_menu' => "Metagame_Logistics_page",
				'supports' => array(  'title', 'thumbnail')
				//,'capabilities' => 'manage_options'
				,'capability_type' => 'post'
			);
			register_post_type('smp_hub', $args);
			
			
		}
		// подправим ширину колонки через css
		function add_column_css(){
			echo '<style type="text/css"> 
					.column-fill{width:20px;}
					.column-cost{width:110px;}
					.column-speed{width:40px;}
					.toplevel_page_Metagame_Logistics_page, #toplevel_page_Metagame_Logistics_page.wp-has-current-submenu div.wp-menu-image
					{				
						background-image:url(' . SMP_URLPATH . 'img/ermak_logistics.png);
						background-repeat: no-repeat;
						background-position: 7px center;
					}
				</style>';
		}
		// мета-поля в редактор		
		function my_extra_fields_hub_type() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(&$this, 'extra_fields_box_func'), 'smp_hub', 'normal', 'high'  );
		}
		
		function extra_fields_box_func( $post )
		{		
			global $Soling_Metagame_Constructor;
			?>
			<div style='display:inline-block;'>
				
				<div style='float:left; position:relative; display:inline-block;'> 
					<div class='h'>	
 					
						<label  class="h2" for="capacity"><?php echo __("Capacity", "smp")." (" .__("units", "smp") . ")"; ?></label><br>
						<input  class="h2" name="capacity" type="number" min="0" step="1" value="<?php print_r( (int)get_post_meta( $post->ID, "capacity", true ));?>"/>
					</div>
					<div class='h'>	
						<label  class="h2" for="cost"><?php _e("Cost", "smp"); ?></label><br>
			 			<input  class="h2" name="cost" type="number" min="0" step="1" value="<?php print_r( (int)get_post_meta( $post->ID, "cost", true ));?>"/>

					</div> 
				</div>
				<div style='float:left; position:relative; display:inline-block;'>
					<div class="h">
						<label  class="h2" for="dislocation_id"><?php _e("Dislocation", "smp"); ?></label><br>
						<?php echo $Soling_Metagame_Constructor->get_location_selecter(array("name" => 'dislocation_id', 'selected' => get_post_meta($post->ID, 'dislocation_id', true))); ?>
					</div>					
					<div class="h">
						<label  class="h2" for="owner_id"><?php _e("Owner", "smp"); ?></label><br>
						<?php echo $Soling_Metagame_Constructor->get_location_selecter(array("name" => 'owner_id', 'selected' => get_post_meta($post->ID, 'owner_id', true))); ?>
					</div>					
				</div>
				<div style='float:left; position:relative; display:inline-block;'>
					<?php 
					do_action("smp_admin_smp_hub_box_func", $post->ID);
					?>				
				</div>
			</div>
			<script type="text/javascript">
				jQuery(function($)
				{					
					$(".rangerr").ionRangeSlider({
							min: -100,
							max: 100, 			
							type: 'single',
							step: 5,
							postfix: "%",
							prettify: true,
							hasGrid: true
					});
				});
			</script>
			<?php 
			wp_nonce_field( basename( __FILE__ ), 'smp_hub_metabox_nonce' );
		}
		
		function true_save_box_data ( $post_id) 
		{				
			global $wpdb, $table_prefix;
			// проверяем, пришёл ли запрос со страницы с метабоксом
			if ( !isset( $_POST['smp_hub_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['smp_hub_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) ) 	return $post_id;
			
			update_post_meta($post_id, 'capacity', 				$_POST['capacity']);			
			update_post_meta($post_id, 'cost', 					$_POST['cost']);				
			update_post_meta($post_id, 'dislocation_id', 		$_POST['dislocation_id']);				
			update_post_meta($post_id, 'owner_id', 				$_POST['owner_id']);
			
			do_action("smp_admin_smp_hub_save_box_data", $post_id, $_POST);			
			//
			if($_POST['post_title']=="")
			{
				$dd			= SMC_Location::get_instance($_POST['dislocation_id']);	
				$wpdb->update(
								$table_prefix."posts",
								array( "post_title" =>  $dd->name ),
								array( "ID" => $post_id )
							);
			}	
			
			return $post_id;
		}
		
		
		function add_views_column( $columns ){
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
				  "IDs"	 			=> __("ID", 'smp'),
				  "title"	 		=> __("Title"),
				  "dislocation_id"	=> __("Dislocation", 'smp'),
				  "owner_id"		=> __("Owner", 'smp'),
				  "capacity" 		=> __("Capacity", 'smp'),		
				  "cost" 			=> __("Cost", 'smp'),			
			   );
			return $posts_columns;			
		}
		// добавляем возможность сортировать колонку
		function add_views_sortable_column($sortable_columns){
			$sortable_columns['IDs'] 					= 'IDs';						
			$sortable_columns['dislocation_id'] 		= 'dislocation_id';						
			$sortable_columns['owner_id'] 				= 'owner_id';						
			$sortable_columns['capacity'] 				= 'capacity';				
			$sortable_columns['cost'] 					= 'cost';						
			return $sortable_columns;
		}	
		// заполняем колонку данными	
		function fill_views_column($column_name, $post_id)
		{
			$post			= get_post($post_id);
			switch( $column_name) 
			{		
				case 'IDs':
					echo "<br><div class='ids'><span>ID</span>".$post_id."</div>";
					break;		
				case 'dislocation_id':
					$term_id	=  get_post_meta($post_id, 'dislocation_id', true);
					echo get_term_by("id", $term_id, 'location')->name."<br><div class='ids'><span>ID</span>".$term_id."</div>";
					break;			
				case 'owner_id':
					$term_id	=  get_post_meta($post_id, 'owner_id', true);
					echo get_term_by("id", $term_id, 'location')->name."<br><div class='ids'><span>ID</span>".$term_id."</div>";
					break;			
				case 'capacity':
					echo get_post_meta($post_id, 'capacity', true);
					break;
				case 'cost':
					echo get_post_meta($post_id, 'cost', true); 
					break;	
			}		
		}
		
		
		//PERMISSION
		function add_permission_type()
		{
			$labels 	= array
			(
				'name' => __('Hub permission', "smp"),
				'singular_name' => __("Hub permission", "smp"), // админ панель Добавить->Функцию
				'add_new' => __("add Hub permission", "smp"),
				'add_new_item' => __("add Hub permission", "smp"), // заголовок тега <title>
				'edit_item' => __("edit Hub permission", "smp"),
				'new_item' => __("add Hub permission", "smp"),
				'all_items' => __("all Hub permissions", "smp"),
				'view_item' => __("view Hub permission", "smp"),
				'search_items' => __("search Hub permission", "smp"),
				'not_found' =>  __("Hub permission not found", "smp"),
				'not_found_in_trash' => __("no found Hub permission in trash", "smp"),
				'menu_name' => __("Hub permission", "smp") // ссылка в меню в админке
			);
			$args 		= array
			(
				'labels' => $labels,
				'public' => true,
				'show_ui' => true, // показывать интерфейс в админке
				'has_archive' => true, 
				'exclude_from_search' => true,
				'menu_position' => 21, // порядок в меню
				'show_in_menu' => "Metagame_Logistics_page",
				'supports' => array( "")
				//,'capabilities' => 'manage_options'
				,'capability_type' => 'post'
			);
			register_post_type('smp_hub_permission', $args);
		}
		// мета-поля в редактор		
		function my_extra_fields_hub_permission_type() 
		{
			add_meta_box( 'extra_fields', __('Parameters of Hub Permission for permanent transit', "smp"), array(&$this, 'extra_fields_box_func_smp_hub_permission'), 'smp_hub_permission', 'normal', 'high'  );
		}
		
		function extra_fields_box_func_smp_hub_permission( $post )
		{		
			global $Soling_Metagame_Constructor;
			$arg				= array(										
										'numberposts'	=> 0,
										'offset'    	=> 0,
										'orderby'  		=> 'titile',
										'order'     	=> 'ASC',
										'post_type' 	=> 'factory',
										'post_status' 	=> 'publish'
									);
			$factory		= get_posts($arg);
			$ar				= array(										
										'numberposts'	=> 0,
										'offset'    	=> 0,
										'orderby'  		=> 'titile',
										'order'     	=> 'ASC',
										'post_type' 	=> 'goods_type',
										'post_status' 	=> 'publish'
									);
			$goods			= get_posts($ar);
			
			$owners			= get_terms('location', array("number"=>0, 'orderby'=>'name', "hide_empty"=>false));
			/*
				factory_id
				dislocation_id
				owner_id
				goods_type_id
				count
				store
			*/
			$batch_meta 	= get_post_meta($post->ID, "batch_meta", true);
			$limit			= get_post_meta($post->ID,'limit', true);
			?>
			<div style='display:inline-block;'>
				<div class="smp_batch_extra_field_column">
					<div class="h">
						<label for="hub_id"><?php echo __("Hub that  gives a permission", "smp")?></label><br>
						<?php echo SMP_Hub::wp_dropdown_hubs(array( 'class'=>'h2', "name"=>"hub_id", 'selected'=>get_post_meta($post->ID, "hub_id", true))); ?>				
					</div>
					<div class="h">
						<label for="owner_id"><?php echo __("Owner of Goods Butch", "smp");?></label><br>
						<?php echo $Soling_Metagame_Constructor->get_location_selecter(array('class'=>'h2', "name"=>"owner_id", 'selected'=>get_post_meta($post->ID, "owner_id", true))); ?>
					</div>
					<div class="h">
						<label for="preority"><?php echo __("Preority", "smp");?></label><br>
						<input id="preority" name="preority" value="<?php echo get_post_meta($post->ID,'preority', true); ?>"/>
					</div>
					<div class="h">
						<label for="limit"><?php echo __("Limit per production cyrcle", "smp");?></label><br>
						<input id="limit" name="limit" min="0" step="1" type="number" value="<?php echo $limit; ?>"/>
						<p style='description'><?php _e("Stay Null for infinity", "smp"); ?>
					</div>
					<div class="h">
						<label for="goods_type_id"><?php echo  __("Goods type", "smp");?></label><br>
						<select name="goods_type_id" class='h2'>
						<?php
						foreach($goods as $good)
						{
							echo "<option value='" . $good->ID . "' " . selected($good->ID, get_post_meta($post->ID, "goods_type_id", true)) . " >".$good->ID. ". " . $good->post_title. "</option>";
						}
						?>
						</select>
					</div>	
				</div>
				
			</div>
				<script type="text/javascript">
					jQuery(function($)
					{					
						$("#preority").ionRangeSlider({
								min: 0,
								max: 10, 			
								type: 'single',
								step: 1,
								prettify: true,
								hasGrid: true
						});
					});
				</script>
			<?php 
			wp_nonce_field( basename( __FILE__ ), 'smp_hub_permission_metabox_nonce' );
		}
		
		function true_save_box_smp_hub_permission ( $post_id) 
		{	
			// проверяем, пришёл ли запрос со страницы с метабоксом
			if ( !isset( $_POST['smp_hub_permission_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['smp_hub_permission_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;							
			update_post_meta($post_id, 'preority', 				$_POST['preority']);	
			update_post_meta($post_id, 'limit', 				$_POST['limit']);	
			update_post_meta($post_id, 'hub_id', 				$_POST['hub_id']);				
			update_post_meta($post_id, 'goods_type_id', 		$_POST['goods_type_id']);				
			update_post_meta($post_id, 'owner_id', 				$_POST['owner_id']);					
			return $post_id;
		}
		
		
		function add_views_column_smp_hub_permission( $columns )
{
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
				  "IDs"	 			=> __("ID", 'smp'),
				  "doo"	 			=> __(" "),
				  "hub_id"			=> __("Hub", 'smp'),
				  "owner_id"		=> __("Owner", 'smp'),
				  "goods_type_id" 	=> __("Goods type", 'smp'),		
				  "preority" 		=> __("Preority", 'smp'),		
				  "limit" 			=> __("Limit per production cyrcle", 'smp'),		
			   );
			return $posts_columns;			
		}
		// добавляем возможность сортировать колонку
		function add_views_sortable_column_smp_hub_permission($sortable_columns){
			$sortable_columns['IDs'] 					= 'IDs';						
			$sortable_columns['doo'] 					= 'doo';						
			$sortable_columns['hub_id'] 				= 'hub_id';						
			$sortable_columns['owner_id'] 				= 'owner_id';						
			$sortable_columns['goods_type_id'] 			= 'goods_type_id';				
			$sortable_columns['preority'] 				= 'preority';				
			$sortable_columns['limit'] 					= 'limit';				
			return $sortable_columns;
		}	
		// заполняем колонку данными	
		function fill_views_column_smp_hub_permission($column_name, $post_id)
		{
			$post			= get_post($post_id);
			switch( $column_name) 
			{		
				case 'IDs':
					echo $post_id;
					break;	
				case 'doo':
					echo '
					<a class="button" href = "/wp-admin/post.php?post=' . $post_id. '&action=edit" class="button-smp" title="'.__("Edit").'"><i class="fa fa-gear"></i></a><BR>
					<a class="button" href = "/wp-admin/post.php?post=' . $post_id. '&action=trash" class="button-smp" title="'.__("Delete").'"><i class="fa fa-remove"></i></a><BR>
					<a class="button" href = "/?'.GOODS_BATCH_NAME.'=' . $post->post_title. '" class="button-smp" title="'.__("Goto").'"><i class="fa fa-eye"></i></a>';
					break;	
				case 'hub_id':
					$term_id	=  get_post_meta($post_id, 'hub_id', true);
					echo get_post($term_id)->post_title;
					break;			
				case 'owner_id':
					$term_id	=  get_post_meta($post_id, 'owner_id', true);
					echo get_term_by("id", $term_id, 'location')->name;
					break;			
				case 'goods_type_id':
					$gt		= get_the_post_thumbnail(get_post_meta( $post_id, 'goods_type_id', true ), array(56, 56)); 
					echo $gt;//->post_title;
					break;				
				case 'preority':					
					echo get_post_meta( $post_id, 'preority', true );
					break;					
				case 'limit':					
					$limit	=  get_post_meta( $post_id, 'limit', true );
					echo $limit ? $limit : __("At no allowance", "smp");
					break;		
			}		
		}
		
		static function get_hub_permission_form($permission_post)
		{
			require_once('SMP_Hub_Permission.php');
			return SMP_Hub_Permission::get_short_form($permission_post);
		}
		
		
		
		
		
		//ROUTH
		function add_routh_type()
		{
			$labels = array(
				'name'              => __('Routh', "smp"),
				'singular_name'     => __('Routh', "smp"),
				'search_items'      => __('Search Routh', "smp"),
				'all_items'         => __('All Rouths', "smp"),
				'parent_item'       => __('Parent Routh', "smp"),
				'parent_item_colon' => __('Parent Routh:', "smp"),
				'edit_item'         => __('Edit Routh', "smp"),
				'update_item'       => __('Update Routh', "smp"),
				'add_new_item'      => __('Add Routh', "smp"),
				'new_item_name'     => __('New Routh Name', "smp"),
				'menu_name'         => __('Rouths', "smp"),
			);

			$args = array(
								'labels' 			=> $labels,
								'show_ui'			=> true,
								'hierarchical' 		=> false,
								'query_var' 		=> true,
								'rewrite'	 		=> 'true',
								'show_admin_column' => true,
			);
			register_taxonomy( 'smp_routh', array("none"), $args );
		}
		
		function add_smp_routh($tax_name)
		{
			global $Soling_Metagame_Constructor;
			?>
			<tr class="form-field">
					<label for="term_meta[location_id]"><?php _e('Location', "smc"); ?></label>									
					<?php echo $Soling_Metagame_Constructor->get_location_selecter(array("name"=>"term_meta[location_id]", 'class'=> 'chosen-select', "selected"=>$term_meta['location_id'])); ?>	
			</tr>			
			<div class="form-field">
				<label for="term_meta[start_hub_id]"><?php _e("Start Hub", "smp"); ?></label>	<BR>
				<?php echo SMP_Hub::wp_dropdown_hubs(array("name"=>"term_meta[start_hub_id]", 'class'=> 'chosen-select')); ?>
			</div>					
			<div class="form-field">
				<label for="term_meta[finish_hub_id]"><?php _e("Finish Hub", "smp"); ?></label>	<BR>
				<?php echo SMP_Hub::wp_dropdown_hubs(array("name"=>"term_meta[finish_hub_id]", 'class'=> 'chosen-select')); ?>
			</div>							
			<div class="form-field">
				<label for="type"><?php _e("Route Type", "smp");?></label><br>
				<?php echo SMP_Routh::dropdown_routh_type( array("class" => "chosen-select", "name" => "term_meta[type]", "id" => "type") ); ?>
			</div>					
			<div class="form-field">
				<label for="term_meta[cost]"><?php _e("Cost", "smp"); ?></label>	<BR>
				<input type="number" name="term_meta[cost]" min="0" step="1"/>
			</div>					
			<div class="form-field">
				<label for="term_meta[capacity]"><?php echo __("Capacity", "smp") . " (" . __("per units", "smp"). ")"; ?></label>	<BR>
				<input type="number" name="term_meta[capacity]" min="0" step="1"/>
			</div>					
			<div class="form-field">
				<label for="term_meta[speed]"><?php echo __("Speed", "smp"). " (". __("per minutes", "smp"). ")"; ?></label>	<BR>
				<input type="number" name="term_meta[speed]" min="0" step="1"/>
			</div>			
			<script>
				set_chosen(".chosen-select", {max_selected_options: 5});
			</script>
			<?php
		}
		
		function edit_smp_routh($term, $tax_name)
		{
			global $Soling_Metagame_Constructor;
			$t_id = $term->term_id;
			$term_meta = get_option( "smp_routh_$t_id" ); 
			var_dump($term_meta);
			?>
			<tr class="form-field">
				<th scope="row" valign="top">
					<label for="term_meta[location_id]"><?php _e('Location', "smc"); ?></label>
				</th>
				<td>					
					<div class="form-field" >
						<?php echo $Soling_Metagame_Constructor->get_location_selecter(array("name"=>"term_meta[location_id]", 'class'=> 'chosen-select', "selected"=>$term_meta['location_id'])); ?>					
					</div>	
				</td>
			</tr>
			<tr class="form-field">
				<th scope="row" valign="top">
					<label for="term_meta[start_hub_id]"><?php _e('Start Hub', "smp"); ?></label>
				</th>
				<td>					
					<div class="form-field" >
						<?php echo SMP_Hub::wp_dropdown_hubs(array("name"=>"term_meta[start_hub_id]", 'class'=> 'chosen-select', "selected"=>$term_meta['start_hub_id'])); ?>					
					</div>	
				</td>
			</tr>
			<tr class="form-field">
				<th scope="row" valign="top">
					<label for="term_meta[finish_hub_id]"><?php _e('Finish Hub', "smp"); ?></label>
				</th>
				<td>					
					<div class="form-field" >
						<?php echo SMP_Hub::wp_dropdown_hubs(array("name"=>"term_meta[finish_hub_id]", 'class'=> 'chosen-select', "selected"=>$term_meta['finish_hub_id'])); ?>					
					</div>	
				</td>
			</tr>
			<tr class="form-field">
				<th scope="row" valign="top">
					<label for="term_meta[cost]"><?php _e("Cost", "smp"); ?></label>	<BR>					
				</th>
				<td>					
					<div class="form-field" > 
						<input type="number" name="term_meta[cost]" min="0" step="1" value="<?php echo $term_meta['cost']?>"/>					
					</div>	
				</td>
			</tr>
			<tr class="form-field">
				<th scope="row" valign="top">
					<label for="term_meta[capacity]"><?php echo __("Capacity", "smp") . " (" . __("per units", "smp"). ")"; ?></label>	<BR>
				</th>
				<td>					
					<div class="form-field" > 
						<input type="number" name="term_meta[capacity]" min="0" step="1" value="<?php echo $term_meta['capacity']?>"/>					
					</div>	
				</td>
			</tr>
			<tr class="form-field">
				<th scope="row" valign="top">
					<label for="term_meta[speed]"><?php echo __("Speed", "smp"). " (". __("per minutes", "smp"). ")"; ?></label>	<BR>
				</th>
				<td>					
					<div class="form-field" > 
						<input type="number" name="term_meta[speed]" min="0" step="1" value="<?php echo $term_meta['speed']?>"/>					
					</div>	
				</td>
			</tr>
			<tr class="form-field">
				<th scope="row" valign="top">					
					<label for="type"><?php _e("Route Type", "smp");?></label><br>	
				</th>
				<td>					
					<div class="form-field" > 
						<?php echo SMP_Routh::dropdown_routh_type( array("class" => "chosen-select", "name" => "term_meta[type]", "id" => "type", "selected" => $term_meta['type']) ); ?>			
					</div>	
				</td>
			</tr>
			<tr class="form-field">
				<th scope="row" valign="top">
					<label for="term_meta[geometry]"><?php echo __("Geometry", "smp"); ?></label>	<BR>
				</th>
				<td>					
					<div class="form-field" > 
					<?php
						require_once(SMP_REAL_PATH.'tpl/Routh_map_editor.php');
					?>				
					</div>	
				</td>
			</tr>
			<script>
				set_chosen(".chosen-select", {});
			</script>
			<?php
		
		}
		
		function save_smp_routh_custom_meta( $term_id ) 
		{
			if ( isset( $_POST['term_meta'] ) ) {
				$t_id = $term_id;
				$term_meta = get_option( "smp_routh_$t_id" );
				$cat_keys = array_keys( $_POST['term_meta'] );
				foreach ( $cat_keys as $key ) {
					if ( isset ( $_POST['term_meta'][$key] ) ) {
						$term_meta[$key] = $_POST['term_meta'][$key];
					}
				}
				// Save the option array.
				update_option( "smp_routh_$t_id", $term_meta );
			}
		}
		
		function smp_routh_columns($theme_columns) {
			$new_columns = array
			(
				'cb' 			=> '<input type="checkbox" />',
				'type'			=> __("Type", "smp"), 
				'id' 			=> 'id',
				'name' 			=> __('Name'),
				'start_hub_id' 	=> __('Start Hub', 'smp'),
				'finish_hub_id' => __('Finish Hub', 'smp'),
				'cost' 			=> __('Cost', 'smp'),
				//'capacity' 		=> __('Capacity', 'smp'),
				'speed' 		=> __('Speed', 'smp'),
			);
			return $new_columns; 
		}
		function manage_smp_routh_columns($out, $column_name, $theme_id) 
		{
			$theme 	= get_term($theme_id, 'smp_routh');
			$tax	= get_option("smp_routh_$theme_id");
			switch ($column_name) {
				case 'id':
					$out 		.= $theme_id;
					break;
				case 'type':
					$type_id	= $tax['type'];
					//$type		= get_post($type_id);
					$fill		= ($type_id == "") ? "000" : get_post_meta($type_id, "color", true);
					$color		= $tax['color'];	
					$out		.= "<div style='width:15px; height:15px; display:inline-block; position:relative; background:#".$fill."' > </div>";
					$out		.= "<div style='width:15px; height:15px; display:inline-block; position:relative; background:#".$color."' > </div>";
					break;
				case 'start_hub_id': 
					$hub		= get_post($tax['start_hub_id']);
					$out 		.= $hub->post_title;
					break;	 
				case 'finish_hub_id': 
					$hub		= get_post($tax['finish_hub_id']);
					$out 		.= $hub->post_title;
					break;	 
				case 'cost': 
					$out 		.= $tax['cost'];
					break;		  	 
				case 'capacity': 
					$out 		.= $tax['capacity'];
					break;		 	 
				case 'speed': 
					$out 		.= (int)$tax['speed'];
					break;		  	 
				default:
					break;
			}
			echo $out;    
		}
		
		function delete_smp_routh_handler($id)
		{
			delete_option("smp_routh_$id");
		}
		
		
		
		//===================================
		//	SMC FILTERS
		//===================================
		
		function smc_location_archive_title($args, $cur_term)
		{
			$tax_type 			= get_query_var('taxonomy');
			global $Soling_Metagame_Constructor;
			global $user_iface_color;
			$op					= get_option(SMC_ID);
			
			switch($tax_type)
			{
				case 'smp_routh':
					$rr				= __('Routh','smp')."";
					return $rr;
				case 'location':
					if($op['use_panel'])	return $args;
					$rr				= "
					<div class='location-tamplate-content'>
					<hr/><h2>".__("all Hubs", "smp").":</h2>";
					$ar				= array(										
													'numberposts'	=> 0,
													'offset'    	=> 0,
													'orderby'  		=> 'id',
													'order'     	=> 'DESC',
													'post_type' 	=> 'smp_hub',
													'post_status' 	=> 'publish',
													'meta_query' 	=> array(
																				array(
																						"key"		=> "dislocation_id",
																						"value"		=> $cur_term->term_id,
																					),
																			),
												);
					$hubs			= get_posts($ar);
					if(count($hubs))
					{
						$rr					.="<ul>";
						foreach($hubs as $hub)
						{
							$rr				.= '<li>' . SMP_Hub::get_short_form($hub) . '</li>';
						}
						$rr					.= "</ul>";
						$rr					.= "<hr/></div>";
					}
					else
					{
						$rr					= "";
					}	
					return $args . $rr;
			}
		
		}
		
		
		function smc_map_external_0($text, $location_id)
		{
			global $Soling_Metagame_Constructor;
			$args		= array(
								 'number' 		=> 0
								,'offset' 		=> 0
								,'orderby' 		=> 'name'
								,"hide_empty"	=> false
							);
			$rouths		= get_terms('smp_routh', $args);
			$html		= '<div style="pointer-events: none; "><svg width="2000" height="' . $Soling_Metagame_Constructor->options['smc_height'] . '" xmlns="http://www.w3.org/2000/svg" id="smp_routh_geom_cont">';
			$centeres	= "<div id='routes_centers' style='position:absolute; top:0; left:0; '>";
			foreach($rouths as $routh)
			{
				$r		= new SMP_Routh($routh->term_id);
				$option	= $r->get_option();
				// if($option['location_id'] != $location_id)	continue;
				$draw	= $r->draw();
				$html	.= $draw[0];
				$centeres.= $draw[1];
			}
			$centeres	.= '</div>';
			return $text . $html . '</svg></div>' . $centeres;
		}
		
		function smc_login_widget_list1($arr)
		{
			if (!SolingMetagameProduction::is_logistics())	return;
			$arr	= array_merge( $arr, array(
				"logistics"		=> array(
					"title"		=> __("My Logistics", "smp"),
					"picto"		=> SMP_URLPATH . "icon/logistics_ico.png",
					"url"		=> get_permalink($this->options['my_hub_ID'])
				)
			));
			
			return $arr;
		}
		
		function my_extra_fields_movebled()
		{
			global $moveble_types;
			foreach($moveble_types as $mov)
			{
				add_meta_box( 'extra_fields2', __('My Logistics', "smp"), array(&$this, 'extra_fields_box_movebled'), $mov, 'normal', 'high'  );
			}
		}
		function extra_fields_box_movebled($post)
		{
			$post_id	= $post->ID;
			?>
				<div class="h h2">
					<label for="transportation_id"><?php echo __("Transportation", "smp");?></label><br>					
					<?php 
						// ID of routh
						$disl_id	= get_post_meta($post_id, "dislocation_id", true);
						echo SMP_Routh::wp_dropdown_hub_rouths($disl_id,  array( 'selected'=>get_post_meta($post_id, 'transportation_id', true), 'id'=>'transportation_id',  'name'=>'transportation_id', 'class' => "chosen-select h2") );
						
					?>	
					<input type="checkbox" id="is_permission" name="is_permission" class="css-checkbox"  <?php echo checked(get_post_meta($post_id,'is_permission', true));?>/>
					<label for="is_permission" class="css-label"><?php _e("is permission","smp");?></label><BR>
				</div>
			<?php
		}
		function save_logistics_parameters($post_id) 
		{
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;	
				
			update_post_meta($post_id, 'transportation_id', 		$_POST['transportation_id']);
			if($_POST['transportation_id']>0)
			{			
				update_post_meta($post_id, 'is_permission', 		$_POST['is_permission']=='on');
			}
			else
			{
				update_post_meta($post_id, 'is_permission', 		0);
			}
			return $post_id;
		}
		function add_views_column_batch( $columns )
		{
			$columns['transportation_id']						= "<i class='fa fa-truck fa-2x hinter' data-hint='".__("Transportation", "smp")."'></i>";//;
			return $columns;
		}
		function add_views_sortable_column_batch($sortable_columns){
			$sortable_columns['transportation_id'] 				= 'transportation_id';	
			return $sortable_columns;
		}
		function fill_views_column_batch($column_name, $post_id)
		{
			//$post			= get_post($post_id);
			switch( $column_name) 
			{				
				case 'transportation_id':
					$gt			= get_post_meta( $post_id, 'transportation_id', true );
					$routh		= get_term_by("id", $gt, "smp_routh");
					$routh_name	= $routh->name;
					$per		= get_post_meta( $post_id, "is_permission", true );
					$perm		= get_post_meta( $post_id, "is_permanent", true );
					echo $gt>0  	? '<img src="'.SMP_URLPATH.'img/check_checked.png" title="'.$routh_name.'">' : '<img src="'.SMP_URLPATH.'img/check_unchecked.png">';
					echo "<BR>";
					echo $per>0  	? '<img src="'.SMP_URLPATH.'img/check_checked.png" title="'.__("is permission","smp").'">' : '<img src="'.SMP_URLPATH.'img/check_unchecked.png">';
					//echo "<BR>";
					//echo $perm>0 	? '<img src="'.SMP_URLPATH.'img/check_checked.png" title="'.__("automatically renew","smp").'">' : '<img src="'.SMP_URLPATH.'img/check_unchecked.png">';
					
					break;	
			}
		}	
		
		
		//вкладка "Транспортировка"		
		function smp_butch_slide_3($args, $batch_id)
		{
			$batch			= SMP_Goods_Batch::get_instance($batch_id);
			$disloc_id		= $batch->get_dislocation_id();
			$count			= $batch->get_meta("count");
			$is_permission	= $batch->get_meta("is_permission");
			$transp_id		= $batch->get_meta("transportation_id");
			$hub_ids		= SMP_Hub::get_all_hubs_by_dislocation($disloc_id);
			$html1			.= "<div class='smp-comment'><h3>".__("Hubs", "smp")."</h3>";
			foreach($hub_ids as $h)
			{
				$hub		= get_post($h);
				$html1		.= ' - '.$hub->post_title. "(" . __("Free: " ,"smp") . SMP_Hub::get_free_capacities($h) . " " .__("unit", "smp") . ")<BR>";
			}
			$html1		.=  "";
			if(count($hub_ids)==0)
				$html		.= $html1."
					<div class='smp-batch-tab-slide'>
						<div class='smp-comment'>".__("There are no aveilable Rouths for trasportation this batch", "smp")."</div>
					</div>
				</div>";
			else
			{
				$html1			.= "
					<div class='smp-table-null' style='margin:7px 0; vertical-align:middle;'>".
						"<img src='" . SMP_URLPATH . ($is_permission ? "img/check_checked.png" : "img/check_unchecked.png") ."' style='vertical-align:middle;'> <span style='vertical-align:middle;'>". __("have transit permission", "smp"). "</span>". 
					"</div>
				</div>";
				$html			.= "
				<div class='smp-batch-tab-slide'>
					<div class='smp-table-null'>
						<label for='batch_count_$batch_id'>".__("Select count of units for transportation","smp")."</label><br>
						<input class='number_tr_gb lbtr' id='batch_count_$batch_id' name='batch_count_$batch_id' type='range' min='0' max='$count' value='$count' bid='$batch_id'/> 
						<input id='cnt$batch_id' class='unvsbl' type='number' max='$count' min='0' value='$count' style='width:100px;'/> ".
						__("unit", "smp").
					"</div>
					<div class='smp-table-null'>
						<label>".__("Choose one of available Routh for transportation this batch to Location-reciever", "smp").'</label><BR>
						<label class="smc_input"> '.
							SMP_Routh::wp_dropdown_rouths( array( 'class' => "chosen-select", 'name'=>"batch_routh_recivier_", 'id'=> 'batch_routh_recivier_'.$batch_id, "hub_id"=>$hub_ids, 'selected' => $transp_id) ) .
					"	</label>
					</div>					
					$html1 
					<div class='smp-table-null smc_margin_top' style='' batch_id='".$batch_id."'>
						<div id='save_change_transportation_".$batch_id."' class='button smc_padding' transport_dist_id='" . $batch_id . "' cash='220'>
							".__("Send request to Hub Administration", "smp")."
						</div>
					</div>".						
				"</div>";
			}
			return array("title" => __("Transportation", "smp"), 	"slide" => $html);
		}
		function smp_butch_slide_3_($arr, $batch_id)
		{			
			//
			$batch			= SMP_Goods_Batch::get_instance($batch_id);
			$routh_id		= $batch->get_meta("transportation_id");
			$routh			= get_term_by("id", $routh_id, "smp_routh");
			//
			$disl_id		= $batch->get_dislocation_id();
			$count			= $batch->get_meta( "count");
			$arg			= array(
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'id',
										'order'     	=> 'DESC',
										'post_type' 	=> 'smp_hub',
										'post_status' 	=> 'publish',
										'meta_query' 	=> array(
																	array(
																			"key"			=> "dislocation_id",
																			"value"			=> $disl_id,
																			"compare"		=> "="
																		  ),
																 ),
									);
			$hubs			= get_posts($arg);
			$hub_ids		= array();
			foreach($hubs as $hub)
			{
				if(!isset($routh_id) || $routh_id == "")	continue;				
				if(!isset($routh))	continue;
				
				$tax				= get_option("smp_routh_".$routh_id);
				$start_hub_id		= $tax['start_hub_id'];
				$start_loc_id		= get_post_meta($start_hub_id,	"dislocation_id", true);
				$finish_hub_id		= $tax['finish_hub_id'];
				$finish_loc_id		= get_post_meta($finish_hub_id,	"dislocation_id", true);
				$dislocation_id		= get_post_meta( $id, 			"dislocation_id", true );
				if($dislocation_id	!= $start_loc_id || $dislocation_id !== $finish_loc_id)	continue;
				//
				$hub_ids[]			= $hub->ID;
			}
			
			$transit_info			.= '';
			"<div class='smp-comment'>".
				__("Transportation batch help", "smp").
			"</div>
			<div id='transit_info'>
				<label>".__("Transit Information", "smp")."</label>
				<br>".
				($routh_id ? __("Batch on way", "smp") : __("Goods batch is not on way", "smp")).
			"</div>";
			
			$html			= ($hubs[0]) 
			? 
			"<div class='smp-batch-tab-slide'>
				<div class='smp-table-null'>
					<label for='batch_count_".$batch_id."'>".__("Select count of units for transportation","smp")."</label><br>
					<input  id='batch_count_".$batch_id."' name='batch_count_".$batch_id."' type='number' min='0' max='".$count."' value='".$count."' style='width:140px;'/> ".
					__("unit", "smp").
				"</div>
				<div class='smp-table-null'>
					<label>".__("Choose one of available Routh for transportation this batch to Location-reciever", "smp").'</label><BR>'.
					SMP_Routh::wp_dropdown_rouths( array( 'class' => "chosen-select_1_", 'name'=> 'batch_routh_recivier_'.$batch_id, "hub_id"=>$hub_ids, 'selected' => get_post_meta($batch_id, 'transportation_id', true)) ) .
				"</div>
				<div class='smp-table-null' style=''>
					<input name='save_change_transportation_".$batch_id."' type='submit'  class='smp-colorized smp-submit'  value='".__("Send request to Hub Administration", "smp")."'/>
				</div>
			</div>".$transit_info
			: 
			"<div class='smp-batch-tab-slide'>
				<div class='smp-comment'>".__("There are no aveilable Rouths for trasportation this batch", "smp")."</div>
			</div>".$transit_info;
			return array("title" => __("Transportation", "smp"), 	"slide" => $html);
		}
		function smp_send_batch_changes($batch_id)
		{
			if($_POST['save_change_transportation_'.$batch_id])
			{
				$count		= $_POST['batch_count_'.$batch_id];
				$access		= $this->transportation_batch_change($batch_id, $_POST['batch_routh_recivier_'.$batch_id], $count);
				
			}
		}
		
		//'edit Location'. Add rouths tomain Map
		function smc_edit_location_main_map($text, $location_id, $location_data)
		{
			global $smc_height;
			$location		= SMC_Location::get_instance($location_id);
			$par			= SMC_Location::get_instance($location->parent);
			$rouths			= SMP_Routh::get_all_rouths("ids");	
			$dat			= '';
			foreach($rouths as $route)
			{
				$term_m		= get_option("smp_routh_".$route);
				if( $term_m['location_id'] == $location->parent || $term_m['location_id'] == $par->parent )
					$dat	.= '<path d="'.$term_m['geometry'].'" stroke="#000000" stroke-width="4" stroke-opacity="1" fill-opacity="0"></path>';
			}
			foreach($rouths as $route)
			{
				$term_m		= get_option("smp_routh_".$route);
				if( $term_m['location_id'] == $location->parent || $term_m['location_id'] == $par->parent  )
					$dat	.= '<path d="'.$term_m['geometry'].'" stroke="#00ff00" stroke-width="2" stroke-opacity="0.5" fill-opacity="0"></path>';
			}
			return $text.
			"	<div style='position:absolute;top:0; left:0; height:$smc_heightpx; width:100%;'>
					<svg width='1000' height='$smc_height' xmlns='http://www.w3.org/2000/svg' >
						<g>$dat</g>
					</svg>
				</div>";
		}
		
		//
		function smp_goods_batch_hint($text, $batch_id)
		{
			return $this->smp_batch_form_dislocation($text, $batch_id);
		}
		function smp_draw_storage_goods_batch_container($text, $batch_id)
		{
			$batch				= SMP_Goods_Batch::get_instance($batch_id);
			$tarnsportation		= $batch->get_meta("transportation_id");
			$is_permission		= $batch->get_meta("is_permission");
			$tex				= $is_permission ?
			$text."
			<div style='position:absolute; pointer-events: none;'>
				<svg width='84.25px' height='74.75px' viewBox='0 0 84.25 74.75' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'>
					<g>
						<path stroke='none' fill='#ffffff' d='M80.65 -12.7 L80.65 -20.95 80.65 -21.05 80.75 -21.05 80.85 -21.05 80.9 -20.95 80.9 -12.7 80.65 -12.7 M72.1 -12.7 L71.85 -12.7 71.85 -16.35 71.85 -16.45 71.95 -16.45 72.05 -16.45 72.1 -16.35 72.1 -12.7'/>
						<path stroke='none' fill='#ffffff' d='M72.1 -12.7 L72.1 -8.75 71.85 -8.75 71.85 -12.7 72.1 -12.7 M80.65 -12.7 L80.9 -12.7 80.9 -8.75 80.65 -8.75 80.65 -12.7 M62.55 -8.75 L62.3 -8.75 62.3 -11 62.3 -11.1 62.4 -11.1 62.5 -11.1 62.55 -11 62.55 -8.75 M85 15 L83.45 15.9 85.05 15.9 85.05 16.15 83 16.15 72.6 22 85.2 22 85.2 22.25 72.15 22.25 64.5 26.55 85.35 26.55 85.35 26.8 64.05 26.8 54.4 32.25 85.5 32.3 85.5 32.55 53.95 32.5 44.9 37.6 85.65 37.6 85.65 37.85 44.45 37.85 34.75 43.3 83.15 43.3 82.7 43.55 34.3 43.55 27.9 47.15 27.9 52.45 67.45 52.45 67.05 52.7 27.9 52.7 27.9 60.45 53.75 60.45 53.35 60.7 27.9 60.7 27.9 68.85 39.4 68.85 38.95 69.1 27.9 69.1 27.9 75.55 27.5 75.75 26.9 75.75 26.9 69.1 26.8 69.05 26.8 68.95 26.8 68.85 26.9 68.85 26.9 68.7 19.4 72 19.1 71.85 26.9 68.4 26.9 61.45 12.2 67.95 11.95 67.8 26.9 61.15 26.9 52.85 26.85 52.8 26.8 52.75 3.55 63.05 3.25 62.85 26.9 52.4 26.9 47.35 24.6 46.25 1.7 56.4 1.7 56.1 24.3 46.1 17.1 42.55 1.65 49.4 1.65 49.15 16.7 42.45 16.65 42.45 16.65 42.35 16.65 42.3 9.5 38.8 1.6 42.2 1.6 41.95 9.15 38.65 2.05 35.15 1.8 34.85 1.75 34.6 2.4 34.25 2.5 34.25 9.45 37.65 9.45 30.15 9.7 30 9.7 37.8 16.65 41.2 16.65 26 16.9 25.85 16.9 41.3 23.9 44.75 23.9 21.8 24.15 21.65 24.15 44.85 27.25 46.4 33.05 43.15 33.05 16.5 Q33.15 16.4 33.3 16.35 L33.3 43 42.5 37.8 42.45 37.8 42.45 37.7 42.45 37.6 42.55 37.6 42.55 11 42.8 10.85 42.8 37.6 42.9 37.6 52.1 32.4 52.1 32.35 52.1 32.25 52.2 32.25 52.4 32.25 52.5 32.2 52.5 32.1 52.45 5.25 52.7 5.1 52.75 32.05 62.3 26.65 62.3 2.4 62.55 2.55 62.55 26.5 71.85 21.3 71.85 21.2 71.85 7.7 72.1 7.85 72.1 21.15 80.65 16.35 80.65 12.6 80.9 12.75 80.9 16.2 81.1 16.05 81.1 16 81.1 15.9 81.2 15.9 81.4 15.9 84 14.45 85 15'/>
						<path stroke-fill='#000000' fill-opacity='0.9' fill='000000' d='M85 15 L85.75 41.75 27.5 75.75 26 75.75 1.75 62 1.5 34.75 59.75 1 85 15'/>
					</g>
				</svg>	
				</svg>
			</div>" :
			$text;
			return $tex;
		}
		
		function transportation_batch_change($batch_id, $routh_id, $count=0)
		{		
			if( $routh_id < 1 )
			{
				update_post_meta($batch_id, 'transportation_id', "");
				return new WP_Error(
										"no_tranportation", 
										"<h2>".__("Transportation canselled", "smp")."</h2>".
										__("All moves of this cargo are cancelled. Goods batch returned to Storage", "smp")
									);
			}
			$batch				= new Batch($batch_id);
			if($count == 0)
			{
				$count			= $batch->get_meta( "count" );
			}
			//если включено управление хабами
			switch($this->options['is_hubs_uncontrolled'])
			{
				case 0: // разрешение даёт владелец хаба
				case 1: // разрешение дается системой на основании параметров хаба
					$hb			= Goods_Batch::get_batch_hub_by_route($batch_id, $routh_id);
					$hub		= SMP_Hub::get_instance($hb);
					$cap		= $hub->get_meta("capacity");
					if($cap < $count)
					{
						
						$howner	= $hub->get_meta("owner_id");
						return new WP_Error( 
												"not_free", 
												"<h2>".sprintf(__("Too large batch for %s!", "smp"), $hub->get('post_title') )."</h2>".
												"<div class='abzaz'>".
												sprintf(	
															__("Your cargo too large for transportation across this Hub. <br>The amount of consignment is %s but capacity of Hub is %s.", "smp"),
															"<b>" . $count . " " . __("unit", "smp") . " </b>", 
															"<b>" . $cap . " " . __("unit", "smp") . " </b>"
														).
												"</div>
												<div class='smp-colorized smp-submit smc_call_to_owner ' owner_id='$howner' style='position:relative!important;margin-top:10px;'>".__("Call to owner of Hub ", "smp")."</div>"
											);
					}
					$free		= SMP_Hub::get_free_capacities($hb);
					if($free < $count)
					{
						$howner	= $hub->get_meta("owner_id");
						return new WP_Error( 
												"not_free", 
												"<div class='abzaz'>".sprintf(
															__("%s The Hub %s have not free capacities. Call the owner of his Hub to be able to move your cargo.", "smp"),
															"<h2>".__("Not Success!", "smp")."</h2>", 
															"<b>".$hub->get('post_title')."</b>"
														) . 
												"</div>
												<div class='smp-colorized smp-submit smc_call_to_owner ' owner_id='$howner' style='position:relative!important;margin-top:10px;'>".__("Call to owner of Hub ", "smp")."</div>"
											);
					}
					require_once('SMP_Hub_Permission.php');
					$check				= SMP_Hub_Permission::is_goods_batch_has_permission($batch_id);	
					break;
				case 2: // fly away!
					$check				= true;
					break;
				
			}
			
			$foo		= $batch->rize_batch_transportation2($count, $routh_id, $batch->body->post_type);
			$new_batch	= $foo[0];
			if($check)
			{
				if(!$new_batch)
				{
					return new WP_Error( "on_way", __("This batch on way now.", "smp") );
				}
				$new_batch->update_meta( 'is_permission', 1 );
				$r				= get_option( "smp_routh_$routh_id" );
				$speed			= SMP_Routh::get_routh_speed($r) * 60;
				$new_batch->update_meta( 'finish_time', time() + $speed   );
				return $new_batch;
			}
			else
			{				
				//update_post_meta($batch_id, 'transportation_id', $routh_id);
				return new WP_Error(
										"Not_permission",
										"<h2>" . __("The answer to your inquiry", "smp")."</h2>
										<div>" . 
											//$new_batch->id . "<br>" . $foo[1] . "<br>" .
											__("The owner of the Hub have received your request. You will be notified of the start of transportation of your cargo.", "smp").
										"</div>"
									);
			}
		}
		
		function smp_batch_form_dislocation($arg, $batch_id)
		{
			global $Soling_Metagame_Constructor;
			$transportation_id		= get_post_meta($batch_id, "transportation_id", true);
			$is_permission			= get_post_meta($batch_id, "is_permission", 	true);
			$finish_time			= get_post_meta($batch_id, "finish_time", 		true);
			$remaind				= $finish_time == "" || $finish_time == -1 ? 0 : floor(($finish_time - time())/60);
			$remaind_comm			= $remaind > 0 ? __("Remaining time", "smp") : __("Lating time", "smp");
			$remaind				= abs($remaind);
			$par					= get_option("smp_routh_".$transportation_id);
			$speed					= SMP_Routh::get_routh_speed($par);
			$last_step_time			= $par['last_step_time'];
			
			if( !($transportation_id == "" || $transportation_id == -1 ))
			{
				$routh				= get_term_by('id', $transportation_id, "smp_routh");	
				if( $is_permission == 1)
				{			
					return '
					<tr style="padding:0; margin:0;" valign="top"> 
						<td width="110"  align="right"   class="lp-batch-table-coll-setting lp-batch-col1" >
							<span class="lp-batch-comment">'.
								__("in transit by", "smp").' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Owners of this Location  controlled this Goods Batch.", "smp")) .
							'</span>
						</td  align="left">
						<td width="180"  class="lp-batch-table-coll-setting lp-batch-col2">
							<span style="font-size:16px;"><i class="fa fa-truck"></i></span> <span  class="lp-batch-data">' . $routh->name .	'</span>
						</td>
					</tr>
					<tr style="padding:0; margin:0;" valign="top"> 
						<td width="110"  align="right"   class="lp-batch-table-coll-setting lp-batch-col1" >
							<span class="lp-batch-comment">'.
								$remaind_comm.' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Owners of this Location  controlled this Goods Batch.", "smp")) .
							'</span>
						</td  align="left">
						<td width="180"  class="lp-batch-table-coll-setting lp-batch-col2">
							<span class="smp-goods-1">'.
							' '	. $remaind  . __(' minutes', 'smp').				
							'</span>
						</td>
					</tr>
					<tr style="padding:0; margin:0;" valign="top"> 
						<td width="110"  align="right"   class="lp-batch-table-coll-setting lp-batch-col1" >
							<span class="lp-batch-comment">'.
								__("Full time", "smp").' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Owners of this Location  controlled this Goods Batch.", "smp")) .
							'</span>
						</td  align="left">
						<td width="180"  class="lp-batch-table-coll-setting lp-batch-col2">
							<span class="smp-goods-1">'.
							' '	. ($speed) . __(' minutes', 'smp').				
							'</span>
						</td>
					</tr>';
				}
				else
				{
					return '
					<tr style="padding:0; margin:0;" valign="top"> 
						<td width="110"  align="right"   class="lp-batch-table-coll-setting lp-batch-col1" >
							<span class="lp-batch-comment">'.
								__("Requested to", "smp").' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Owners of this Location  controlled this Goods Batch.", "smp")) .
							'</span>
						</td  align="left">
						<td width="180"  class="lp-batch-table-coll-setting lp-batch-col2">
							<span style="font-size:16px;"><i class="fa fa-truck"></i></span> <span  class="lp-batch-data">' . $routh->name .	'</span>
						</td>
					</tr>';
				}
			}
			else
			{
				return $arg;
			}
		}
		
		function smp_bath_stroke_exec($args, $user_is_owner, $goods_batch, $owner)
		{		
			//return  $args . "<div style='background:red;'>WWW</div>";
			global $Soling_Metagame_Constructor;
			$id					= $goods_batch->ID;		
			
			//server
			if($_POST['update_hub_batches'])
			{
				update_post_meta($id, 'is_permission',	$_POST['is_permission_'.$id] == 'on');
				update_post_meta($id, 'is_permanent',	$_POST['is_permanent_'.$id]  == 'on');
			}
			
			
			$count				= get_post_meta($id, "count", true);
			$transoptition		= '
			<div class="">
				<div class="h">
					<label for="permission_volume'.$id.'">'.__("allowed volume", "smp").'</label><br>
					<input  class="h2" name="permission_volume'.$id.'" id="permission_volume'.$id.'" type="number" min="0" step="1" max="'.$count.'" value ="'.$count.'"/>
				</div>
				<div class="h">
					<input '.checked( get_post_meta($id, 'is_permission', true), 1, false).' id="is_permi'.$id.'" type="checkbox" class="css-checkbox_black" name="is_permission_'.$id.'"/>
					<label for="is_permi'.$id.'" class="css-label_black">' . __("is permission", "smp")	. '</label>
				</div>
			</div>';
			return $args . '
						<table class="lp-batch-table" cellpadding="0" cellspacing="0" align="left" style="display:table; margin-bottom:45px;" > 			
							<tr class="lp-batch-table-coll-setting" valign="middle">
								<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1">					
									<span class="lp-batch-comment" style="width:125px;margin-top:7px;">'.
										__("Transit", "smp").' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("This is permission for transit this goods batch to finish Hub", "smp")) .
									'</span>
								</td  align="left">
								<td class="lp-batch-table-coll-setting lp-batch-col2" style="min-width:300px;">
									<div style="padding:3px 0;" per_batch_id="'.$id.'" ask="'.__('Cancellation of permit return the batch to start location. Continue?', 'smp').'">
										<input  id="is_permi'.$id.'" type="checkbox" class="css-checkbox_black22 get_permission" name="is_permission_'.$id.'" '. checked(get_post_meta($id, "is_permission",true), 1,false).'/>
										<label for="is_permi'.$id.'" class="css-label_black22">' . __("is permission", "smp") . '</label><BR>
									</div>
								</td>
							</tr>
						</table>
					<form name="new_post_'.$id.'" method="post"  enctype="multipart/form-data" id="new_post_'.$id.'">
					</form>';
					"<div style='padding:3px 0; width:350px;'>
						<input  id='is_permi".$id."' type='checkbox' class='css-checkbox_black' name='is_permission_".$id."'/>
						<label for='is_permi".$id."' class='css-label_black'>" . __("is permission", "smp") . "</label><BR>
						
						<input  id='is_perm".$id."' type='checkbox' class='css-checkbox_black' name='is_permanent_".$id."'/>
						<label for='is_perm".$id."' class='css-label_black'>" . __("automatically renew", "smp")	. "</label>
					</div>
					<div id='exec_batch_" . $id . "' style='display:none;'>
						<h4>". __("Transportation's manipulations", "smp"). "</h4>
						<div>
							<span class='lp-batch-comment'>".__("Owner", "smp")."</span> <span class='lp-batch-data'>". $owner->name."</span>
						</div>
						<form name='new_post_".$id."' method='post'  enctype='multipart/form-data' id='new_post_".$id."'>
								<hr/>".
								$transoptition.
								/*
								SMP_Assistants::get_tabs(
															array(
																	apply_filters("smp_logistic_win_0", array("title" => __("The Permission", "smp"), 		"slide" => $transoptition)),
																
																)
														).
								*/
								"<hr/>
						</form>
					</div>";
		}
		
		function smp_batch_card_price($args)
		{
			return $args."";
		}
		
		
		function get_routh_summae($routh_id, $batch)
		{
			$routh					= get_term_by("id", $routh_id, "smp_routh");
			$routh_name				= $routh->name;
			$par					= get_option("smp_routh_" . $routh_id);
			$start_hub_id			= SMP_Hub::get_start_hub_of_transiting_batch($batch);
			$hub_name				= get_post($start_hub_id)->post_title;
			$tr_speed				= SMP_Routh::get_routh_speed($par);
			$cost					= $par['cost'];	
			$start_hub_summ			= get_post_meta($start_hub_id, "cost", true);
			$count					= get_post_meta($batch, 'count', true);
			return array('cost' => $start_hub_summ * $count  + $cost * $count, 'hub_name'=> $hub_name, 'speed'=>$tr_speed, 'routh_name'=>$routh_name);
		}
		
		
		function smp_my_tools($args)
		{
			if(!$this->options['logistics_enabled'])	return $args;						
			$args[]	= array(
				"title" => "<div class='smp_tool_icon'><img src='" . SMP_URLPATH . "img/hub_icon.png'></div>", 
				"hint" 	=> __("Logistic helper", "smp"), 
				"slide" => "",
				"name" 	=> "get_lgstcs_report_btn",
				"exec"	=> "get_lgstcs_circle_report", 
				"args"	=> "" 
			);
			return $args;
		}
		
		function open_location_content_widget($args, $location_id)		
		{
			$html1					.= "
			<div class='klapan3_subtitle lp_clapan_raze'>
				<div style='position:relative; display:block; '>".
					__("all Hubs", "smp") . 
			'	</div>
				<div class="lp_clapan_raze1"></div>
			</div>';
			$html1		.= "
				<div class='smp_bevel_form'>
				";
			$hubs					= SMP_Hub::get_all_hubs_by_dislocation($location_id[1], true);
			foreach($hubs as $hub)
			{
				$html1				.= SMP_Hub::get_pictogramm($hub);//
			}
			$html1		.= "</div>";
			$new_p					= apply_filters(
														"smp_location_hubs_widget_klapan",
														array
														(
															"title" 	=> "<img src=" . SMP_URLPATH . "img/hub_picto.png>", 
															"slide" 	=> $html1 ,//. $this->admin_consume_form($location_id, true), 
															'hint' 		=> __("all Hubs", "smp")
														),
														$location_id,
														$loc_data
													);
			$last					= $args[count($args)-1];
			$args1 					= array_slice  ($args, 0, -1);
			$args1[]				= $new_p;
			$args1[]				= $last;
			return $args1;
		}
		
		//-------------------
		//
		// CRON EXEC
		//
		//===================
		function get_current_circle_id()
		{
			return (int)get_option( "smp_logistics_circle_id" );
		}
		function update_current_circle_id($id)
		{
			update_option( "smp_logistics_circle_id", ((int)$id)+1 );
		}
		function logistics_handler()
		{
			global $Direct_Message_Menager, $SMP_Currency, $moveble_types;
			//
			$time						= time();	
			
			$results					= "";
			$all_rouths					= SMP_Routh::get_all_rouths();
			foreach($all_rouths as $routh)
			{	
				$par				= get_option("smp_routh_".$routh->term_id);
				$speed				= SMP_Routh::get_routh_speed($par);
				$start_hub_id		= $par['start_hub_id'];
				$finish_hub_id		= $par['finish_hub_id'];
				$capacity			= $par['capacity'];
				$cost				= $par['cost'];	
				
				//count routh step
				$batch_args			= array(
												'numberposts'	=> -1,
												'offset'    	=> 0,
												'post_type' 	=> $moveble_types,
												'post_status' 	=> 'publish',
												'fields'		=> 'ids',
												'meta_query'	=> array(
																			'relation'			=> "AND",
																			array(
																					'key'		=> 'transportation_id',
																					'value'		=> $routh->term_id
																				 ),
																			array(
																					'key'		=> 'is_permission',
																					'value'		=> 1,
																					'compaire'	=> '='
																				 )
																		 ),
											);
				$batches				= get_posts($batch_args);
				//insertLog("logistics_handler", $batches);
				foreach($batches as $batch)
				{
					$obj				= Batch::get_instance($batch);
					$gt_id				= $obj->get_meta('goods_type_id');
					$finish_time		= $obj->get_meta( "finish_time");
					$ow_id				= $obj->get_meta("owner_id");
					$ow					= SMC_Location::get_instance($ow_id);
					$ow_name			= isset($ow) ? $ow->name : sprintf(__("Owner by ID %s is not availble.", "smз"), $ow_id);
					if((int)$finish_time 		== 0)
						$finish_time			= $time;
					if($finish_time > $time)	continue;
					//insertLog("logistics_handler", $obj);					
					$count				= $obj->get_meta( 'count', true);
					$start_dislocation	= get_post_meta($start_hub_id, "dislocation_id", true);
					$finish_dislocation	= get_post_meta($finish_hub_id, "dislocation_id", true);				
					
					// меняем дислокацию товара
					$dislocation_id		= $obj->get_meta( "dislocation_id");					
					if( $start_dislocation == $dislocation_id )
					{
						$new_dislocation	= get_post_meta($finish_hub_id, "dislocation_id", true);
						$hub_id				= $start_hub_id;
						$hub_cost			= get_post_meta($start_hub_id, "cost", true);						
					}
					if( $finish_dislocation == $dislocation_id )
					{
						$new_dislocation	= get_post_meta($start_hub_id, "dislocation_id", true);
						$hub_id				= $finish_hub_id;
						$hub_cost			= get_post_meta($finish_hub_id, "cost", true);						
					}
					//TODO: pay from Hub owner
					$successfull			= apply_filters("smp_goods_batch_before_change_dislocation", true, $batch, $dislocation_id , $new_dislocation, $hub_id);
					if(is_wp_error($successfull))
					{
						$result				= "<div>" . $successfull . "</div>";
						return;
					}
					else
					{
						$new_batch_obj		= $obj->change_dislocation( $new_dislocation );
						$new_batch_id		= $new_batch_obj[0];
						$new_batch			= SMP_Goods_Batch::get_instance($new_batch_id);
						apply_filters("smp_goods_batch_change_meta", $new_batch_id, $payer_loc_id, $new_dislocation);
						
						$gt					= Goods_Type::get_instance($gt_id);
						$gt_name			= isset($gt) ? $gt->post_title : __("Goods Type not availble.[" .  $gt_id . "]", "smp");
						$dislocation		= SMC_Location::get_instance($new_dislocation);
						$loc_name			= isset($dislocation) ? $dislocation->name : __("Dislocation of Hub not availble.", "smp");
						
						$result				= "<div>" . 
						sprintf(
							__("Goods batch of %s (ID=%s) owned by %s successfully arrived at the destination Location %s through route %s. Paid fee %s.", "smp"), 
							"<b>".$gt_name."</b>", 
							$new_batch_id,
							"<b>".$ow_name."</b>",
							"<b>".$loc_name."</b>", 
							"<b>".$routh->name."</b>",
							(int)$hub_cost
							) . "</div>";
					}
					$results				.= $result;
				}			
			}
			if($results != "")
			{
				self::insert_report($results);
			}
			
		}
		static function insert_report($results)
		{			
			global $SMP_Locistics;
			$time						= time();
			$current_circle_id			= $SMP_Locistics->get_current_circle_id();
			require_once(SMP_REAL_PATH.'class/Circle_Report.php');
			if(!Circle_Report::save_global($current_circle_id, LOGISTICS_CIRCLE_TYPE, $time, $results))
			{
				insertLog("logistics_handler", "<p class='color:red; font-size:1.1em; line-height:2;'>Error Logistics: not save report in circle #%s" . $current_circle_id ."</p>");
				
			}
			$SMP_Locistics->update_current_circle_id($current_circle_id);
		}
		static function get_last_report_count()
		{
			global $wpdb;
			return $wpdb->get_row ( "SELECT MAX(report_id) AS last FROM ". $wpdb->prefix.REPORT_DB_PREFIX . " WHERE report_type=".LOGISTICS_CIRCLE_TYPE, ARRAY_A );
		}
		static function get_reports($col = 20)
		{
			global $wpdb;
			$id	= self::get_last_report_count();
			return $wpdb->get_results ( "SELECT * FROM ". $wpdb->prefix.REPORT_DB_PREFIX . " WHERE report_id>=".($id['last'] - $col)." AND report_type=".LOGISTICS_CIRCLE_TYPE ." ORDER BY report_id DESC", ARRAY_A );
		}
		
		function smc_myajax_submit($params)
		{	
			global $user_iface_color, $Soling_Metagame_Constructor, $GoodsBatch, $SMP_Currency, $start;
			$start	= getmicrotime();
			$html	= "none";
			switch($params[0])
			{
				case 'add_new_hub_permission':
					$id					= $params[1];
					$dat				= $params;
					if(!SMP_Hub::is_user_owner($id))	
					{
						break;
					}
					$location_id		= $dat[2];
					$preority			= $dat[3];
					$goods_type_id		= $dat[4];
					$limit				= $dat[5];
					$new_post_id	= wp_insert_post(array(
																			'post_name'			=> ''
																			, 'post_status' 	=> 'publish'
																			, "post_type"		=> "smp_hub_permission"
																			)
																	);
					update_post_meta($new_post_id, 'hub_id', 		$id);
					update_post_meta($new_post_id, 'owner_id', 		$location_id);
					update_post_meta($new_post_id, 'preority', 		$preority);
					update_post_meta($new_post_id, 'goods_type_id', $goods_type_id);
					update_post_meta($new_post_id, 'limit', 		$limit);
					$perm				= SMP_Locistics::get_hub_permission_form(get_post($new_post_id));
					$d					= array(	
													'add_new_hub_permission', 
													array(
															'text' 			=> array($perm, $id),
															'params' 		=> $params,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'get_hub_permissions':
					require_once('SMP_Hub_Permission.php');
					$id					= $params[1];
					if(SMP_Hub::is_user_owner($id))	
					{					
						$perm			= '						
						<div>
							<h3>'.__("My Hubs permissions", "smp").'</h3>	
							<span class="smc-alert smc_button" target_name="new_permission" func="set_current_hub_id" args="'.$id.'">'.__("add Hub permission", "smp").'</span>
						</div>';
					}
					else
					{									
						$perm			= '						
						<div>
							<h3>'.__("all Hub permissions", "smp").'</h3>	
						</div>';
					}
					$perm				.= SMP_Hub_Permission::get_hub_permissions_list($id);
					$d					= array(	
													'get_hub_permissions', 
													array(
															'text' 			=> array($perm, $id),
															'a_alert'		=> $alert,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'delete_hub_permissions':	
					$id					= $params[1];
					$hub_id				= get_post_meta($id, 'hub_id', true);
					if(!SMP_Hub::is_user_owner($hub_id))	
					{
						break;
					}
					wp_delete_post($id);
					$d					= array(	
													'delete_hub_permissions', 
													array(
															'text' 			=> $id,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'change_transit':
					$batch_id			= $params[1];
					$val				= $params[2];
					$batch				=  SMP_Goods_Batch::get_instance($batch_id);
					$routh_id			= $batch->get_meta( 'transportation_id');
					$routh				= get_term_by("id", $routh_id, "smp_routh");
					$count				= $batch->get_meta('count');
					$r					= get_option("smp_routh_$routh_id");
					
					if(SMP_Hub::is_user_owner($r['start_hub_id']) || SMP_Hub::is_user_owner($r['finish_hub_id']))
					{
						
						//проверяем свободные ёмкости
						$hub_id			= SMP_Hub::get_start_hub_of_transiting_batch($batch_id);
						$hub			= get_post($hub_id);
						$hub_name		= isset($hub) ? $hub->post_title : __("No hub availvle.", "smp");
						$free_caps		= (int)SMP_Hub::get_free_capacities($hub_id);
						
						if($free_caps < $count && $val == 1)
						{
							$d					= array(	
													'change_transit', 
													array(
															'text' 	=> array(
																				sprintf(__("No free capasities for transit. Free capabilities are %d units. Please wait for end of ending transportation", "smp"), $free_caps), 
																				$batch_id
																			),
															'time'	=> getmicrotime()  - $start
														  )
												);
							$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
							print $d_obj;
							break;
						}
						//проверяем разрешения
						require_once('SMP_Hub_Permission.php');
						$check			= SMP_Hub_Permission::is_goods_batch_has_permission($batch_id);
						if($check && $free_caps < $count)
						{														
							$val		= 1;
						}		
						if($val==1)
						{
							$speed			= (int)SMP_Routh::get_routh_speed($r) * 60;
							$time			= time();
							$finished_time	= $time + $speed;
							$batch->update_meta( 'finish_time', $finished_time);
							$results		= sprintf(
								__("Owner of hub %s give single transit permission for goods batch #%s that transit through route %s at %s. Expected time of arrival at the destination - %s", "smp"),
								 "<b>".$hub_name."</b>", 
								$batch_id,
								$routh->name,
								 "<b>".date_i18n( 'M j, Y - G:i', $time)."</b>",
								 "<b>".date_i18n( 'M j, Y - G:i', $finished_time)."</b>"
								);
						}
						else
						{
							$batch->update_meta('finish_time', -1);
							$results		= sprintf(
								__("Owner of hub %s remove single transit permission for goods batch #%s that transit through route %s at %s. Reasons figured out themselves", "smp"),
								 "<b>".$hub_name."</b>", 
								$batch_id,
								$routh->name, 
								 "<b>".date_i18n( __( 'M j, Y - G:i' ), $time)."</b>"
								);
						}
						$batch->update_meta( "is_permission", $val);
						$text			= $val;	
						self::insert_report($results);
					}
					else
					{
						$val;
						$a_alert		= __("You haven't rights to do this.");
					}
					$d					= array(	
													'change_transit', 
													array(
															'text' 			=> $val,
															"a_alert"		=> $a_alert, 
															'routh_id'		=> $routh_id,
															'time'			=> getmicrotime()  - $start,
															
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'print_batch_card':
					$batch_id			= $params[1];
					$gb					= SMP_Goods_Batch::get_instance($batch_id);
					//if($Soling_Metagame_Constructor->cur_user_is_owner(get_post_meta($batch_id, 'owner_id', true)))
					//{
					//$b				= (bool)wp_delete_post($batch_id);
					//}
					//if(!$b)				$batch_id = false;
					$d					= array(	
													'print_batch_card', 
													array(
															'text' 			=> $gb->get_stroke($gb->body, -1, array('width'=>800, 'height'=>300, 'is_price'=>false)),
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "smp_complete_move_batch":
					$bid				= $params[1];
					$rid				= $params[3];
					$batch				= SMP_Goods_Batch::get_instance($bid);
					$routh_id			= $batch->get_meta("transportation_id");
					$location_id		= $batch->get_meta("dislocation_id");
					if($routh_id>0)
					{
						$routh			= get_term_by("id", $routh_id, "smp_routh");
						$html			= sprintf(__("Batch # %s continue moved by %s. Please wait.", "smp"), $bid, "<B>".$routh->name."</b>");
						$complete		= false;
					}
					else
					{
						$html			= sprintf(__("Moved batch #%s to %s!", "smp"), $bid, "<b>". SMC_Location::get_instance($location_id)->name ."</b>");
						$is_owner_text	= $batch->user_is_owner() ? "<div style='height:100px;'><table><tr><td>" . $batch->get_widget_picto(0, false, array("your_bottom"=>0, "your_right"=>-50, "size"=>80)) . "</td><td>".$html. "</td></tr></table></div>" : null;
						$complete		= true;
						$routhes		=  $this->smc_map_external_0("", 0);
					}
					$d					= array(	
													$params[0], 
													array(
															'text' 			=> $html,
															'complete'		=> $complete,
															'bid'			=> $bid,
															'lid'			=> $location_id,
															'platoon_id'	=> (int)get_post_meta($bid, "platoon_id", true),
															"routes"		=>	$routhes,
															"is_owner"		=> $is_owner_text,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'get_location_hubs':
					$hubs				= $params[1]==0 ? SMP_Hub::get_all_hubs(): SMP_Hub::get_all_hubs_by_dislocation($params[1]);
					foreach($hubs as $hub)
					{
						$ht			.= SMP_Hub::get_short_form($hub);
					}
					/**/
					if(count($hubs)==0)
						$ht			.= "<div>".__("There are no hubs in this Location.", "smp")."</div>";
					$html				.= '<div style="display:inline-block;; position:absolute; top:1px; left:10px; width:100%; height:100%;  color:white; font-size:13px;">
						<h3 style="color:white;">'.__("all Hubs",'smp').': </h3>'.
						$ht.
					'</div>';
					
					$d					= array(	
													'get_location_hubs', 
													array(
															'text' 			=> $html,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'get_current_location_map':
					$datas				= $Soling_Metagame_Constructor->set_location_map($params[1]);
					$d					= array(	
													'get_current_location_map', 
													array(
															'text' 			=> $datas[0],
															'exec' 			=> 'post_draw',
															'args' 			=> array($datas[1], $Soling_Metagame_Constructor->options['map_titles_coords']),
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'get_current_location_factories':
					$facs				= $params[1]==0 ? Factories::get_all_factories() :  Factories::get_all_location_factories($params[1]);
					foreach($facs as $fac)
					{
						$ht			.= Factories::get_short_form($fac);
					}
					if(count($facs)==0)
						$ht			.= "<div >".__("There are no factories in this Location.", "smp")."</div>";
					$html				.= '<div class="location-tamplate-content" style="display:inline-block;; position:absolute; top:1px; left:10px; width:100%; height:100%;  color:white; font-size:13px;">'.
						'<h3 style="color:white;">'.__("all Factories",'smp').': </h3>'.
						'<ul>'.$ht.'</ul>'.
					'</div>';
					$d					= array(	
													'get_current_location_factories', 
													array(
															'text' 			=> $html,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "smp_lgstcs_get_circle_report":
					$html	= "<h1>" . __("Last logistics reports", "smp") . "</h1>";
					$pers	= self::get_reports(20);
					if(!count($pers))
					{
						$html		.= "<div class='smp-comment' style='width:98%;'>". __("There are no present report", "smp")."</div>";
					}
					else
					{	
						foreach($pers as $rep)
						{
							$html	.= "<div><b>". $rep['finish_time']."</b><br>".$rep["content"]."<hr></div>";
						}
					}
					$d					= array(	
													$params[0], 
													array(
															'text' 			=> $html,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "get_hub_form":
					global $hubs_rouths;
					$hub_id				= $params[1][0];
					$hub				= get_post($hub_id);
					$i					= $params[1][1];
					$form				= "<div class='smp-pr-main' id='production-".$hub_id."' button_id='".$i."' factory_id='".$hub_id."' style='display:block!important;'>";
					$form				.= '<h3>'.$hub->post_title.'</h3>';
					$form				.= SMP_Hub::get_form($hub_id);					
					$form				.= "</div>";
					$d					= array(	
													$params[0], 
													array(
															'text' 			=> $form,
															"id"			=> $i, 
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
			}
		}
	}
	
?>