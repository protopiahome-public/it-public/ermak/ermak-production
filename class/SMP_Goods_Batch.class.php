<?php 
/*
	this final class? that moved functional of ald classes - Goods_batch.php and Batch.php
	under construction!!!
*/
	class SMP_Goods_Batch extends Batch
	{
		static function init()
		{
			add_action('init', 										array(__CLASS__, 	'add_Goods_batch'), 11 );
			add_action('admin_menu',								array(__CLASS__, 	'my_extra_fields_goods_batch'));
			add_action('save_post_goods_batch',						array(__CLASS__, 	'true_save_box_data'));
			add_filter('manage_edit-goods_batch_columns',		 	array(__CLASS__,	'add_views_column'), 4);
			add_filter('manage_edit-goods_batch_sortable_columns', 	array(__CLASS__,	'add_views_sortable_column'));
			add_filter('manage_goods_batch_posts_custom_column', 	array(__CLASS__,	'fill_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
			add_action('admin_head',								array(__CLASS__,	'add_column_css'));
			add_filter('pre_get_posts',								array(__CLASS__,	'add_column_views_request'));
	
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		static function get_type()
		{
			return GOODS_BATCH_NAME;
		}
		function get_class_name()
		{
			return "SMP_Goods_Batch";
		}
		static function add_Goods_batch()
		{
				$labels = array(
					'name' => __('Goods batch', "smp"),
					'singular_name' => __("Goods batch", "smp"), // ����� ������ ��������->�������
					'add_new' => __("Add Goods batch", "smp"),
					'add_new_item' => __("add new Goods batch", "smp"), // ��������� ���� <title>
					'edit_item' => __("Edit Goods batch", "smp"),
					'new_item' => __("new Goods batch", "smp"),
					'all_items' => __("all Goods batches", "smp"),
					'view_item' => __("view Goods batch", "smp"),
					'search_items' => __("Search Goods batch", "smp"),
					'not_found' =>  __("Goods batch not found", "smp"),
					'not_found_in_trash' => __("no found Goods batch in trash", "smp"),
					'menu_name' => __("Goods batch", "smp") // ������ � ���� � �������
				);
				$args = array(
					'labels' => $labels,
					'public' => true,
					'show_ui' => true, // ���������� ��������� � �������
					'has_archive' => true, 
					'exclude_from_search' => true,
					'menu_position' => 21, // ������� � ����
					'show_in_menu' => "Metagame_Production_page",
					'supports' => array(  'title', 'thumbnail')
					//,'capabilities' => 'manage_options'
					,'capability_type' => 'post'
				);
				register_post_type(GOODS_BATCH_NAME, $args);
		}
		
		// ����-���� � ��������
		
		static function my_extra_fields_goods_batch() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(__CLASS__, 'extra_fields_box_func'), GOODS_BATCH_NAME, 'normal', 'high'  );
		}
		static function extra_fields_box_func( $post )
		{	
			global $Soling_Metagame_Constructor;
			$arg				= array(										
										'numberposts'	=> 100,
										'offset'    	=> 0,
										'orderby'  		=> 'titile',
										'order'     	=> 'ASC',
										'post_type' 	=> 'factory',
										'post_status' 	=> 'publish'
									);
			$factory		= get_posts($arg);
			$ar				= array(										
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'titile',
										'order'     	=> 'ASC',
										'post_type' 	=> 'goods_type',
										'post_status' 	=> 'publish'
									);
			$goods			= get_posts($ar);
			
			$owners			= get_terms(SMC_LOCATION_NAME, array("number"=>0, 'orderby'=>'name', "hide_empty"=>false));
			?>
			<div style='display:inline-block;'>
				<div class="smp_batch_extra_field_column">
					<div class="h">
						<label for="factory_id"><?php echo __("Manufacturer", "smp");?></label><br>
						<select name="factory_id" class='h2'>
						<?php
						foreach($factory as $fac)
						{
							echo "<option value='" . $fac->ID . "' " . selected($fac->ID, get_post_meta($post->ID, "factory_id", true)) . " >". $fac->ID . ". " . $fac->post_title. "</option>";
						}
						?>
						</select>
					</div>
					<div class="h">
						<label for="dislocation_id"><?php echo __("Dislocation", "smp")?></label><br>
						<?php echo $Soling_Metagame_Constructor->get_location_selecter(array( 'class'=>'h2', "name"=>"dislocation_id", 'selected'=>get_post_meta($post->ID, "dislocation_id", true))); ?>				
					</div>
					<div class="h">
						<label for="owner_id"><?php echo __("Owner", "smp");?></label><br>
						<?php echo $Soling_Metagame_Constructor->get_location_selecter(array('class'=>'h2', "name"=>"owner_id", 'selected'=>get_post_meta($post->ID, "owner_id", true))); ?>
					</div>
					<div class="h">
						<label for="goods_type_id"><?php echo  __("Goods type", "smp");?></label><br>
						<select name="goods_type_id" class='h2'>
						<?php
						foreach($goods as $good)
						{
							echo "<option value='" . $good->ID . "' " . selected($good->ID, get_post_meta($post->ID, "goods_type_id", true)) . " >".$good->ID. ". " . $good->post_title. "</option>";
						}
						?>
						</select>
					</div>				
					<div class='h'>
						<?php $qual		= get_post_meta( $post->ID, "quality", true ); ?>
						<label  class="h2" for="quality"><?php _e("Quality", "smp"); ?></label><br>
						<input  class="h2" name="quality" id='quality' value="<?php print_r( !$qual? 50 : $qual);?>"/>
					</div>
					<div class="h">
						<label for="count"><?php _e("Count", "smp");?></label><br>
						<input  class="h2" name="count" type="number" min="0" step="1" value ="<?php print_r( get_post_meta($post->ID, "count", true));?>"/>
					</div>
					<div class="h">
						<label for="best_before"><?php _e("Best before", "smp");?></label><br>
						<input  class="h2" name="best_before" type="number" step="1" value ="<?php print_r( get_post_meta($post->ID, "best_before", true));?>"/>
					</div>
					<div class="h">
						<label for="price"><?php _e("Price in Store", "smp");?></label><br>
						<input  class="h2" name="price" type="number" min="0" step="1" value ="<?php print_r( get_post_meta($post->ID, "price", true));?>"/>
					</div>
					<div class="h">
						<input  id="store" name="store" class="css-checkbox" type="checkbox" <?php echo ( get_post_meta($post->ID, "store", true)  == '1' ? 'checked="checked"' : ''); ?> value="1"/>
						<label for="store" class="css-label"><?php _e("Sale", "smp");?></label><br> 
					</div> 
					<div class="h">
						<input  id="is_blocked" name="is_blocked" class="css-checkbox" type="checkbox" <?php echo ( get_post_meta($post->ID, "is_blocked", true)  == '1' ? 'checked="checked"' : ''); ?> value="1"/>
						<label for="is_blocked" class="css-label"><?php _e("is blocked", "smp");?></label><br> 
					</div> 
				</div>
				<div class="smp_batch_extra_field_column">
					
						<?php echo apply_filters("smp_batch_extra_fields",  "", $post->ID);?>
						<?php echo apply_filters("smp_batch_extra_fields2", "", $post->ID);?>				
						<?php echo apply_filters("smp_batch_extra_fields3", "", $post->ID);?>					
				</div>
			</div>
				<script type="text/javascript">
					jQuery(function($)
					{					
						$("#quality").ionRangeSlider({
								min: 0,
								max: 100, 			
								type: 'single',
								step: 5,
								postfix: "%",
								prettify: true,
								hasGrid: true
						});
					});
				</script>
			<?php
			wp_nonce_field( basename( __FILE__ ), 'goods_batch_metabox_nonce' );
		}
		
		
		static function true_save_box_data ( $post_id) 
		{					
			global $table_prefix;
			// ���������, ������ �� ������ �� �������� � ����������
			if ( !isset( $_POST['goods_batch_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['goods_batch_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// ���������, �������� �� ������ ���������������
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// ���������, ����� ������������, ����� �� �� ������������� ������
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;		
				
			update_post_meta($post_id, 'factory_id', 	(int)$_POST['factory_id']);
			update_post_meta($post_id, 'dislocation_id',(int)$_POST['dislocation_id']);
			update_post_meta($post_id, 'owner_id',		(int)$_POST['owner_id']);
			update_post_meta($post_id, 'goods_type_id',	(int)$_POST['goods_type_id']);
			update_post_meta($post_id, 'store',			(int)$_POST['store']);
			update_post_meta($post_id, 'count', 		(int)$_POST['count']);
			update_post_meta($post_id, 'price', 		(int)$_POST['price']);
			update_post_meta($post_id, 'quality', 		(int)$_POST['quality']);
			update_post_meta($post_id, 'best_before',	(int)$_POST['best_before']);
			update_post_meta($post_id, 'is_blocked',	(int)$_POST['is_blocked']);
			
			if($_POST['post_title']=="")
			{
				$dd							= get_post ($_POST['factory_id']);
				$dislocation				= $dd->post_title;
				$ow							= get_post ($_POST['goods_type_id']);
				$goods						= $ow->post_title;
				$post->post_title			= $dislocation." (". $goods . ")";	
				
				global $wpdb;
				$wpdb->update(
								$table_prefix."posts",
								array("post_title" => $post->post_title),
								array("ID" => $post_id)
							);
			}	
			
			//find
			$f		= Batch::find_good_type(
												$_POST['goods_type_id'],
												$_POST['factory_id'], 
												$_POST['dislocation_id'],
												$_POST['owner_id'],
												$_POST['store']
											);
			if($f)
			{
				$b	= new Batch($post_id);
				$b->add_goods(get_post_meta($f->id));
				wp_delete_post($f->id, true);
			}
			/**/
			return $post_id;
		}
		
		static function add_views_column( $columns ){
		
			$posts_columns = array(
				  "cb" 				=> " ",
				  "IDs"	 			=> __("ID", 'smp'),
				  "doo" 			=> __(""),				  
				  "goods_type"		=> "<i class='fa fa-gift fa-2x hinter' data-hint='" . __("Goods type", 'smp') . "' height=20></i>",
				  "best_before"		=> "<i class='fa fa-calendar-o fa-2x hinter' data-hint='" . __("Best before", 'smp') . "' height=20></i>",
				  "Manufacturer"	=> __("Manufacturer", 'smp'),
				  "owner"	 		=> __("Owner", 'smp'),
				  "dislocation"	 	=> "<i class='fa fa-map-marker fa-2x hinter' data-hint='" . __("Dislocation", 'smp') . "' height=20></i>",
				  "count"		 	=> "<i class='fa fa-bars fa-2x hinter' data-hint='" . __("Count", 'smp') . "' height=20></i>",
				  "price"			=> __("Price", 'smp'),
				  "store"			=> "<i class='fa fa-shopping-cart fa-2x hinter' data-hint='" . __("Sale", 'smp') . "' height=20></i>",
				  "is_blocked"		=> '<i class="fa fa-lock fa-2x hinter" data-hint="'.__("is blocked", "smp").'" height=20></i>',
			   );
			return $posts_columns;			
		}
		
		// ��������� ����������� ����������� �������
		static function add_views_sortable_column($sortable_columns){
			$sortable_columns['doo'] 			= 'doo';
			$sortable_columns['count'] 			= 'count';				
			$sortable_columns['best_before'] 	= 'best_before';			
			$sortable_columns['goods_type'] 	= 'goods_type';			
			$sortable_columns['price'] 			= 'price';			
			$sortable_columns['IDs'] 			= 'IDs';			
			$sortable_columns['owner'] 			= 'owner';			
			$sortable_columns['Manufacturer'] 	= 'Manufacturer';	
			$sortable_columns['dislocation'] 	= 'dislocation';			
			$sortable_columns['store'] 			= 'store';			
			$sortable_columns['is_blocked'] 	= 'is_blocked';			
			return $sortable_columns;
		}	
		
		// ��������� ������� �������	
		static function fill_views_column($column_name, $post_id)
		{
			//$post			= get_post($post_id);
			switch( $column_name) 
			{				
				case 'doo':
					echo '
					<a class="button pbutton" href = "/wp-admin/post.php?post=' . $post_id. '&action=edit" class="button-smp" title="'.__("Edit").'"><i class="fa fa-gear"></i></a><BR>
					<a class="button pbutton" href = "/wp-admin/post.php?post=' . $post_id. '&action=trash" class="button-smp" title="'.__("Delete").'"><i class="fa fa-remove"></i></a><BR>
					<a class="button pbutton" href = "/?goods_batch=' . $post->post_title. '" class="button-smp" title="'.__("Goto").'"><i class="fa fa-eye"></i></a>';
					break;			
				case 'IDs':
					echo "<br>
					<div class='ids'><span>ID</span>".$post_id. "</div>
					<p>
					<div class='button' add_bid='$post_id' post_type='SMP_Goods_Batch'>" . __("Double", "smc") . "</div>";
					break;	
				case 'owner':
					$fct	=  get_term_by("id", get_post_meta( $post_id, 'owner_id', true ), "location");
					echo $fct->name ."<br><div class='ids'><span>ID</span>".$fct->term_id."</div>";
					break;	
				case 'Manufacturer':
					$fct	=  get_post(get_post_meta( $post_id, 'factory_id', true ));
					echo $fct->post_title ."<br><div class='ids'><span>ID</span>".$fct->ID."</div>";
					break;	
				case 'best_before':
					$infinitely = get_post_meta( $post_id, "best_before", true );
					echo $infinitely > MAX_BEST_BEFORE ? "<B>".__("infinitely", "smp"). "</B><BR>". $infinitely : $infinitely. " " . __("circles", "smp");
					break;	
				case 'count':
					echo get_post_meta( $post_id, "count", true );
					break;	
				case 'price':
					echo get_post_meta( $post_id, "price", true );
					break;			
				case 'goods_type':
					$gtt	= get_post_meta( $post_id, 'goods_type_id', true );
					$gt		= Goods_Type::get_trumbnail($gtt,  56). "<br><div class='ids'><span>ID</span>" . $gtt."</div>"; 
					echo $gt;//->post_title;
					break;		
				case 'dislocation':
					$gt		= get_term_by("id", get_post_meta( $post_id, 'dislocation_id', true ), "location");
					echo $gt->name. "<br><div class='ids'><span>ID</span>".$gt->term_id."</div>";
					break;
				case 'store':
					$gt	= get_post_meta( $post_id, 'store', true );
					echo $gt ? '<img src="'.SMP_URLPATH.'img/check_checked.png">' : '<img src="'.SMP_URLPATH.'img/check_unchecked.png">';
					//echo $gt==1 ? "<i class='fa fa-coffee fa-3x'></i>" : " -- ";
					break;
				case 'is_blocked':
					$gt	= get_post_meta( $post_id, 'is_blocked', true );
					echo $gt ? '<img src="'.SMP_URLPATH.'img/check_checked.png">' : '<img src="'.SMP_URLPATH.'img/check_unchecked.png">';
					//echo $gt==1 ? "<i class='fa fa-coffee fa-3x'></i>" : " -- ";
					break;
			}		
		}
		
		// �������� ������ ��� ���������� �������	
		static function add_column_views_request( $object )
		{
			if($object->get('post_type') != GOODS_BATCH_NAME) return $object;
			switch( $object->get('orderby'))
			{
				case 'owner_id':
					$object->set('meta_key', 'owner_id');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'count':
					$object->set('meta_key', 'count');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'best_before':
					$object->set('meta_key', 'best_before');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'price':
					$object->set('meta_key', 'price');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'dislocation':
					$object->set('meta_key', 'dislocation');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'Manufacturer':
					$object->set('meta_key', 'Manufacturer');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'IDs':
					$object->set('meta_key', 'IDs');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'store':
					$object->set('meta_key', 'store');
					$object->set('orderby', 'meta_value_num');
					break;
				
			}
			return  $object;
		}	
		// ��������� ������ ������� ����� css
		static function add_column_css()
		{
			echo '
			<style type="text/css">
				.column-IDs{width:110px;height:60px;} 
				.column-count{max-width:100px;} 
				.type-goods_batch .column-title{width:20px;}
			</style>';
		}
		
		static function add_DB()
		{
			global $wpdb;
			$wpdb->query(
				"CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "smp_goods_batch_meta` (
				  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
				  `factory_id` bigint(20) unsigned NOT NULL DEFAULT '0',
				  `dislocation_id` bigint(20) unsigned NOT NULL DEFAULT '0',
				  `owner_id` bigint(20) unsigned NOT NULL DEFAULT '0',
				  `goods_type_id` bigint(20) unsigned NOT NULL DEFAULT '0',
				  `is_blocked` TINYINT(1) unsigned NOT NULL DEFAULT '0',
				  `store` TINYINT(1) unsigned NOT NULL DEFAULT '0',
				  `count` bigint(20) unsigned NOT NULL DEFAULT '1',
				  `quality` int(5)  unsigned NOT NULL DEFAULT '50',    
				  `best_before` int(10)  unsigned NOT NULL DEFAULT '0',    
				  `currency_type_id` bigint(20)  unsigned NOT NULL DEFAULT '0',    
				  `waybill_tact` bigint(20)  unsigned NOT NULL DEFAULT '0',    
				  `is_permission` TINYINT(1)  unsigned NOT NULL DEFAULT '0',  
				  `transportation_id` bigint(20)  unsigned NOT NULL DEFAULT '0',      
				  PRIMARY KEY (`ID`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
			);
		}
		static function install()
		{
			self:: add_DB();
			$my_post = array(
								  'post_title'   		=> __("My Goods Batches", "smp"),
								  'post_type' 			=> 'page',
								  'post_content' 		=> "[my_storage_page]"  ,
								  'post_status'  		=> 'publish',
								   'comment_status'		=> 'closed',
								);

			return	wp_insert_post( $my_post );
		}
		static function add_properties($names, $types, $defaults)
		{	
			global $wpdb;
			if(!is_array($names))			$names 		= array($names);
			if(!is_array($types))			$types 		= array($types);			
			if(!is_array($defaults))		$defaults 	= array($defaults);			
			$ct								= count($types);
			$cn								= count($names);
			$cd								= count($defaults);
			if($cn > $ct) 					$types		= array_merge( $types,		array_fill(0, $cn-$ct,  $types[0]));
			if($cn > $cd) 					$defaults	= array_merge( $defaults,	array_fill(0, $cn-$cd,  $defaults[0]));
			$query							= "ALTER IGNORE TABLE `" . $wpdb->prefix . "smp_goods_batch_meta` ADD COLUMN (";
			$values							= array();
			$i=0;
			foreach($names as $nm)
			{
				$values[]					= $nm . " " . $types[$i] . " NOT NULL DEFAULT " . $defaults[$i];
				$i++;
			}
			$query							.= implode(", ", $values). ");";
			//insertLog( "SMP_Goods_Batch::add_properties", $query );
			return $wpdb->query($query);
		}
		static function serverside_service($all_goods_batchses)
		{
			$comm_visible		= "none";
			$serv				= "";
			//$html				=  count($all_goods_batchses)."<BR>";
			if(isset($_POST))
			{
				foreach($all_goods_batchses as $goods_batch) 
				{
					$gb	= self::get_instance($goods_batch->ID);
					if($_POST['save_change'.$goods_batch->ID.''])
					{	
						if(get_current_user_id() != $gb->user_is_owner()) continue;
						// �������� ����� ������ ������ �������
						if($_POST['location_'.$goods_batch->ID]>0 && $_POST['transfer_to_location_'.$goods_batch->ID]>0)
						{
							echo $_POST['transfer_to_location_'.$goods_batch->ID]."<BR>";							
							echo 'id = '.$goods_batch->ID."<BR>";							
							$n		= Batch::rize_batch_owner(
																new Batch($goods_batch->ID),
																$_POST['location_'.$goods_batch->ID],
																$_POST['transfer_to_location_'.$goods_batch->ID]													
															);
							$new_batch		= $n[0];
							$new_count		= $n[1];
							$serv			.= __("Change owner", "smp")." " .$new_count." ".__("unit", "smp") . "<BR>";
							
						}						
					}
					if($_POST["to_store".$goods_batch->ID])
					{
						if(get_current_user_id() != $gb->user_is_owner()) continue;
						if($_POST['sale_to_location_'.$goods_batch->ID]>0)
						{
							$n		= Batch::rize_batch_store(
																new Batch($goods_batch->ID),
																$_POST['sale_to_location_'.$goods_batch->ID],
																!get_post_meta($goods_batch->ID, "store", true)
															 );
							$new_batch		= $n[0];
							$new_count		= $n[1];
							$serv			.= "<BR>".__("Change store", "smp")." " .$new_count." ".__("unit", "smp") . "<BR>";
						}
					}
					if($_POST['delete_'.$goods_batch->ID])
					{
						if(get_current_user_id() != $gb->user_is_owner()) continue;
						if($_POST['delete_to_location_'.$goods_batch->ID]>0)
						{
							$batch				= new Batch($goods_batch->ID);
							$batch->rize_batch_delete($_POST['delete_to_location_'.$goods_batch->ID]);
						}
					}
					if($_POST['change_price_'.$goods_batch->ID])
					{
						if(get_current_user_id() != $gb->user_is_owner()) continue;
						if($_POST['new_price_'.$goods_batch->ID]>0)
						{
							update_post_meta( $goods_batch->ID, "price", (int)$_POST['new_price_'.$goods_batch->ID]);
						}
					}
					do_action("smp_send_batch_changes", $goods_batch->ID);
				}	
			}
			if($serv !="")	
			{
				$comm_visible	= "display";			
			}
			
			//var_dump($_POST);
			
			//echo 'page_id = '.$_GET['page_id'];
			
			
			
			
			
			$html				.= "<div class='lp-comment_server' style='display:".$comm_visible.";'>".$serv."
									</div>";
			
			return $html;
		}
		function get_paying_form()
		{
			$price				= $this->get_meta("price");
			$count				= $this->get_meta("count");
			$ctid				= $this->get_meta("currency_type_id");
			$store				= $this->get_meta("store");
			$ct					= SMP_Currency_Type::get_instance($ctid);
			$id					= $this->id;
			$html				= "
			<div id='pay_gb_form' gbid='" . $this->id . "'>
				<div>
					<input type='checkbox' class='css-checkbox1' id='pay_form_$id' " . checked(1, $store, 0) . " isstoregb  bid='$id' />
					<label class='css-label1' for='pay_form_$id'>" . __("Send to Store", "smp"). "</label>
				</div>
				<!--input type='radio' id='is_store_0' name='is_tore' class='css-checkbox1' value='0' " . checked(0, $store, 0) . "/>
				<label class='css-label1' for='is_store_0'>" . __("Remove from Store", "smp"). "</label>
				<br>
				<input type='radio' id='is_store_1' name='is_tore' class='css-checkbox1' value='1' " . checked(1, $store, 0) . "/>
				<label class='css-label1' for='is_store_1'>" . __("Send to Store", "smp"). "</label-->
				<div style='display:block; padding:10px;'></div>
				<label>". __("Select count of units for sale","smp")."</label><br>
				<input type='range'  min='0' max='$count' name='price_range' value='$count' style='width:200px;' ttg='price' class='smc_vertical_align' id='sale_to_location_$id' inbgc bid='$id'/>
				<input type='number' min='0' name='price' value='$count' style='width:100px;' ttg='price_range' class='smc_vertical_align'/>
				<div style='display:block; padding:10px;'></div> 
				<label for='new_price_$id'>".__("Set price for one unit of this batch in Store","smp")."</label><br>
				<input  price_gb_store  bid='$id' id='new_price_$id' name='new_price_$id' type='number' min='0' value='".$price."' style='width:200px;'/>
				<div style='display:block; padding:10px;'></div>
				<label>".__("Change Currency Type", "smp")."</label>
				<label class='smc_input'>".
					SMP_Currency_Type::wp_dropdown(array("id"=>"currency_type_id", "class"=>"h2", 'selected'=>$ctid)).
				"</label>
				<div style='display:block; padding:10px;'></div>
				<div class='button smc_padding' gbbt='to_store' bid='$id'>".__("Change")."</div>
			</div>";
			return $html;
		}
	}
?>