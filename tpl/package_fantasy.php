<?php
	//function get_fantasy_production_package()
	{
		// goods types
		$goods_types	= array();
		$gt_data		= array(	
									array(
											"post_title"		=> "�������� ������",
											"post_name"			=> "swampiron",
											"post_excerpt"		=> "",
											"industry"			=> "167",
											"complexity"		=> "1",
											"usage_duration"	=> "1000",
											"color"				=> "FF9A42",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "swampiron-copy.jpg",
										  ),	
									array(
											"post_title"		=> "�����",
											"post_name"			=> "bron",
											"post_excerpt"		=> "",
											"industry"			=> "162",
											"complexity"		=> "10",
											"usage_duration"	=> "1000",
											"color"				=> "703885",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "������� �����",
											"post_name"			=> "bomb",
											"post_excerpt"		=> "",
											"industry"			=> "162",
											"complexity"		=> "10",
											"usage_duration"	=> "1000",
											"color"				=> "08A642",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "�����",
											"post_name"			=> "mushrooms",
											"post_excerpt"		=> "",
											"industry"			=> "164",
											"complexity"		=> "1",
											"usage_duration"	=> "1000",
											"color"				=> "D62222",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "����������� ������",
											"post_name"			=> "gem",
											"post_excerpt"		=> "",
											"industry"			=> "167",
											"complexity"		=> "5",
											"usage_duration"	=> "1000",
											"color"				=> "1994FF",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "���������",
											"post_name"			=> "wood",
											"post_excerpt"		=> "",
											"industry"			=> "167",
											"complexity"		=> "1",
											"usage_duration"	=> "1000",
											"color"				=> "8F7135",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																							array('goods_type' => '', 'goods_value' => '')
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "������",
											"post_name"			=> "club",
											"post_excerpt"		=> "",
											"industry"			=> "162",
											"complexity"		=> "5",
											"usage_duration"	=> "1000",
											"color"				=> "856240",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "����� � �������",
											"post_name"			=> "potion",
											"post_excerpt"		=> "",
											"industry"			=> "161",
											"complexity"		=> "10",
											"usage_duration"	=> "1000",
											"color"				=> "FF870F",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "���������",
											"post_name"			=> "isvest",
											"post_excerpt"		=> "",
											"industry"			=> "167",
											"complexity"		=> "1",
											"usage_duration"	=> "1000",
											"color"				=> "B8B379",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																							array('goods_type' => '', 'goods_value' => '')
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "����������",
											"post_name"			=> "cata",
											"post_excerpt"		=> "",
											"industry"			=> "162",
											"complexity"		=> "10",
											"usage_duration"	=> "1000",
											"color"				=> "A35F1A",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "��������� �����",
											"post_name"			=> "kvartz_pesok",
											"post_excerpt"		=> "",
											"industry"			=> "167",
											"complexity"		=> "1",
											"usage_duration"	=> "1000",
											"color"				=> "D6CD7E",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																							array('goods_type' => '', 'goods_value' => '')
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "����",
											"post_name"			=> "cocs",
											"post_excerpt"		=> "",
											"industry"			=> "161",
											"complexity"		=> "1",
											"usage_duration"	=> "1000",
											"color"				=> "474747",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "������ ������",
											"post_name"			=> "orcs_poison",
											"post_excerpt"		=> "",
											"industry"			=> "164",
											"complexity"		=> "3",
											"usage_duration"	=> "1000",
											"color"				=> "34ED3A",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "����������� �����",
											"post_name"			=> "poison",
											"post_excerpt"		=> "",
											"industry"			=> "162",
											"complexity"		=> "10",
											"usage_duration"	=> "1000",
											"color"				=> "81B2B5",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "��������� ������",
											"post_name"			=> "iron",
											"post_excerpt"		=> "",
											"industry"			=> "167",
											"complexity"		=> "5",
											"usage_duration"	=> "1000",
											"color"				=> "9FB7C2",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "�������",
											"post_name"			=> "web",
											"post_excerpt"		=> "",
											"industry"			=> "163",
											"complexity"		=> "3",
											"usage_duration"	=> "1000",
											"color"				=> "BFB769",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "����",
											"post_name"			=> "plow",
											"post_excerpt"		=> "",
											"industry"			=> "162",
											"complexity"		=> "10",
											"usage_duration"	=> "1000",
											"color"				=> "E926FF",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "70",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "�����",
											"post_name"			=> "powder",
											"post_excerpt"		=> "",
											"industry"			=> "161",
											"complexity"		=> "3",
											"usage_duration"	=> "1000",
											"color"				=> "654299",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "�������",
											"post_name"			=> "selitre",
											"post_excerpt"		=> "",
											"industry"			=> "161",
											"complexity"		=> "3",
											"usage_duration"	=> "1000",
											"color"				=> "496344",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "selitre-copy.jpg",
										  ),	
									array(
											"post_title"		=> "����",
											"post_name"			=> "sera",
											"post_excerpt"		=> "",
											"industry"			=> "167",
											"complexity"		=> "1",
											"usage_duration"	=> "10000",
											"color"				=> "E1E810",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "����",
											"post_name"			=> "set",
											"post_excerpt"		=> "",
											"industry"			=> "163",
											"complexity"		=> "3",
											"usage_duration"	=> "1000",
											"color"				=> "ADADAD",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "������",
											"post_name"			=> "glass",
											"post_excerpt"		=> "",
											"industry"			=> "163",
											"complexity"		=> "3",
											"usage_duration"	=> "1000",
											"color"				=> "82C2CC",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "�����",
											"post_name"			=> "axe",
											"post_excerpt"		=> "",
											"industry"			=> "162",
											"complexity"		=> "5",
											"usage_duration"	=> "1000",
											"color"				=> "FFC163",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "����",
											"post_name"			=> "torf",
											"post_excerpt"		=> "",
											"industry"			=> "167",
											"complexity"		=> "1",
											"usage_duration"	=> "1000",
											"color"				=> "453014",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "torf-copy.jpg",
										  ),	
									array(
											"post_title"		=> "�����",
											"post_name"			=> "charcoal",
											"post_excerpt"		=> "",
											"industry"			=> "167",
											"complexity"		=> "1",
											"usage_duration"	=> "1000",
											"color"				=> "5C4B3E",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),	
									array(
											"post_title"		=> "��",
											"post_name"			=> "spider_poison",
											"post_excerpt"		=> "",
											"industry"			=> "164",
											"complexity"		=> "1",
											"usage_duration"	=> "1000",
											"color"				=> "5C8559",
											"quality_multiplier"=> "0",
											"powerfull_multiplier"=> "0",
											"capacity_multiplier"=> "0",
											"speed_multiplier"=> "0",
											"cost_multiplier"=> "0",
											"goods_type_equipment"=> "",
											"components"		=> array(
																						),
											"thumbnail"			=> "",
										  ),
													);
		foreach($gt_data as $gt)
		{
			$gt_ID 					= wp_insert_post(array(
																			  "comment_status"	=>"closed"
																			, "post_name"		=> $gt["post_name"]
																			, "post_status" 	=> "publish"
																			, "post_title"		=> $gt["post_title"]
																			, "post_excerpt"	=> $gt["post_excerpt"]
																			, "post_type"		=>"goods_type"
																			)
																	);
			add_post_meta($gt_ID, "industry", 		$gt["industry"]);
			add_post_meta($gt_ID, "description", 	$gt["post_excerpt"]);
			add_post_meta($gt_ID, "complexity", 	$gt["complexity"]);
			add_post_meta($gt_ID, "usage_duration", $gt["usage_duration"]);
			add_post_meta($gt_ID, "color", 			$gt["color"]);
			add_post_meta($gt_ID, "quality_multiplier",$gt["quality_multiplier"]);
			add_post_meta($gt_ID, "powerfull_multiplier",$gt["powerfull_multiplier"]);
			add_post_meta($gt_ID, "capacity_multiplier",$gt["capacity_multiplier"]);
			add_post_meta($gt_ID, "cost_multiplier",$gt["cost_multiplier"]);
			
			
			//components
			$components		= array();
			foreach($gt["components"][0] as $gtt)
			{			
				$components[] = array("goods_type" => $goods_types[$gtt["goods_type"]], "goods_value"=>$gtt["goods_value"]);
			}
			add_post_meta($gt_ID, "components", 			array($components));
			//insert trumbail				
			$wp_upload_dir 	= wp_upload_dir();				
			$filename		= download_url(SMC_DRAGON_URLPATH."" . "img/".$gt["post_name"].".jpg");		
			$wp_filetype 	= wp_check_filetype(basename($filename), null );
			cyfu_publish_post($gt_ID, $filename);
			$goods_types[$gt["post_title"]]			= $gt_ID;
			set_time_limit (20);
		}
		foreach($gt_data as $gt)
		{
			$fff			= $goods_types[$gt["goods_type_equipment"]];
			if($fff=="")	$fff = -1;
			add_post_meta($gt_ID, "goods_type_equipment", $fff);
		}
	}
?>