<?php
	function my_storage_page()
	{
		if(!is_user_logged_in())
		{
			return "<div class='smp-comment'>".__("You must logged in!", 'smp')."<BR>
			<a href='".wp_login_url( home_url())."' title='Login'>".__('Login', 'smc')."</a></div>";
		}
		
		return Goods_Batch::get_all_batchs_switcher(-1, true, "mine_gb");
		
		/*
		global $user_iface_color,$current_goods_type, $GoodsBatch, $Soling_Metagame_Constructor, $all_goods_types;
		$all_goods_types	= Goods_Type::get_global();
		$all_locations1		= get_terms(SMC_LOCATION_NAME, array("number"=>0, 'orderby'=>'name', "hide_empty"=>false));
		$all_locations		= array();
		$all_loc_types		= array();
		foreach($all_locations1 as $location)
		{
			if( $Soling_Metagame_Constructor->cur_user_is_owner($location->term_id))
			{
				$all_locations[]	= $location->term_id;
				$all_loc_types[]	= $Soling_Metagame_Constructor->get_location_type($location->term_id);
			}
		}
		
		$all_goods_batchses = array();
		if ($all_locations) {
			$arg		= array(
										'numberposts'	=> 1000,
										'offset'    	=> 0,
										'orderby'  		=> 'id',
										'order'     	=> 'ASC',
										'post_type' 	=> GOODS_BATCH_NAME,
										'post_status' 	=> 'publish',
										'meta_query'	=> array(
																	array(
																			'key'		=> 'owner_id',
																			'value'		=> $all_locations,
																			'operator'	=> "OR"
																		 )
																),
									); 
			$all_goods_batchses	= get_posts($arg);
		}
			
		//==========================
		//
		//from server
		//
		//==========================
		$html				= SMP_Goods_Batch::serverside_service($all_goods_batchses);
		//$html				.= "<div class='smp-production-container'><div class='smp-production-container'><div id='col1' class='smp-cl1'>";
		$sorted_batches		= array();	
		foreach($all_goods_types as $goods_type) 
		{
			$sorted_batches[(string)$goods_type->ID]= array();
		}
		
		function by_type($batch)
		{
			global $current_goods_type;			
			//var_dump(get_post_meta($batch->ID,"goods_type_id",true) == $current_goods_type->ID);
			return get_post_meta($batch->ID,"goods_type_id", true) == $current_goods_type->ID;
		}
		$arr		= array();
		foreach($all_goods_types as $current_goods_type)
		{
			$sorted_batches[(string)$current_goods_type->ID]	= array_filter($all_goods_batchses, "by_type");
			//echo "<BR> --- ".$current_goods_type->post_title."<BR>";
			//var_dump($sorted_batches[(string)$current_goods_type->ID]);
			
		}
				
		$i=0;
		foreach($all_goods_types as $goods_type)
		{
			$title			=  $goods_type->post_title . "<span class='smp-colorized' style='font-weight:700;'> (".count($sorted_batches[(string)$goods_type->ID]).")</span>";
			
			$slide			.= "<div class='smp-pr-main' id='production-".$goods_batch->ID."' button_id='".$ii++."' factory_id='".$goods_batch->ID."' style=''>";
			$slide			.= '<h3>'. __("Goods type", "smp").' <span style=\'font-weight:700; color:'.$user_iface_color.'!important\'><a href=\'/?goods_type='.$goods_type->post_name.'\'>' . $goods_type->post_title.'</a></span></h3>';
			$cb				= $sorted_batches[(string)$goods_type->ID];
			$i = 0;
			$slide			.= "<div class='smp-store-batch-list'>";
			foreach($cb as $goods_batch)
			{
				$gb			= SMP_Goods_Batch::get_instance($goods_batch->ID);
				if($gb->user_is_owner())
				{
					$post_data			= 1;
				}
				else
				{				
					$post_data			= -1;
				}
				$slide		.= $gb->get_stroke($gb->body, $post_data);				
				$i++;
			}
			if($i==0)
				$slide		.= "<div class=smp-comment>".__("No Goods of this type", "smp")."</div>";
			$slide			.= "</div>";
			$slide			.= "</div>";	

			$arr[]			= array("title"=>$title, "slide"=>$slide);
		}	
		$html				.= Assistants::get_lists($arr);
		/*
			
		$i=0;
		foreach($all_goods_types as $goods_type)
		{
			$html			.=  "<div class='smp-pr-title' style='text-align:right;' id='production-btn-" . $goods_type->ID . "' button_id=" . $i++ . ">" . $goods_type->post_title . "<span class='smp-colorized' style='font-weight:700;'> (".count($sorted_batches[(string)$goods_type->ID]).")</span></div>";
		}		
		$html				.= "
			</div>
			<div id='col2' class='smp-cl2'>
				
					<!--form method='post'  enctype='multipart/form-data'  id='form'-->"; 
		$ii = 0;
		foreach($all_goods_types as $goods_type)
		{
			
			$html			.= "<div class='smp-pr-main' id='production-".$goods_batch->ID."' button_id='".$ii++."' factory_id='".$goods_batch->ID."' style=''>";
			$html			.= '<h3>'. __("Goods type", "smp").' <span style=\'font-weight:700; color:'.$user_iface_color.'!important\'><a href=\'/?goods_type='.$goods_type->post_name.'\'>' . $goods_type->post_title.'</a></span></h3>';
			$cb				= $sorted_batches[(string)$goods_type->ID];
			$i = 0;
			$html			.= "<div class='smp-store-batch-list'>";
			foreach($cb as $goods_batch)
			{
				$gb			= SMP_Goods_Batch::get_instance($goods_batch->ID);
				if($gb->user_is_owner())
				{
					$post_data			= 1;
				}
				else
				{				
					$post_data			= -1;
				}
				$html		.= $gb->get_stroke($gb->body, $post_data);				
				$i++;
			}
			if($i==0)
				$html		.= "<div class=smp-comment>".__("No Goods of this type", "smp")."</div>";
			$html			.= "</div>";
			$html			.= "</div>";	
		}
		$html				.= '
							<!--/form-->						
						</div>
					</div>	
				</div>	
						';
		*/
		$html				.= "<div class='smp-comment' style='margin-bottom:10px;'>".__("Choose Cood Type you need (from left collumn) for see all butches, than you holded. Attantion! Owners of the batch are your Locations formally. If You owned mutually with other Player, gds. are controlled by your co-proprietors too. You may:", "smp")."</div>";
	
		unset($current_goods_type);
		wp_reset_postdata();
		
		return  "<div id=smc_content>" . $html . "</div>";
	}
?>