<?php
	require_once(SMP_REAL_PATH.'shortcodes/store_shortcode.php');
	require_once(SMP_REAL_PATH.'shortcodes/my_productions_shortcode.php');
	require_once(SMP_REAL_PATH.'shortcodes/my_storage_shortcode.php');
	require_once(SMP_REAL_PATH.'shortcodes/smp_bank.php');
	require_once(SMP_REAL_PATH.'shortcodes/smp_my_hubs_shortcode.php');
	require_once(SMP_REAL_PATH.'shortcodes/smp_my_invoices_shortcode.php');
	require_once(SMP_REAL_PATH.'shortcodes/smp_my_tools.php');
	
	function smp_add_shortcodes()
	{
		add_shortcode('my_production_page', 	'my_production_page'); 
		add_shortcode('my_storage_page',		'my_storage_page'); 
		add_shortcode('smp_store', 				'smp_store'); 
		add_shortcode('smp_bank', 				'smp_bank'); 
		add_shortcode('smp_my_hubs', 			'smp_my_hubs'); 
		add_shortcode('smp_my_invoices', 		'smp_my_invoices'); 
		add_shortcode('smp_personal_calc', 		'smp_my_tools'); 
	
	}
	add_action("init", "smp_add_shortcodes", 99);
?>